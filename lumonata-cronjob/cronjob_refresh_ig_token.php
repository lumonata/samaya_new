<?php

require_once( realpath( dirname( __FILE__ ) ) . '/../lumonata_config.php' );
require_once( realpath( dirname( __FILE__ ) ) . '/../lumonata-classes/db.php' );

global $db;

//-- GET all destinations list

$s = 'SELECT * FROM lumonata_articles AS a WHERE a.larticle_type = %s AND a.larticle_status = %s';
$q = $db->prepare_query( $s, 'destinations', 'publish' );
$r = $db->do_query( $q );
$n = $db->num_rows( $r );

if( $n > 0 )
{
    while( $d = $db->fetch_array( $r ) )
    {
        $s2 = 'SELECT lvalue FROM lumonata_additional_fields AS a WHERE a.lapp_id = %d AND a.lkey = %s AND a.lapp_name = %s';
        $q2 = $db->prepare_query( $s2, $d[ 'larticle_id' ], 'ig_token', 'destinations' );
        $r2 = $db->do_query( $q2 ); 
        $d2 = $db->fetch_array( $r2 );

        if( isset( $d2[ 'lvalue' ] ) && !empty( $d2[ 'lvalue' ] ) )
        {
            $token = stripslashes( $d2[ 'lvalue' ] );
            $curl  = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=' . $token,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
            ));

            $res = curl_exec( $curl );

            curl_close( $curl );

            if( !empty( $res ) )
            {
                $dt = json_decode( $res, true );

                if( isset( $dt[ 'access_token' ] ) )
                {
                    $db->update( 'lumonata_additional_fields', array( 'lvalue' => $dt[ 'access_token' ] ), array( 'lapp_id' => $d[ 'larticle_id' ], 'lkey' => 'ig_token', 'lapp_name' => 'destinations' ) );
                }
            }
        }
    }
}

exit;
