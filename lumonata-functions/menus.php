<?php

if( isset( $_POST['app_set_name'] ) )
{
    require_once( '../lumonata_config.php' );
    require_once( '../lumonata-functions/user.php' );
    
    if( is_user_logged() )
    {
        require_once( '../lumonata_config.php' );
        require_once( '../lumonata_settings.php' );
        require_once( '../lumonata-functions/settings.php' );
        require_once( '../lumonata-classes/actions.php' );
        
        if( $_POST['app_set_name'] == 'pages' )
        {
            echo get_published_pages();
        }
        if( $_POST['app_set_name'] == 'archive' )
        {
        	echo get_published_post_archive( $_POST['plug_post_type'] );
        }
        elseif( $_POST['app_set_name'] == 'url' )
        {
            echo get_custome_url();
        }
        elseif( in_array( $_POST['app_set_name'], $_POST['plug_post_type'] ) )
    	{
    		echo get_published_post( $_POST['app_set_name'] );
    	}
        else
        {
        	echo get_published_apps( $_POST['app_set_name'] );
        }
    }
}

if( isset( $_POST['theorder'] ) )
{
    require_once( '../lumonata-functions/user.php' );
    
    if( is_user_logged() )
    {
        require_once( '../lumonata_config.php' );
        require_once( '../lumonata_settings.php' );
        require_once( '../lumonata-functions/settings.php' );
        require_once( '../lumonata-classes/actions.php' );
        
        update_menu_order( $_POST['active_tab'], $_POST['theorder'] );
    }
}

if( isset( $_POST['create_menu_set'] ) )
{
    require_once( '../lumonata-functions/user.php' );
    
    if( is_user_logged() )
    {
        if( !defined( 'SITE_URL' ) )
        {
            define( 'SITE_URL', get_meta_data( 'site_url' ) );
        }
        
        if( !empty( $_POST['setname'] ) )
        {
            add_menu_set();
        }
    }
}

if( isset( $_POST['removed_menu_set'] ) )
{
    require_once( '../lumonata-functions/user.php' );
    
    if( is_user_logged() )
    {
        require_once( '../lumonata_config.php' );
        require_once( '../lumonata_settings.php' );
        require_once( '../lumonata-functions/settings.php' );
        require_once( '../lumonata-classes/actions.php' );
        
        if( !defined( 'SITE_URL' ) )
        {
            define( 'SITE_URL', get_meta_data( 'site_url' ) );
        }
        
        if( remove_menu_set( $_POST['removed_menu_set'] ) )
        {
            echo "OK";
        }
    }
}

if( isset( $_POST['edit_items'] ) )
{
    require_once( '../lumonata-functions/user.php' );
    
    if( is_user_logged() )
    {
        require_once( '../lumonata_config.php' );
        require_once( '../lumonata_settings.php' );
        require_once( '../lumonata-functions/settings.php' );
        require_once( '../lumonata-classes/actions.php' );
        
        if( !defined( 'SITE_URL' ) )
        {
            define( 'SITE_URL', get_meta_data( 'site_url' ) );
        }
        
        if( $_POST['edit_items'] == 'edit' )
        {
            // echo edit_menu_items( $_POST['id'], $_POST['tab']);
            if( edit_menu_items( $_POST['id'], $_POST['tab'] ) )
            {
                echo 'OK';
            }
        }
        elseif( $_POST['edit_items'] == 'remove' )
        {
            if( remove_menu_items( $_POST['id'], $_POST['tab'] ) )
            {
                echo 'OK';
            }
        }
    }
}

if( isset( $_POST['add_selected'] ) )
{
    require_once( '../lumonata_config.php' );
    require_once( '../lumonata_settings.php' );
    require_once( '../lumonata-functions/user.php' );
    require_once( '../lumonata-functions/settings.php' );
    require_once( '../lumonata-classes/actions.php' );
    
    if( is_user_logged() )
    {
        if( !defined( 'SITE_URL' ) )
        {
            define( 'SITE_URL', get_meta_data( 'site_url' ) );
        }
        
        $json_menu_set = get_meta_data( 'menu_set', 'menus' );
        
        if( !empty( $json_menu_set ) )
        {
            $menu_set      = json_decode( $json_menu_set, true );
            $menu_set_keys = array_keys( $menu_set );
            $active_tab    = '';
            
            if( $_POST['add_selected'] == 'single' )
            {
                if( empty( $_POST['tab'] ) )
                {
                    if( count( $menu_set_keys ) > 0 )
                    {
                        $active_tab = $menu_set_keys[0];
                    }
                }
                else
                {
                    $active_tab = $_POST['tab'];
                }
                
                if( add_menu_set_items( $active_tab ) )
                {
                    echo 'OK';
                }
            }
            else
            {
                if( isset( $_POST['selected_menu'] ) )
                {
                    if( empty( $_GET['tab'] ) )
                    {
                        if( count( $menu_set_keys ) > 0 )
                        {
                            $active_tab = $menu_set_keys[0];
                        }
                    }
                    else
                    {
                        $active_tab = $_GET['tab'];
                    }
                    
                    add_menu_set_items( $active_tab );
                }
            }
        }
    }
}

add_actions( 'menus', 'set_menus' );

function update_menu_order( $menu_set, $new_order = array() )
{
    return update_meta_data( 'menu_order_' . $menu_set, json_encode( $new_order ), 'menus' );
}

function edit_menu_items( $id, $menuset )
{
    
    require_once("../lumonata-plugins/language/function_helper.php");
    if( empty( $menuset ) )
    {
        $json_menu_set = get_meta_data( 'menu_set', 'menus' );
        $menu_set      = json_decode( $json_menu_set, true );
        $menu_set_keys = array_keys( $menu_set );
        
        if( count( $menu_set_keys ) > 0 )
        {
            $menuset = $menu_set_keys[0];
        }
        else
        {
            return false;
        }
    }
    
    $menu_items = get_meta_data( 'menu_items_' . $menuset, 'menus' );
    $menu_items = json_decode( $menu_items, true );

    // $active_lang = check_active_plugin_language();
    // $nav_menu_lang = array();
    // if($active_lang){
    //     $data_lang = get_language_data();
    //     $code_lang  = $data_lang['code_lang'];
    //     foreach($code_lang as $c_lang){
    //         $nav_menu_lang[] = $_POST['label_'.$c_lang];
    //     }
    // }

    
    $ssl_option = get_meta_data('ssl_option');
    $http = ($ssl_option==1?'https://':'http://');
    
    $data_lang = "";
    if(check_active_plugin_language())
    {
        $data_lang = get_language_datas();
    }
    
    foreach( $menu_items as $key => $val )
    {
        if( is_array( $val ) )
        {
            if( $val['id'] == $id )
            {
                $destination = "";
                if(isset($_POST['destination']) && !empty($_POST['destination']))
                {
                    $link        = $_POST['link'];
                    $permalink   = $_POST['permalink'];
                    $destination = $_POST['destination'];

                    $type_post = "";
                    $link_explode = explode("/", $link);
                    if(count($link_explode) == 3)
                    {
                        $type_post = "publish_post";
                    }
                    
                    if(strpos($link, '/'))
                    {
                        $link        = set_new_link_with_destination($link, $destination, '', $type_post);
                        $permalink   = set_new_link_with_destination($permalink, $destination, '', $type_post);
                    }
                }

                $new_menu = array(
                    'id'                        => $val['id'],
                    'label'                     => $_POST['label'],
                    'target'                    => $_POST['target'],
                    'link_without_dest'         => $http.$_POST['link'],
                    'permalink_without_dest'    => $http.$_POST['permalink'],
                    'destination'               => $destination,
                    'link'                      => $http.$link,
                    'permalink'                 => $http.$permalink,
                    'css_class'                 => $_POST['css_class'],
                );

                if(!empty($data_lang))
                {
                    $code_lang  = $data_lang['code_lang'];
                    foreach($code_lang as $c_lang){

                        $link_lang  = '';
                        $permalink_lang = '';

                        $type_post = "";
                        
                        $link_explode = explode("/", $_POST['link']);
                        if(count($link_explode) == 3)
                        {
                            $type_post = "publish_post";
                        }

                        if(!empty($destination))
                        {
                            if(strpos($link, '/'))
                            {
                                $link_lang      = set_new_link_with_destination($_POST['link'], $_POST['destination'], $c_lang, $type_post);
                                $permalink_lang = set_new_link_with_destination($_POST['permalink'], $_POST['destination'], $c_lang, $type_post);
                            }
                        }

                        $new_menu += array(
                            'label_'.$c_lang    => $_POST['label_'.$c_lang],
                            'link_'.$c_lang     => $http.$link_lang,
                            'pemalink_'.$c_lang => $http.$permalink_lang
                        );
                    }
                }

                $new_menu_items[] = $new_menu;
            }
            else
            {
                $menu_old = array(
                    'id'                     => $val['id'],
                    'label'                  => $val['label'],
                    'target'                 => $val['target'],
                    'link_without_dest'      => $val['link_without_dest'],
                    'permalink_without_dest' => $val['permalink_without_dest'],
                    'destination'            => $val['destination'],
                    'link'                   => $val['link'],
                    'permalink'              => $val['permalink'],
                    'css_class'              => $val['css_class'],
                );

                if(!empty($data_lang))
                {
                    $code_lang  = $data_lang['code_lang'];
                    foreach($code_lang as $c_lang){
                        $label_lang = (isset($val['label_'.$c_lang]) ? $val['label_'.$c_lang] : '');
                        $menu_old += array(
                            'label_'.$c_lang    => $label_lang,
                            'link_'.$c_lang     => $val['link_'.$c_lang],
                            'pemalink_'.$c_lang => $val['pemalink_'.$c_lang]
                        );
                    }
                }

                $new_menu_items[] = $menu_old;
            }
        }
    }
    
    $edited_menu_items = json_encode( $new_menu_items );
    
    return update_meta_data( 'menu_items_' . $menuset, $edited_menu_items, 'menus' );
}

function remove_menu_items( $id, $menuset )
{
    if( empty( $menuset ) )
    {
        $json_menu_set = get_meta_data( 'menu_set', 'menus' );
        $menu_set      = json_decode( $json_menu_set, true );
        $menu_set_keys = array_keys( $menu_set );
        
        if( count( $menu_set_keys ) > 0 )
        {
            $menuset = $menu_set_keys[0];
        }
        else
        {
            return false;
        }
    }
    
    //-- Remove Menu Items
    $menu_items = get_meta_data( 'menu_items_' . $menuset, 'menus' );
    $menu_items = json_decode( $menu_items, true );
    $menu_order = get_meta_data( 'menu_order_' . $menuset, 'menus' );
    $menu_order = json_decode( $menu_order, true );
    
    foreach( $menu_items as $key => $val )
    {
        if( is_array( $val ) )
        {
            if( in_array( $val['id'], get_child_items( $id, $menu_order ) ) )
            {
                unset( $menu_items[$key] );
            }
        }
    }
    
    $menu_items = json_encode( $menu_items );
    
    if( update_meta_data( 'menu_items_' . $menuset, $menu_items, 'menus' ) )
    {
        
        //-- Remove Menu Order
        $new_order = remove_menu_orders( $id, $menuset, $menu_order );
        $new_order = json_encode( $new_order );
        
        return update_meta_data( 'menu_order_' . $menuset, $new_order, 'menus' );
    }
}

function get_child_items( $root_id, $menu_order = array() )
{
    $results = array();
    
    foreach( $menu_order as $key => $val )
    {
        if( find_is_match_array( $root_id, $menu_order[$key] ) )
        {
            $results = array(
                 $root_id 
            );
            
            if( isset( $val['children'] ) && is_array( $val['children'] ) )
            {
                foreach( $val['children'] as $keyc => $valc )
                {
                    $results = array_merge( $results, get_child_items( $valc['id'], $val['children'] ) );
                }
            }
        }
    }
    
    return $results;
}

function remove_menu_orders( $id, $menuset, &$menu_order = array() )
{
    
    if( empty( $menuset ) || $id == '' || empty( $menu_order ) )
    {
        return;
    }
    
    foreach( $menu_order as $key => &$val )
    {
        if( find_is_match_array( $id, $menu_order[$key] ) )
        {
            unset( $menu_order[$key] );
        }
        else
        {
            if( isset( $val['children'] ) && is_array( $val['children'] ) )
            {
                remove_menu_orders( $id, $menuset, $val['children'] );
            }
        }
    }
    
    return filter_order( $menu_order );
}

function filter_order( &$array = array() )
{
    foreach( $array as $key => &$val )
    {
        
        if( empty( $val['children'] ) )
        {
            unset( $val['children'] );
        }
        
        if( isset( $val['children'] ) && is_array( $val['children'] ) )
        {
            filter_order( $val['children'] );
        }
    }
    
    return $array;
}

function find_is_match_array( $id, $array = array() )
{
    if( $id == $array['id'] )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function add_menu_set_items( $active_tab )
{
    if( empty( $active_tab ) )
    {
        return false;
    }
    
    if( !empty( $active_tab ) )
    {
        $ssl_option = get_meta_data('ssl_option');
        $http = ($ssl_option==1?'https://':'http://');
        
        $menu_set_items        = get_meta_data( "menu_items_" . $active_tab, 'menus' );
        $decode_menu_set_items = json_decode( $menu_set_items, true );
        
        if( count( $decode_menu_set_items ) == 0 )
        {
            $id = 0;
        }
        else
        {
            $tmp_arr = array();
            
            foreach( $decode_menu_set_items as $key_1 => $val_1 )
            {
                $tmp_arr[] = $val_1['id'];
            }
            
            $id = max( $tmp_arr ) + 1;
        }
        
        if( $_POST['add_selected'] == 'single' )
        {
            $link        = $_POST['link'];
            $permalink   = $_POST['permalink'];
            $destination = "";
            $type_post = isset($_POST['type_post']) ? $_POST['type_post'] : '';

            if(isset($_POST['destination']) && !empty($_POST['destination']))
            {
                $destination = $_POST['destination'];
                $link        = set_new_link_with_destination($link, $destination,'', $type_post);
                $permalink   = set_new_link_with_destination($permalink, $destination, '',$type_post);
            }
            

            $menu_items = array(
                'id'                     => $id,
                'label'                  => $_POST['label'],
                'target'                 => $_POST['target'],
                'link_without_dest'      => $http.$_POST['link'],
                'permalink_without_dest' => $http.$_POST['permalink'],
                'link'                   => $http.$link,
                'permalink'              => $http.$permalink,
                'css_class'              => '',
                'destination'            => $destination
            );

            
            if(check_active_plugin_language())
            {
                $data_lang = get_language_datas();
                $code_lang = $data_lang['code_lang'];
                foreach($code_lang as $c_lang){

                    $link_lang      = '';
                    $permalink_lang = '';

                    if(!empty($destination))
                    {
                        $link_lang      = set_new_link_with_destination($_POST['link'], $destination, $c_lang, $type_post);
                        $permalink_lang = set_new_link_with_destination($_POST['permalink'], $destination, $c_lang, $type_post);
                    }

                    $menu_items += array(
                        'link_'.$c_lang     => $http.$link_lang,
                        'pemalink_'.$c_lang => $http.$permalink_lang
                    );
                }
            }

            $the_menu_items[] = $menu_items;
            
            $the_menu_order[] = array(
                 'id' => $id 
            );
        }
        else
        {
            foreach( $_POST['selected_menu'] as $key => $value )
            {
                $the_menu_items[] = array(
                    'id'        => $id,
                    'label'     => $_POST['label'][$value],
                    'target'    => $_POST['target'][$value],
                    'link'      => $_POST['link'][$value],
                    'permalink' => $_POST['permalink'][$value],
                    'css_class' => '',
                );
                
                $the_menu_order[] = array(
                    'id' => $id 
                );
                
                $id++;
            }
        }
        
        $menu_items = get_meta_data( 'menu_items_' . $active_tab, 'menus' );
        $menu_order = get_meta_data( 'menu_order_' . $active_tab, 'menus' );
        
        if( empty( $menu_items ) )
        {
            set_meta_data( 'menu_items_' . $active_tab, json_encode( $the_menu_items ), 'menus' );
        }
        else
        {
            $the_menu_items = array_merge( json_decode( $menu_items, true ), $the_menu_items );
            update_meta_data( 'menu_items_' . $active_tab, json_encode( $the_menu_items ), 'menus' );
        }
        
        if( empty( $menu_order ) )
        {
            set_meta_data( 'menu_order_' . $active_tab, json_encode( $the_menu_order ), 'menus' );
        }
        else
        {
            $the_menu_order = array_merge( json_decode( $menu_order, true ), $the_menu_order );
            update_meta_data( 'menu_order_' . $active_tab, json_encode( $the_menu_order ), 'menus' );
        }
        
        return true;
    }
}

function add_menu_set()
{
    $current_menu_set = get_meta_data( 'menu_set', 'menus' );
    $menu_key         = generateSefUrl( rem_slashes( $_POST['setname'] ) );
    $menu_set         = array(
         $menu_key => $_POST['setname'] 
    );
    
    if( empty( $current_menu_set ) )
    {
        $menu_set = json_encode( $menu_set );
        set_meta_data( 'menu_set', $menu_set, 'menus' );
    }
    else
    {
        $ob_menu_set     = json_decode( $current_menu_set, true );
        $menu_set        = array_merge( $ob_menu_set, $menu_set );
        $encode_menu_set = json_encode( $menu_set );
        
        update_meta_data( 'menu_set', $encode_menu_set, 'menus' );
    }
    
    header( 'location:' . get_tab_url( $menu_key ) );
}

function remove_menu_set( $menu_set )
{
    //-- Remove menu set
    $json_menu_set = get_meta_data( 'menu_set', 'menus' );
    $ob_menu_set   = json_decode( $json_menu_set, true );
    
    foreach( $ob_menu_set as $key => $val )
    {
        if( $menu_set == $key )
        {
            unset( $ob_menu_set[$key] );
            break;
        }
    }
    
    $encode_menu_set = json_encode( $ob_menu_set );
    
    //-- Remove the menu items
    delete_meta_data( 'menu_order_' . $menu_set, 'menus' );
    delete_meta_data( 'menu_items_' . $menu_set, 'menus' );
    
    return update_meta_data( 'menu_set', $encode_menu_set, 'menus' );
}

function set_menus()
{
    add_actions( 'section_title', 'Menus' );
    add_actions( 'admin_tail', 'get_javascript', 'interface-1.2' );
    add_actions( 'admin_tail', 'get_javascript', 'inestedsortable' );
    add_actions( 'admin_tail', 'menus_javascript' );
    add_actions( 'admin_tail', 'view_more_tabs_js' );
    
    $html = '
	<td class="apps-set-wrapp">
		<div class="the_apps_set">
			<h2>Applications Set</h2>
			<div class="apps-box">
				<select class="apps-set" name="apps_set" autocomplete="off">
					<option value="">Choose Applications</option>
					<option value="archive">Post Archive</option>
					<option value="pages">Pages</option>
					<option value="blogs">Blogs</option>
					<option value="articles">Articles</option>
					' . attemp_actions( 'plugin_menu_set' ) . '
					<option value="url">Custome URL</option>
				</select>
			</div>
			<form method="post" action="">
				<span id="error" ></span>
				<div class="sets_apps_item">
					<input type="hidden" name="curpage" id="activetab" value="' . ( isset( $_GET['tab'] ) ? $_GET['tab'] : '' ) . '">
					<input type="hidden" name="curpage" id="curpage" value="' . cur_pageURL() . '">
					<ul id="the_apps_lists">' . get_published_pages() . '</ul>
				</div>
				<div class="apps-act">
					<input type="button" value="Select all" name="select_all" class="button" />
					<input type="submit" value="Add to menu set" name="add_selected" class="button" />
				</div>
			</form>
		</div>
	</td>';
    
    $json_menu_set = get_meta_data( 'menu_set', 'menus' );
    $menu_set      = json_decode( $json_menu_set, true );
    
    $html .= '
	<td class="menu-set-wrapp">
		<div class="the_menus_set">
			<h2>Menus Set</h2>
			<form action="" method="post">
				<div class="setname">
					<span>Set Name: </span>
					<input type="text" value="" name="setname" class="medium_textbox" />
					<input type="submit" name="create_menu_set" value="Create Menu Set" class="button" />
				</div>
			</form>
			<ul class="tabs">';
    
			    $active_tab = 'the_active_one';
			    
			    if( empty( $json_menu_set ) || count( $menu_set ) < 1 )
			    {
			        $html .= '';
			    }
			    else
			    {
			        $menu_set_keys = array_keys( $menu_set );
			        
			        $i = 0;
			        
			        if( empty( $_GET['tab'] ) )
			        {
			            if( count( $menu_set_keys ) > 0 )
			            {
			                $active_tab = $menu_set_keys[0];
			            }
			        }
			        else
			        {
			            $active_tab = $_GET['tab'];
			        }
			        
			        foreach( $menu_set as $key => $val )
			        {
			            $script = '
						<script type="text/javascript">
							jQuery(function(){
								jQuery("#del_set_' . $i . '").click(function(){
									jQuery.post("../lumonata-functions/menus.php", { removed_menu_set:"' . $key . '" }, function(data){
										if(data=="OK"){
											location = "' . get_state_url( 'menus' ) . '"
										}
									});
								});
							});
						</script>';

			            if( $i < 4 )
			            {
			                if( $key == $active_tab )
			                {
			                    $html .= '
								<li class="active">
									<a href="' . get_tab_url( $key ) . '">' . $val . '</a>
									<span id="del_set_' . $i . '" class="delete_tab">X</span>
									' . $script . '
								</li>';
			                }
			                else
			                {
			                    $html .= '
								<li>
									<a href="' . get_tab_url( $key ) . '">' . $val . '</a>
									<span id="del_set_' . $i . '" class="delete_tab">X</span>
									' . $script . '
								</li>';
			                }
			            }
			            elseif( $key == $active_tab )
			            {
			                $html .= '
							<li class="active">
								<a href="' . get_tab_url( $key ) . '">' . $val . '</a>
								<span id="del_set_' . $i . '" class="delete_tab">X</span>
								' . $script . '
							</li>';
			            }
			            
			            $i++;
			        }
			        
			        $i = 0;
			        
			        $view_more_tabs = false;
			        
			        foreach( $menu_set as $key => $val )
			        {
			            if( $i == 4 )
			            {
			                $html2 = '
							<li class="view_more_tabs">
								<a href="javascript:;" >
									<img src="'. HTTP . TEMPLATE_URL . '/images/ico-arrow.png" border="0" />
								</a>
								<ul id="sub_tab">';
			                if( $key != $active_tab )
			                {
			                    $html2 .= '<li><a href="' . get_tab_url( $key ) . '">' . $val . '</a></li>';
			                    $view_more_tabs = true;
			                }
			            }
			            elseif( $i > 4 and $key != $active_tab )
			            {
			                $html2 .= '<li><a href="' . get_tab_url( $key ) . '">' . $val . '</a></li>';
			                $view_more_tabs = true;
			            }
			            
			            $i++;
			        }
			        
			        if( $view_more_tabs )
			        {
			            $html2 .= '</ul>';
			            $html2 .= '</li>';
			            
			            $html .= $html2;
			        }
			    }
			    
			    $html .= '
			</ul>

			<div class="the_sets">';
    
			    json_decode( $json_menu_set, true );
			    
			    if( empty( $json_menu_set ) || count( $menu_set ) < 1 )
			    {
			        $html .= '
		    		<p>Please create the Menu Set first before you add the Application Items. To add new Menu Set, please fill the Set Name above and then click Create Menu Set.</p>
		    		<p>After you create the Menu Set, then choose the Application on the left side to add to the Menu that you created. Click Add To Menu Set to add the items to this Menu</p>
		    		<p>When you finish adding the application items, you can sorting the Menu Items by dragg and dropping it.</p>';
			    }
			    else
			    {
			        $html .= '
		    		<div class="process-alert" id="procces_alert">Saving...</div>
		    		<iframe id="iframe_menuset" src="menus.php?active_tab=' . $active_tab . '" width="100%" frameborder="0"></iframe>';
			    }
			    
			    $html .= '

		    	</div>
		    </div>
		</td>';
    
    	$html = '
		<table id="tb_menu" class="tb-menu">
			<tr>' . $html . '</tr>
		</table>
		<script>
			var ua = navigator.userAgent.toLowerCase(); 

			if (ua.indexOf("safari")!=-1){
				if(ua.indexOf("chrome")  > -1){
					//chrome
					jQuery(".the_apps_set").css("width","100%");
				}else{
					//safari
					if(navigator.appVersion.indexOf("Mac")!=-1){
						jQuery(".the_apps_set").css("width","100%");
					}
				}
			}				

	        jQuery(window).resize(function() {
	        	var wheight = jQuery(window).height();
	        	jQuery("#tb_menu").height(wheight - 115);
	    		jQuery(".sets_apps_item").height(wheight - 325);
	    		jQuery("#iframe_menuset").height(wheight - 278);
			});

			jQuery(window).trigger("resize");
    	</script>';
    
    return $html;
}

function get_menu_items( $menu_items = array(), $menu_order = array(), $active_tab, $active_lang=false, $data_lang='' )
{
    $ssl_option = get_meta_data('ssl_option');
    $http = ($ssl_option==1?'https://':'http://');
    
    $html   = '';
    $target = array(
        '_self' => 'Self',
        '_blank' => 'Blank' 
    );

    $destination = get_destination_list();
    $destinations = array();

    if(!empty($destination))
    {
        foreach($destination as $d)
        {
            $destinations += array(
                $d['sef'] => $d['title']
            );
        }
    }
    
    
    if( is_array( $menu_order ) )
    {
        foreach( $menu_order as $key => $val )
        {
            $items_val = array_match( $menu_items, 'id', $val['id'] );
            
            $html .= '
			<li id="ele-' . $val['id'] . '" class="clear-element page-item1">
				<div class="sort-handle">
					<div class="apps_item">
						<div class="apps_item_text">' . $items_val[0]['label'] . '</div>
						<div class="view" id="view_' . $val['id'] . '"></div>
					</div>
				</div>
				<div class="menuset_details_item" id="details_' . $val['id'] . '" style="display:none;">
					<p class="label">
						<label>Label :</label>
                        <input type="text" value="' . $items_val[0]['label'] . '" id="label_' . $val['id'] . '" name="title[' . $val['id'] . ']" class="medium_textbox" />
            ';

            if($active_lang){
                $html .= label_menu_form_lang_func($val['id'], $data_lang, $items_val);
            }

            $html .= '
						<span class="clear-element"></span>
					</p>
					<p class="target">
						<label>Target :</label>
						<select id="target_' . $val['id'] . '" name="target[' . $val['id'] . ']">';
            
				            foreach( $target as $key_items => $val_items )
				            {
				                if( $key_items == $items_val[0]['target'] )
				                {
				                    $html .= '<option value="' . $key_items . '" selected="selected">' . $val_items . '</option>';
				                }
				                else
				                {
				                    $html .= '<option value="' . $key_items . '">' . $val_items . '</option>';
				                }
				            }
				            
				            $html .= '

						</select>
						<span class="clear-element"></span>
                    </p>
                    ';
                
                if(!empty($destination))
                {
                    $html .=' 
                    <p class="destination">
						<label>Destination :</label>
						<select id="destination_' . $val['id'] . '" name="destination[' . $val['id'] . ']">';
            
				            foreach( $destinations as $key_items => $val_items )
				            {
				                if( $key_items == $items_val[0]['destination'] )
				                {
				                    $html .= '<option value="' . $key_items . '" selected="selected">' . $val_items . '</option>';
				                }
				                else
				                {
				                    $html .= '<option value="' . $key_items . '">' . $val_items . '</option>';
				                }
				            }
				            
				            $html .= '

						</select>
						<span class="clear-element"></span>
                    </p>
                    ';
                }
                    
                    // echo stristr( '^http://*', $items_val[0]['link'] );
            
		            if( stristr( '^http://*', $items_val[0]['link_without_dest'] ) )
		            {
		                $html .= '
						<p class="url">
							<label>URL</label>: 
							<input type="hidden" value="' . $items_val[0]['link_without_dest'] . '"  id="link_' . $val['id'] . '" name="link[' . $val['id'] . ']" class="medium_textbox urlNum1" />
							<input type="hidden" value="' . $items_val[0]['permalink_without_dest'] . '" id="permalink_' . $val['id'] . '" name="permalink[' . $val['id'] . ']" class="urlNum2" />
						</p>';
		            }
                    else if( stristr( '^https://*', $items_val[0]['link_without_dest'] ) )
                    {
                        $html .= '
                        <p class="url">
                            <label>URL</label>: 
                            <input type="hidden" value="' . $items_val[0]['link_without_dest'] . '"  id="link_' . $val['id'] . '" name="link[' . $val['id'] . ']" class="medium_textbox urlNum1" />
                            <input type="hidden" value="' . $items_val[0]['permalink_without_dest'] . '" id="permalink_' . $val['id'] . '" name="permalink[' . $val['id'] . ']" class="urlNum2" />
                        </p>';
                    }
		            else
		            {
                        $html .= '
                        <p class="url">
						    <label>URL :</label>
						    <input type="hidden" value="' . str_replace($http, "", $items_val[0]['link_without_dest']) . '" id="link_' . $val['id'] . '" name="link[' . $val['id'] . ']" />
                            <input type="text" value="' . str_replace($http, "", $items_val[0]['permalink_without_dest']) . '" id="permalink_' . $val['id'] . '" name="permalink[' . $val['id'] . ']" class="medium_textbox" />
                        </p>';
                    }
                    
                    $html .= '
                        <p class="label">
                            <label>CSS Class (optional) :</label>
                            <input type="text" value="' . $items_val[0]['css_class'] . '" id="css_class_' . $val['id'] . '" name="css_class_[' . $val['id'] . ']" class="medium_textbox" />
                            <span class="clear-element"></span>
                        </p>
                    ';
		            
		            $html .= '

					<div class="act">
						<input type="button" value="Remove" id="remove_items_' . $val['id'] . '" name="remove_menu[' . $val['id'] . ']" class="button" />
						<input type="button" value="Save" id="edit_items_' . $val['id'] . '" name="save_menu[' . $val['id'] . ']" class="button" />
					</div>

				</div>';
            
	            if( isset( $val['children'] ) )
	            {
	                if( is_array( $val['children'] ) )
	                {
	                    $html .= '
						<ul class="page-list">
							' . get_menu_items( $menu_items, $val['children'], $active_tab, $active_lang, $data_lang ) . '
						</ul>';
	                }
	            }
	            
	            $html .= '

			</li>';
        }
    }
    
    return $html;
}

function looping_js_nav( $menu_items = array() )
{
    $html = '';
    
    if( !empty( $menu_items ) )
    {        
        foreach( $menu_items as $key => $val )
        {            
            $html .= "
            <script type=\"text/javascript\">
            	jQuery(document).ready(function(){

					$('#view_" . $val['id'] . "').click(function(){
						$('#details_" . $val['id'] . "').slideToggle(100);
						return false;
					});

					$('#edit_items_" . $val['id'] . "').click(function(){
						$.post('../lumonata-functions/menus.php',{
							id:'" . $val['id'] . "',
							label:$('#label_" . $val['id'] . "').val(),
							label_cn:$('#label_cn_" . $val['id'] . "').val(),
							label_jp:$('#label_jp_" . $val['id'] . "').val(),
							target:$('#target_" . $val['id'] . "').val(),
							destination:$('#destination_" . $val['id'] . "').val(),
							link:$('#link_" . $val['id'] . "').val(),
							permalink:$('#permalink_" . $val['id'] . "').val(),
							css_class:$('#css_class_" . $val['id'] . "').val(),
							tab:$('#activetab').val(),
							edit_items:'edit'
						},function(data){
                            console.log(data);
							$('#procces_alert').show();

							if(data=='OK'){
								location.reload();
							}
						});

					});

					$('#remove_items_" . $val['id'] . "').click(function(){
						$.post('../lumonata-functions/menus.php',{
							id:'" . $val['id'] . "',
							tab:$('#activetab').val(),
							edit_items:'remove'
						},function(data){
							$('#procces_alert').show();

							if(data=='OK'){
								location.reload();
							}
						});
					});
				});
			</script>";
        }        
    }
    
    return $html;    
}

function array_match( $array, $key, $value )
{
    $results = array();

    if( is_array( $array ) )
    {        
        if( isset( $array[$key] ) )
        {
            if( $array[$key] == $value )
            {
                $results[] = $array;
            }
        }
        
        foreach( $array as $subarray )
        {
            $results = array_merge( $results, array_match( $subarray, $key, $value ) );
        }
        
    }
    
    return $results;    
}

function get_published_pages()
{    
    global $db;
    
    $html = '';
    
    $pub_pages = the_published_pages();

    $destination = "";
    if(check_active_plugin_destination())
    {
        $destination = get_destination_list();
    }

    $no = 0;
    
    while( $data = $db->fetch_array( $pub_pages ) )
    {        
        $larticle_title = $data['larticle_title'];
        
        $html .= "
        <li class=\"draggable\">
        	<div class=\"apps_item\">
        		<div class=\"apps_item_text\">" . $larticle_title . "</div>
        		<div class=\"add\" id=\"add_" . $data['larticle_id'] . "\">
        			<input type=\"checkbox\" name=\"selected_menu[]\" class=\"selected_menu\" value=\"" . $data['larticle_id'] . "\" />
        		</div>
        		<div class=\"view\" id=\"view_" . $data['larticle_id'] . "\"></div>
        	</div>
        	<div class=\"apps_details_item\" id=\"details_set_" . $data['larticle_id'] . "\" style=\"display:none;\">
        		<p>
        			<label>Label:</label>
        			<input type=\"text\" value=\"" . $larticle_title . "\" id=\"label_" . $data['larticle_id'] . "\" name=\"label[" . $data['larticle_id'] . "]\" class=\"medium_textbox\" />
        		</p>
        		<p>
        			<label>Target:</label>
        			<select id=\"target_" . $data['larticle_id'] . "\" name=\"target[" . $data['larticle_id'] . "]\">
        				<option value=\"_self\">Self</option>
        				<option value=\"_blank\">Blank</option>
        			</select>
                </p> ";

            if(!empty($destination))
            {
                $html .= '
                    <p>
                        <label>Destination: </label>
                        <select id="destination_' . $no . '" name="destination[' . $no . ']">
                ';
                foreach($destination as $d)
                {
                    $html .= '
                        <option value="'.$d['sef'].'">'.$d['title'].'</option>
                    ';
                }
                $html .= '
                        </select>
                    </p>
                ';
            }
                
        // $html .= "
        // 		<div style=\"text-align:right;\">
        // 			<input type=\"button\" id=\"add_to_menu_set_" . $data['larticle_id'] . "\" value=\"Add to menu set\" name=\"add[" . $data['larticle_id'] . "]\" class=\"button\" />
        // 		</div>
        // 		<input type=\"text\" value=\"" . "/?page_id=" . $data['larticle_id'] . "\" id=\"link_" . $data['larticle_id'] . "\" name=\"link[" . $data['larticle_id'] . "]\" />
        //         <input type=\"text\" value=\"" . site_url() . '/' . $data['lsef'] . "/\" id=\"permalink_" . $data['larticle_id'] . "\" name=\"permalink[" . $data['larticle_id'] . "]\" />";

        $html .= "
        		<div style=\"text-align:right;\">
        			<input type=\"button\" id=\"add_to_menu_set_" . $data['larticle_id'] . "\" value=\"Add to menu set\" name=\"add[" . $data['larticle_id'] . "]\" class=\"button\" />
        		</div>
        		<input type=\"hidden\" value=\"" . site_url() . '/' . $data['lsef'] . "/\" id=\"link_" . $data['larticle_id'] . "\" name=\"link[" . $data['larticle_id'] . "]\" />
                <input type=\"hidden\" value=\"" . site_url() . '/' . $data['lsef'] . "/\" id=\"permalink_" . $data['larticle_id'] . "\" name=\"permalink[" . $data['larticle_id'] . "]\" />";
                
        $html .= "
        	</div>
        </li>
        <script type=\"text/javascript\">
        	jQuery(document).ready(function(){
				$('#view_" . $data['larticle_id'] . "').click(function(){
					$('#details_set_" . $data['larticle_id'] . "').slideToggle(100);
					return false;
				});

				$('#add_to_menu_set_" . $data['larticle_id'] . "').click(function(){
                    $('#procces_alert').show();

					$.post('../lumonata-functions/menus.php',{
						label:$('#label_" . $data['larticle_id'] . "').val(),
						target:$('#target_" . $data['larticle_id'] . "').val(),
						destination:$('#destination_" . $no . "').val(),
						link:$('#link_" . $data['larticle_id'] . "').val(),
						permalink:$('#permalink_" . $data['larticle_id'] . "').val(),
						tab:$('#activetab').val(),
						add_selected:'single'
					},function(data){
						if(data=='OK'){
							location=$('#curpage').val();
						}
					});
				});
			});
        </script>";
        
        $no++;
    }
    
    return $html;  
}

function get_published_post( $app_name = '' )
{
    global $db;
    
    $html = '';

    $destination = "";
    if(check_active_plugin_destination())
    {
        $destination = get_destination_list();
    }
    
    $pub_post = the_published_post( $app_name );
    $no = 0;
    while( $data = $db->fetch_array( $pub_post ) )
    {        
        $larticle_title = $data['larticle_title'];
        $type = $data['larticle_type'];
        
        $html .= "
        <li class=\"draggable\">
        	<div class=\"apps_item\">
        		<div class=\"apps_item_text\">" . $larticle_title . "</div>
        		<div class=\"add\" id=\"add_" . $data['larticle_id'] . "\">
        			<input type=\"checkbox\" name=\"selected_menu[]\" class=\"selected_menu\" value=\"" . $data['larticle_id'] . "\" />
        		</div>
        		<div class=\"view\" id=\"view_" . $data['larticle_id'] . "\"></div>
        	</div>
        	<div class=\"apps_details_item\" id=\"details_set_" . $data['larticle_id'] . "\" style=\"display:none;\">
        		<p>
        			<label>Label:</label>
        			<input type=\"text\" value=\"" . $larticle_title . "\" id=\"label_" . $data['larticle_id'] . "\" name=\"label[" . $data['larticle_id'] . "]\" class=\"medium_textbox\" />
        		</p>
        		<p>
        			<label>Target:</label>
        			<select id=\"target_" . $data['larticle_id'] . "\" name=\"target[" . $data['larticle_id'] . "]\">
        				<option value=\"_self\">Self</option>
        				<option value=\"_blank\">Blank</option>
        			</select>
                </p>";

            if(!empty($destination))
            {
                $html .= '
                    <p>
                        <label>Destination: </label>
                        <select id="destination_' . $no . '" name="destination[' . $no . ']">
                ';
                foreach($destination as $d)
                {
                    $html .= '
                        <option value="'.$d['sef'].'">'.$d['title'].'</option>
                    ';
                }
                $html .= '
                        </select>
                    </p>
                ';
            }
        
        $html .= "
        		<div style=\"text-align:right;\">
        			<input type=\"button\" id=\"add_to_menu_set_" . $data['larticle_id'] . "\" value=\"Add to menu set\" name=\"add[" . $data['larticle_id'] . "]\" class=\"button\" />
                </div>
                
        		<input type=\"hidden\" value=\"" . site_url() . '/' . $type . '/' .$data['lsef']. "\" id=\"link_" . $data['larticle_id'] . "\" name=\"link[" . $data['larticle_id'] . "]\" />
        		<input type=\"hidden\" value=\"" . site_url() . '/' . $type . '/' .$data['lsef'] . "/\" id=\"permalink_" . $data['larticle_id'] . "\" name=\"permalink[" . $data['larticle_id'] . "]\" />
        	</div>
        </li>
        <script type=\"text/javascript\">
        	jQuery(document).ready(function(){
				$('#view_" . $data['larticle_id'] . "').click(function(){
					$('#details_set_" . $data['larticle_id'] . "').slideToggle(100);
					return false;
				});

				$('#add_to_menu_set_" . $data['larticle_id'] . "').click(function(){
					$('#procces_alert').show();

					$.post('../lumonata-functions/menus.php',{
						label:$('#label_" . $data['larticle_id'] . "').val(),
						target:$('#target_" . $data['larticle_id'] . "').val(),
						destination:$('#destination_" . $no . "').val(),
						link:$('#link_" . $data['larticle_id'] . "').val(),
						permalink:$('#permalink_" . $data['larticle_id'] . "').val(),
						tab:$('#activetab').val(),
                        add_selected:'single',
                        type_post:'publish_post'
					},function(data){
						if(data=='OK'){
							location=$('#curpage').val();
						}
					});
				});
			});
		</script>";        
    }
    
    return $html;
}

function get_published_apps( $app_name = '' )
{    
    global $db;
    
    $html = '';
    
    $pub_apps = the_published_apps( $app_name );
    
    while( $data = $db->fetch_array( $pub_apps ) )
    {
        $lname = $data['lname'];
        
        $html .= "
        <li class=\"draggable\">
        	<div class=\"apps_item\">
        		<div class=\"apps_item_text\">" . $lname . "</div>
        		<div class=\"add\" id=\"add_" . $data['lrule_id'] . "\">
        			<input type=\"checkbox\" name=\"selected_menu[]\"  class=\"selected_menu\" value=\"" . $data['lrule_id'] . "\" />
        		</div>
        		<div class=\"view\" id=\"view_" . $data['lrule_id'] . "\"></div>
        	</div>
        	<div class=\"apps_details_item\" id=\"details_set_" . $data['lrule_id'] . "\" style=\"display:none;\">
        		<p>
        			<label>Label:</label>
        			<input type=\"text\" id=\"label_" . $data['lrule_id'] . "\" value=\"" . $lname . "\" name=\"label[" . $data['lrule_id'] . "]\" class=\"medium_textbox\" />
        		</p>
        		<p>
        			<label>Target:</label>
    				<select id=\"target_" . $data['lrule_id'] . "\" name=\"target[" . $data['lrule_id'] . "]\">
    					<option value=\"_self\">Self</option>
    					<option value=\"_blank\">Blank</option>
    				</select>
    			</p>
    			<div style=\"text-align:right;\">
    				<input id=\"add_to_menu_set_" . $data['lrule_id'] . "\" type=\"button\" value=\"Add to menu set\" name=\"add[" . $data['lrule_id'] . "]\" class=\"button\" />
    			</div>
    			<input type=\"hidden\" id=\"link_" . $data['lrule_id'] . "\" value=\"" . "/?app_name=" . $data['lgroup'] . "&amp;cat_id=" . $data['lrule_id'] . "\" name=\"link[" . $data['lrule_id'] . "]\" />
    			<input type=\"hidden\" id=\"permalink_" . $data['lrule_id'] . "\" value=\"" . $data['lgroup'] . "/" . $data['lsef'] . "/\" name=\"permalink[" . $data['lrule_id'] . "]\" />
    		</div>";
        
        	$active_tab = ( isset( $_GET['tab'] ) ) ? $_GET['tab'] : '';
        
        	$html .= "
        	<script type=\"text/javascript\">
        		jQuery(document).ready(function(){
					$('#view_" . $data['lrule_id'] . "').click(function(){
						$('#details_set_" . $data['lrule_id'] . "').slideToggle(100);
						return false;
					});

					$('#add_to_menu_set_" . $data['lrule_id'] . "').click(function(){
						$('#procces_alert').show();

						$.post('../lumonata-functions/menus.php',{
							label:$('#label_" . $data['lrule_id'] . "').val(),
							target:$('#target_" . $data['lrule_id'] . "').val(),
							link:$('#link_" . $data['lrule_id'] . "').val(),
							permalink:$('#permalink_" . $data['lrule_id'] . "').val(),
							tab:$('#activetab').val(),
							add_selected:'single'
						},function(data){
							if(data=='OK'){
								location=$('#curpage').val();
							}
						});
					});
				});
			</script>
		</li>";        
    }
    
    return $html;    
}


function get_destination_list()
{
    global $db;

    $q = $db->prepare_query("
        SELECT larticle_title, larticle_id, lsef
        FROM lumonata_articles
        WHERE larticle_type=%s AND larticle_status=%s
    ",
        "destinations", "publish"
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);
    
    $result = array();

    if($n > 0)
    {
        while($d = $db->fetch_array($r))
        {
            $title = $d['larticle_title'];
            $sef = $d['lsef'];

            $result[] = array(
                'title' => $title,
                'sef'   => $sef
            );
        }
    }

    return $result;
}

function get_published_post_archive( $apps = array() )
{
    global $db;
    
    $html    = '';
    $archive = the_post_archive();

    $destination = "";
    if(check_active_plugin_destination())
    {
        $destination = get_destination_list();
    }

    $no = 0;

    while( $data = $db->fetch_array( $archive ) )
    {
    	$type = $data['larticle_type'];
    	$name = array_key_exists( $type, $apps ) ? $apps[$type] : ucfirst($type);
        
        $html .= "
        <li class=\"draggable\">
        	<div class=\"apps_item\">
        		<div class=\"apps_item_text\">" . $name . "</div>
        		<div class=\"add\" id=\"add_" . $no . "\">
        			<input type=\"checkbox\" name=\"selected_menu[]\" class=\"selected_menu\" value=\"" . $no . "\" />
        		</div>
        		<div class=\"view\" id=\"view_" . $no . "\"></div>
        	</div>
        	<div class=\"apps_details_item\" id=\"details_set_" . $no . "\" style=\"display:none;\">
        		<p>
        			<label>Label:</label>
        			<input type=\"text\" value=\"" . $name . "\" id=\"label_" . $no . "\" name=\"label[" . $no . "]\" class=\"medium_textbox\" />
        		</p>
        		<p>
        			<label>Target:</label>
        			<select id=\"target_" . $no . "\" name=\"target[" . $no . "]\">
        				<option value=\"_self\">Self</option>
        				<option value=\"_blank\">Blank</option>
        			</select>
                </p>";

            if(!empty($destination))
            {
                $html .= '
                    <p>
                        <label>Destination: </label>
                        <select id="destination_' . $no . '" name="destination[' . $no . ']">
                ';
                foreach($destination as $d)
                {
                    $html .= '
                        <option value="'.$d['sef'].'">'.$d['title'].'</option>
                    ';
                }
                $html .= '
                        </select>
                    </p>
                ';
            }

            // $link = site_url() . '/' . $type;
            // $new_link = set_new_link_with_destination($link, 'seminyak');

            // print_r($new_link);
                
        $html .= "
        		<div style=\"text-align:right;\">
        			<input type=\"button\" id=\"add_to_menu_set_" . $no . "\" value=\"Add to menu set\" name=\"add[" . $no . "]\" class=\"button\" />
        		</div>
        		<input type=\"hidden\" value=\"". site_url() . '/' . $type . "/\" id=\"link_" . $no . "\" name=\"link[" . $no . "]\" />
        		<input type=\"hidden\" value=\"". site_url() . '/' . $type . "/\" id=\"permalink_" . $no . "\" name=\"permalink[" . $no . "]\" />
        	</div>
        </li>
        <script type=\"text/javascript\">
        	jQuery(document).ready(function(){
				$('#view_" . $no . "').click(function(){
					$('#details_set_" . $no . "').slideToggle(100);
					return false;
				});

				$('#add_to_menu_set_" . $no . "').click(function(){
					$('#procces_alert').show();

					$.post('../lumonata-functions/menus.php',{
						label:$('#label_" . $no . "').val(),
						target:$('#target_" . $no . "').val(),
						destination:$('#destination_" . $no . "').val(),
						link:$('#permalink_" . $no . "').val(),
						permalink:$('#permalink_" . $no . "').val(),
                        tab:$('#activetab').val(),
						add_selected:'single'
					},function(data){
						if(data=='OK'){
							location=$('#curpage').val();
						}
					});
				});
			});
		</script>";

		$no++; 
    }

    return $html;
}


function set_new_link_with_destination($link, $destination, $lang='', $type_post='')
{
    $explode_link = explode("/", $link);
    $count_explode = count($explode_link);

    if($type_post == "publish_post"){
        $link_three = $explode_link[2];
        $second_link = $explode_link[1];
        $last_link = $second_link.'/'.$link_three;
    }else{
        $last_link = $explode_link[1];
    }

    if(empty($lang))
    {
        $new_link = site_url().'/'.$destination.'/'.$last_link;
    }
    else
    {
        $new_link = site_url().'/'.$lang.'/'.$destination.'/'.$last_link;
    }
    
    return $new_link;
}

function get_custome_url()
{
    $html = "
    <li class=\"draggable\">
    	<div class=\"apps_details_item\" >
    		<p>
	    		<label>Label:</label>
	    		<input type=\"text\" value=\"\" id=\"label\" name=\"label\" class=\"medium_textbox\" />
    		</p>
    		<p>
    			<label>URL:</label>
    			<input type=\"text\" value=\"\" id=\"link\" name=\"link\" class=\"medium_textbox\" />
    			<i>( include the http:// )</i>
    		</p>
    		<p>
    			<label>Target:</label>
    			<select name=\"target\" id=\"target\">
    				<option value=\"_self\">Self</option>
    				<option value=\"_blank\">Blank</option>
    			</select>
    		</p>
    		<div style=\"text-align:right;\">
    			<input type=\"button\" value=\"Add to menu set\" id=\"add_to_menu_set\" name=\"add\" class=\"button\" />
    		</div>
    	</div>
    </li>
    <script type=\"text/javascript\">
    	jQuery(document).ready(function(){
			$('#add_to_menu_set').click(function(){
				$('#procces_alert').show();

				$.post('../lumonata-functions/menus.php',{
					label:$('#label').val(),
					target:$('#target').val(),
					link:$('#link').val(),
					permalink:$('#link').val(),
					tab:$('#activetab').val(),
                    add_selected:'single',
				},function(data){
                    console.log(data);
					if(data=='OK'){
						location=$('#curpage').val();
					}
				});
			});
		});
	</script>";
    
    return $html;    
}

function menus_javascript()
{
    $html = "
    <script type=\"text/javascript\">
		$(document).ready( function(){
			//-- reset all to unchecked
	        $('.selected_menu').each(function(){
	            $('.selected_menu').removeAttr('checked');
	        });

	        $('input[name=select_all]').click(function(){
	        	var selected_all=true;

	        	$('.selected_menu').each(function(){
	            	if(this.checked==false){
	            		this.checked=true;
	            		selected_all=false;
					}
	        	});

	        	if(selected_all){
	        		$('.selected_menu').each(function(){
		            	this.checked=false;
		        	});
				}
			});

			$('select[name=apps_set]').change(function(){
				$('#the_apps_lists').html('<img src=\"" . get_theme_img() . "/loader.gif\" />');
				var obj = new Object();
					obj.app_set_name   = $('select[name=apps_set]').val();
					obj.plug_post_type = new Array();";

					$apps = run_actions('plugin_menu_post_type');
					
					if( !empty( $apps ) )
					{
						foreach( $apps as $key => $val )
						{
							$html .= 'obj.plug_post_type.push("' . $key . '");';
						}
					}

				$html .= "
				$.post('../lumonata-functions/menus.php', obj, function(data){
					$('#the_apps_lists').html(data);
				});
			});
        });
    </script>";

    return $html;   
}

function the_menus( $args = '', $lang='', $check_lang=false )
{
    if( empty( $args ) )
    {
        return;
    }
    
    $var['menuset']    = ' ';
    $var['style']      = 'li';
    $var['show_title'] = "true";
    $var['addClass']   = 'theMenu';
    $var['addID']      = 'navMenu-' . time();
    
    if( !empty( $args ) )
    {
        $args = explode( '&', $args );
        
        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );
            
            if( $variable == 'menuset' || $variable == 'style' || $variable == 'show_title' || $variable == 'addClass' || $variable == 'addID' )
            {
                $var[$variable] = $value;
            }
        }
    }
    
    $menuset = get_meta_data( 'menu_set', 'menus' );
    $menuset = json_decode( $menuset, TRUE );
    
    if( !is_array( $menuset ) )
    {
        return;
    }
    
    foreach( $menuset as $key => $val )
    {
        if( strtolower( $val ) == strtolower( $var['menuset'] ) )
        {
            $menuset_key = $key;
            $set_name    = $val;
            break;
        }
    }
    
    if( !isset( $menuset_key ) )
    {
        return;
    }
    
    $menu_items = get_meta_data( 'menu_items_' . $menuset_key, 'menus' );
    $menu_items = json_decode( $menu_items, TRUE );
    
    $menu_order = get_meta_data( 'menu_order_' . $menuset_key, 'menus' );
    $menu_order = json_decode( $menu_order, TRUE );
    
    $return = '';
    
    if( $var['show_title'] == 'true' )
    {
        if( !empty( $var['set_name'] ) )
        {
            $return = '<h2>' . $var['set_name'] . '</h2>';
        }
        else
        {
            $return = '<h2>' . $set_name . '</h2>';
        }
    }
    
    $return .= fetch_menu_set_items( $menu_items, $menu_order, $var['style'], $var['addClass'], $var['addID'], $lang, $check_lang );
    
    return $return;
}

function the_page_menu( $home = true )
{    
    global $db;
    
    $query  = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE larticle_type='pages' AND larticle_status='publish' AND lshare_to=0 AND lsef != 'quick-facts' AND lsef != 'home' ORDER BY lorder" );    
    $result = $db->do_query( $query );
    
    $menu = "
    <ul>";
    
	    if( $home )
	    {
	        $menu .= "<li><a href=\"". HTTP . site_url() . "/\">Home</a></li>";
	    }
	    
	    while( $data = $db->fetch_array( $result ) )
	    {	        
	        if( is_permalink() )
	        {
	            $menu .= "<li><a href=\"". HTTP . site_url() . "/" . $data['lsef'] . "/\">" . $data['larticle_title'] . "</a></li>";
	        }	        
	        else
	        {
	            $menu .= "<li><a href=\"". HTTP . site_url() . "/?page_id=" . $data['larticle_id'] . "\">" . $data['larticle_title'] . "</a></li>";
	        }	        
	    }
    
    	$menu .= "
    </ul>";
    
    return $menu;    
}

function fetch_menu_set_items( $menu_items = array(), $menu_order = array(), $style, $addClass = '', $addID = '', $lang='', $check_lang=false )
{
    $return = '';
    $i      = 0;

    // echo count($menu_items);

    if( is_array( $menu_order ) )
    {
        $liOpen  = '';
        $liClose = '';
        $ulOpen  = '';
        $ulClose = '';
        
        if( $style == 'li' )
        {
            $return .= '' . $ulOpen . '<ul class="' . $addClass . '" id="' . $addID . '" >';
        }
        
        foreach( $menu_order as $key => $val )
        {
            $items_val = array_match( $menu_items, 'id', $val['id'] );

            if( is_permalink() )
            {
                if($check_lang)
                {
                    if( substr( $items_val[0]['pemalink_'.$lang], 0, 4 ) == "http" )
                    {
                        $link = $items_val[0]['pemalink_'.$lang];
                        $link = str_replace("http://", "", $link);
                        $link = str_replace("https://", "", $link);
                        $link = HTTP.$link;
                    }
                    else if( substr( $items_val[0]['pemalink_'.$lang], 0, 4 ) == "https" )
                    {
                        $link = $items_val[0]['pemalink_'.$lang];
                        $link = str_replace("https://", "", $link);
                        $link = str_replace("http://", "", $link);
                        $link = HTTP.$link;
                    }
                    else
                    {
                        $link = HTTP . SITE_URL . '/' . $items_val[0]['pemalink_'.$lang];
                        
                        $active_plugin = get_active_plugin( 'plugins' );
    
                        if( isset( $active_plugin['lumonata-destination-application'] ) )
                        {
                            $id = explode( '=', $items_val[0]['link_'.$lang] );
    
                            if( isset( $id[1] ) )
                            {
                                $dest = get_additional_field_relationship( $id[1], 'destination' );
                                
                                if( !empty( $dest ) )
                                {
                                    $link = HTTP . SITE_URL . '/' . $dest . '/' . $items_val[0]['pemalink_'.$lang];
                                }
                            }
                        }
                    }
                }
                else
                {
                    if( substr( $items_val[0]['permalink'], 0, 4 ) == "http" )
                    {
                        $link = $items_val[0]['permalink'];
                        $link = str_replace("http://", "", $link);
                        $link = str_replace("https://", "", $link);
                        $link = HTTP.$link;
                    }
                    else if( substr( $items_val[0]['permalink'], 0, 4 ) == "https" )
                    {
                        $link = $items_val[0]['permalink'];
                        $link = str_replace("https://", "", $link);
                        $link = str_replace("http://", "", $link);
                        $link = HTTP.$link;
                    }
                    else
                    {
                        $link = HTTP . SITE_URL . '/' . $items_val[0]['permalink'];
                        
                        $active_plugin = get_active_plugin( 'plugins' );
    
                        if( isset( $active_plugin['lumonata-destination-application'] ) )
                        {
                            $id = explode( '=', $items_val[0]['link'] );
    
                            if( isset( $id[1] ) )
                            {
                                $dest = get_additional_field_relationship( $id[1], 'destination' );
                                
                                if( !empty( $dest ) )
                                {
                                    $link = HTTP . SITE_URL . '/' . $dest . '/' . $items_val[0]['permalink'];
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if( substr( $items_val[0]['link'], 0, 4 ) == "http" )
                {
                    $link = $items_val[0]['link'];
                    $link = str_replace("http://", "", $link);
                    $link = str_replace("https://", "", $link);
                    $link = HTTP.$link;
                }
                else if( substr( $items_val[0]['link'], 0, 4 ) == "https" )
                {
                    $link = $items_val[0]['link'];
                    $link = str_replace("https://", "", $link);
                    $link = str_replace("http://", "", $link);
                    $link = HTTP.$link;
                }
                else
                {
                    $link = HTTP . THE_SITE_URL . $items_val[0]['link'];
                    
                    $active_plugin = get_active_plugin( 'plugins' );
                    if( isset( $active_plugin['lumonata-destination-application'] ) )
                    {
                        $id = explode( '=', $items_val[0]['link'] );
                        if( isset( $id[1] ) )
                        {
                            $dest = get_additional_field_relationship( $id[1], 'destination' );
                            
                            if( !empty( $dest ) )
                            {
                                $link = HTTP . SITE_URL . '/' . $dest . '/' . $items_val[0]['permalink'];
                            }
                        }
                    }
                }
            }
            
            // SET SELECTED MENU --
            $selected = ( is_active_menu( $link ) ? 'active' : '' );

            if( $style == 'li' )
            {
                $optional_css = $items_val[0]['css_class'];
                $cls = generateSefUrl( $items_val[0]['label'] );
                
                if( isset( $val['children'] ) && is_array( $val['children'] ) )
                {
                    $label = $items_val[0]['label'];
                    if($check_lang)
                    {
                        $label_lang = (isset($items_val[0]['label_'.$lang]) ? $items_val[0]['label_'.$lang] : '' );
                        $label = (empty($label_lang) ? $label : $label_lang);
                    }

                    $return .= "<li class='" . $selected . ' ' . $cls . "'>" . $liOpen . "<a href=\"" . $link . "\" class='".$optional_css."'>" . $label . "</a>";
                }
                else
                {
                    $label = $items_val[0]['label'];
                    if($check_lang)
                    {
                        $label_lang = (isset($items_val[0]['label_'.$lang]) ? $items_val[0]['label_'.$lang] : '' );
                        $label = (empty($label_lang) ? $label : $label_lang);
                    }
                    $return .= "<li class='" . $selected . ' ' . $cls . "'><a href=\"" . $link . "\" class='".$optional_css."'>" . $label . "</a>";
                }
            }
            else
            {
                $return .= "<div><a class='" . $addClass . "' href=\"" . $link . "\">" . $items_val[0]['label'] . "</a>";
            }
            
            if( isset( $val['children'] ) && is_array( $val['children'] ) )
            {
                if( $style == 'li' )
                {
                    $return .= fetch_menu_set_items( $menu_items, $val['children'], $style, '', '', $lang, $check_lang ) . "" . $liClose . "</li>";
                }
                else
                {
                    $return .= fetch_menu_set_items( $menu_items, $val['children'], $style, '', '', $lang, $check_lang ) . "</div>";
                }
            }
            else
            {
                if( $style == 'li' )
                {
                    $return .= "</li>";
                }
                else
                {
                    $return .= "</div>";
                }
            }
            
            $i++;
        }
        
        if( $style == 'li' )
        {
            $return .= "</ul>" . $ulClose;
        }
    }
    
    return $return;
}


function get_the_categories( $args = '' )
{    
    global $db;
    
    $var['order']         = 'ASC';
    $var['type']          = 'li';
    $var['app_name']      = ''; 
    $var['category_name'] = '';
    $var['sef']           = '';
    $var['parent_id']     = 0;

    if( !empty( $args ) )
    {        
        $args = explode( '&', $args );
        
        foreach( $args as $val )
        {            
            list( $variable, $value ) = explode( '=', $val );
            
            if( $variable == 'app_name' || $variable == 'parent_id' || $variable == 'type' || $variable == 'order' || $variable == 'category_name' || $variable == 'sef' )
            {                
                $var[$variable] = $value;
            }            
        }        
    }
    
    if( !empty( $var['category_name'] ) && !empty( $var['app_name'] ) )
    {        
        $query  = $db->prepare_query( "SELECT a.* FROM lumonata_rules a WHERE a.lname=%s AND a.lrule='categories' AND a.lgroup=%s AND", $var['category_name'], $var['app_name'] );
        $result = $db->do_query( $query );        
        $data   = $db->fetch_array( $result );
        
        $var['parent_id'] = $data['lrule_id'];        
    }
    elseif( !empty( $var['sef'] ) && !empty( $var['app_name'] ) )
    {
        $query  = $db->prepare_query( "SELECT a.* FROM lumonata_rules a WHERE lsef=%s AND lrule='categories' AND lgroup=%s AND", $var['sef'], $var['app_name'] );
        $result = $db->do_query( $query );        
        $data  = $db->fetch_array( $result );
        
        $var['parent_id'] = $data['lrule_id'];
    }
    
    return recursive_taxonomy( 0, 'categories', $var['app_name'], $var['type'], array(), $var['order'], $var['parent_id'], 0, true );
}


function get_language_datas()
{
    global $db;

    $q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
	$n = $db->num_rows($r);

    $data = array();
    if($n > 0)
    {
        while($d = $db->fetch_array($r))
        {
            $id             = $d['larticle_id'];
            $title          = ucwords($d['larticle_title']);
            $code           = strtolower(get_additional_field( $id, 'language_code', 'language' ));
            $status_default = get_additional_field( $id, 'status_default', 'language' );
            if($status_default != 1)
            {
                $data['code_lang'][]  = $code;
                $data['title_lang'][] = $title;
            }
        }
    }

    return $data;
}

?>