<?php
/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENAMPILKAN TITLE HERO PADA PAGE
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_data_page($appsef, $destination_id, $lang='', $check_lang=false, $string_translations='')
{
    global $db;

    // STRING TRANSLATIONS
    $translations = array('Accommodation', 'Dining', 'Spa');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $result = array();

    if($appsef == "about-us"  || $appsef == "press-download" || $appsef == "contact-us" ||  $appsef == "press-accolades")
    {
        
        $data_page = get_articles_data('pages', $appsef);
    }
    else if($appsef == "policy")
    {
        $data_page = "policy";
    }
    else
    {
        if($appsef == "press-gallery")
        {
            $appsef = "gallery";
        }

        $data_page = get_meta_data('post_type_setting', $appsef, $destination_id);
    }

    if(!empty($data_page))
    {
        if($appsef == "about-us" || $appsef == "press-download" || $appsef == "press-accolades" || $appsef == "contact-us")
        {

            // if($appsef == "gallery")
            // {
            //     $appsef = "press-gallery";
            // }

            $page_id     = $data_page[0]['id'];
            $title       = $data_page[0]['title'];
            $subtitle    = $data_page[0]['subtitle'];
            $description = $data_page[0]['desc'];
            $metatitle   = $data_page[0]['meta_title'];
            $metakey     = $data_page[0]['meta_keywords'];
            $metadesc    = $data_page[0]['meta_description'];

            if($check_lang)
            {
                $title_lang       = get_additional_field($page_id, 'title_'.$lang, 'pages');
                $subtitle_lang    = get_additional_field($page_id, 'subtitle_'.$lang, 'pages');
                $description_lang = get_additional_field($page_id, 'content_'.$lang, 'pages');
                $meta_title_lang  = get_additional_field($page_id, 'meta_title_'.$lang, 'pages');
                $meta_key_lang    = get_additional_field($page_id, 'meta_keywords_'.$lang, 'pages');
                $meta_desc_lang   = get_additional_field($page_id, 'meta_description_'.$lang, 'pages');
                
                $title       = (empty($title_lang) ? $title:             $title_lang);
                $subtitle    = (empty($subtitle_lang) ? $subtitle:       $subtitle_lang);
                $description = (empty($description_lang) ? $description: $description_lang);
                $metatitle   = (empty($meta_title_lang) ? $metatitle:    $meta_title_lang);
                $metakey     = (empty($meta_key_lang) ? $metakey:        $meta_key_lang);
                $metadesc    = (empty($meta_desc_lang) ? $metadesc:      $meta_desc_lang);
            }

            $bg_image    = get_featured_img( $page_id, 'pages', false );
            $bg_hero     = (!empty($bg_image) ? HTTP.site_url().$bg_image['large'] : "");
        }
        else if($appsef == "policy")
        {
	        $bg_image    = get_additional_field($destination_id, 'bg_image_policy','destinations');
            $title       = get_additional_field($destination_id, 'title_policy', 'destinations');
            $subtitle    = get_additional_field($destination_id, 'sub_title_policy', 'destinations');
            $description = get_additional_field($destination_id, 'description_policy', 'destinations');
            $metatitle   = get_additional_field($destination_id, 'meta_title_policy', 'destinations');
            $metakey     = get_additional_field($destination_id, 'meta_keywords_policy', 'destinations');
            $metadesc    = get_additional_field($destination_id, 'meta_description_policy', 'destinations');

            if($check_lang)
            {
                $title_lang       = get_additional_field($destination_id, 'title_policy_'.$lang, 'destinations');
                $subtitle_lang    = get_additional_field($destination_id, 'sub_title_policy_'.$lang, 'destinations');
                $description_lang = get_additional_field($destination_id, 'description_policy_'.$lang, 'destinations');

                $meta_title_lang  = get_additional_field($destination_id, 'meta_title_policy_'.$lang, 'destinations');
                $meta_key_lang    = get_additional_field($destination_id, 'meta_keywords_policy_'.$lang, 'destinations');
                $meta_desc_lang   = get_additional_field($destination_id, 'meta_description_policy_'.$lang, 'destinations');
                
                $title       = (empty($title_lang) ? $title:             $title_lang);
                $subtitle    = (empty($subtitle_lang) ? $subtitle:       $subtitle_lang);
                $description = (empty($description_lang) ? $description: $description_lang);
                $metatitle   = (empty($meta_title_lang) ? $metatitle:    $meta_title_lang);
                $metakey     = (empty($meta_key_lang) ? $metakey:        $meta_key_lang);
                $metadesc    = (empty($meta_desc_lang) ? $metadesc:      $meta_desc_lang);
            }

            $bg_hero = HTTP.SITE_URL.'/lumonata-plugins/destinations/bg_image_policy/'.$bg_image;
        }
        else
        {
            $post_type_setting = json_decode($data_page, true);
        
            $title             = $post_type_setting['title'];
            $description       = $post_type_setting['description'];
            $subtitle          = $post_type_setting['subtitle'];
            $metatitle         = $post_type_setting['meta_title'];
            $metakey           = $post_type_setting['meta_keywords'];
            $metadesc          = $post_type_setting['meta_description'];
           
            $pdf_file          = (isset($post_type_setting['pdf_file_spa_menu']) ? $post_type_setting['pdf_file_spa_menu']: '' );
            $code_rest_diary   = (isset($post_type_setting['code_rest_diary']) ? $post_type_setting['code_rest_diary'] : '' );
            // $subtitle          = $tr_string[$appsef];

            if($check_lang)
            {
                $title_lang             = (isset($post_type_setting['title_'.$lang]) ? $post_type_setting['title_'.$lang] : '' );
                $subtitle_lang          = (isset($post_type_setting['subtitle_'.$lang]) ? $post_type_setting['subtitle_'.$lang] : '');
                $description_lang       = (isset($post_type_setting['description_'.$lang]) ? $post_type_setting['description_'.$lang] : '');
                $meta_title_lang        = (isset($post_type_setting['meta_title_'.$lang]) ? $post_type_setting['meta_title_'.$lang] : '' );
                $meta_key_lang          = (isset($post_type_setting['meta_keywords_'.$lang]) ? $post_type_setting['meta_keywords_'.$lang] : '' );
                $meta_desc_lang         = (isset($post_type_setting['meta_description_'.$lang]) ? $post_type_setting['meta_description_'.$lang] : '' );
                $pdf_file_lang          = (isset($post_type_setting['pdf_file_spa_menu_'.$lang]) ? $post_type_setting['pdf_file_spa_menu_'.$lang] : '' );
    
                $title          = (empty($title_lang) ? $title:       $title_lang);
                $subtitle       = (empty($subtitle_lang) ? $subtitle: $subtitle_lang);
                $description    = (empty($description_lang) ? $description:   $description_lang);
                $metatitle      = (empty($meta_title_lang) ? $metatitle:   $meta_title_lang);
                $metakey        = (empty($meta_key_lang) ? $metakey:   $meta_key_lang);
                $metadesc       = (empty($meta_desc_lang) ? $metadesc:   $meta_desc_lang);
                $pdf_file       = (empty($pdf_file_lang) ? $pdf_file:   $pdf_file_lang);
            }

            $bg_image          = $post_type_setting['bg_image'];
            $url_bg            = get_url_hero_image_post_setting($bg_image);
            $bg_hero           = $url_bg['bg_image_large'];

            
            $result['pdf_file']        = $pdf_file;
            $result['code_rest_diary'] = $code_rest_diary;
        }

        $new_title = '<h1 class="text _text text-title-page-1">'.$title.'</h1>';

        if($appsef != "events" && $appsef != "policy")
        {
            if($appsef == "what-to-do-act-in")
            {
                if(strpos(strtolower($title), ' in '))
                {
                    $explode_title = explode(" in ", strtolower($title));
                    $title_1       = $explode_title[0]." IN";
                    $title_2       = $explode_title[1];
                    $new_title     = '<h1 class="text _text text-title-page-1">'.strtoupper($title_1).'<br/>'.strtoupper($title_2).'</h1>';
                }
            }
            elseif($appsef == "about-us")
            {
                if(strpos(strtolower($title), ' of '))
                {
                    $explode_title = explode(" of ", strtolower($title));
                    $title_1       = $explode_title[0]." OF";
                    $title_2       = $explode_title[1];
                    $new_title     = '<h1 class="text _text text-title-page-1">'.strtoupper($title_1).'<br/>'.strtoupper($title_2).'</h1>';
                }
            }
            else
            {
                $explode_title = explode(" ", $title);
                $title_1       = str_replace(" ".end($explode_title), "", $title);
                $title_2       = end($explode_title);
                $new_title     = '<h1 class="text _text text-title-page-1">'.$title_1.'<br/>'.$title_2.'</h1>';

                if($check_lang)
                {
                    $new_title     = '<h1 class="text _text text-title-page-1">'.$title.'</h1>';
                }
            }
        }

        $result['title_hero_template'] = '
            <div class="container container-title-page clearfix">
                <h3 class="text _text text-subtitle-page">'.$subtitle.'</h3>
                '.$new_title.'
                <div class="element line-title-hero-page"></div>
            </div>
        ';
        $result['bg_hero']  = $bg_hero;
        $result['og_image'] = $bg_hero;

        if($appsef == "events")
        {
            $title = $subtitle;
        }
        
         
            
        $result['title']       = $title;
        $result['description'] = $description;
        $result['subtitle']    = $subtitle;
        $result['type']        = $appsef;
        $result['metatitle']   = $metatitle;
        $result['metakey']     = $metakey;
        $result['metadesc']    = $metadesc;
    }

    $villa_reservation    = get_additional_field($destination_id, 'villa_reservation', 'destinations');
    $restaurant_reservation    = get_additional_field($destination_id, 'restaurant_reservation', 'destinations');
    $guest_reservation    = get_additional_field($destination_id, 'guest_reservation', 'destinations');
    
        $wa_room = isset( $villa_reservation ) && !empty( $villa_reservation ) ? $villa_reservation : '';
    $wa_restaurant = isset( $restaurant_reservation ) && !empty( $restaurant_reservation ) ? $restaurant_reservation : '';
    $wa_inhouse_guest = isset( $guest_reservation  ) && !empty( $guest_reservation  ) ? $guest_reservation  : '';
    
        if( !empty( $wa_room ) || !empty( $wa_restaurant ) ||  !empty( $wa_inhouse_guest ) ) {
        $wa_html = '<div class="floating_wa">
            <a>
                <div class="bg-[#A6631B] rounded-[50%] p-4 ml-4 mr-4 overflow-hidden flex justify-center align-center"><div class="font-bold text-white flex justify-center align-center"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="40" height="60" viewBox="0,0,256,256"
style="fill:#000000;">
<g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.12,5.12)"><path d="M25,2c-12.69047,0 -23,10.30953 -23,23c0,4.0791 1.11869,7.88588 2.98438,11.20898l-2.94727,10.52148c-0.09582,0.34262 -0.00241,0.71035 0.24531,0.96571c0.24772,0.25536 0.61244,0.35989 0.95781,0.27452l10.9707,-2.71875c3.22369,1.72098 6.88165,2.74805 10.78906,2.74805c12.69047,0 23,-10.30953 23,-23c0,-12.69047 -10.30953,-23 -23,-23zM25,4c11.60953,0 21,9.39047 21,21c0,11.60953 -9.39047,21 -21,21c-3.72198,0 -7.20788,-0.97037 -10.23828,-2.66602c-0.22164,-0.12385 -0.48208,-0.15876 -0.72852,-0.09766l-9.60742,2.38086l2.57617,-9.19141c0.07449,-0.26248 0.03851,-0.54399 -0.09961,-0.7793c-1.84166,-3.12289 -2.90234,-6.75638 -2.90234,-10.64648c0,-11.60953 9.39047,-21 21,-21zM16.64258,13c-0.64104,0 -1.55653,0.23849 -2.30859,1.04883c-0.45172,0.48672 -2.33398,2.32068 -2.33398,5.54492c0,3.36152 2.33139,6.2621 2.61328,6.63477h0.00195v0.00195c-0.02674,-0.03514 0.3578,0.52172 0.87109,1.18945c0.5133,0.66773 1.23108,1.54472 2.13281,2.49414c1.80347,1.89885 4.33914,4.09336 7.48633,5.43555c1.44932,0.61717 2.59271,0.98981 3.45898,1.26172c1.60539,0.5041 3.06762,0.42747 4.16602,0.26563c0.82216,-0.12108 1.72641,-0.51584 2.62109,-1.08203c0.89469,-0.56619 1.77153,-1.2702 2.1582,-2.33984c0.27701,-0.76683 0.41783,-1.47548 0.46875,-2.05859c0.02546,-0.29156 0.02869,-0.54888 0.00977,-0.78711c-0.01897,-0.23823 0.0013,-0.42071 -0.2207,-0.78516c-0.46557,-0.76441 -0.99283,-0.78437 -1.54297,-1.05664c-0.30567,-0.15128 -1.17595,-0.57625 -2.04883,-0.99219c-0.8719,-0.41547 -1.62686,-0.78344 -2.0918,-0.94922c-0.29375,-0.10568 -0.65243,-0.25782 -1.16992,-0.19922c-0.51749,0.0586 -1.0286,0.43198 -1.32617,0.87305c-0.28205,0.41807 -1.4175,1.75835 -1.76367,2.15234c-0.0046,-0.0028 0.02544,0.01104 -0.11133,-0.05664c-0.42813,-0.21189 -0.95173,-0.39205 -1.72656,-0.80078c-0.77483,-0.40873 -1.74407,-1.01229 -2.80469,-1.94727v-0.00195c-1.57861,-1.38975 -2.68437,-3.1346 -3.0332,-3.7207c0.0235,-0.02796 -0.00279,0.0059 0.04687,-0.04297l0.00195,-0.00195c0.35652,-0.35115 0.67247,-0.77056 0.93945,-1.07812c0.37854,-0.43609 0.54559,-0.82052 0.72656,-1.17969c0.36067,-0.71583 0.15985,-1.50352 -0.04883,-1.91797v-0.00195c0.01441,0.02867 -0.11288,-0.25219 -0.25,-0.57617c-0.13751,-0.32491 -0.31279,-0.74613 -0.5,-1.19531c-0.37442,-0.89836 -0.79243,-1.90595 -1.04102,-2.49609v-0.00195c-0.29285,-0.69513 -0.68904,-1.1959 -1.20703,-1.4375c-0.51799,-0.2416 -0.97563,-0.17291 -0.99414,-0.17383h-0.00195c-0.36964,-0.01705 -0.77527,-0.02148 -1.17773,-0.02148zM16.64258,15c0.38554,0 0.76564,0.0047 1.08398,0.01953c0.32749,0.01632 0.30712,0.01766 0.24414,-0.01172c-0.06399,-0.02984 0.02283,-0.03953 0.20898,0.40234c0.24341,0.57785 0.66348,1.58909 1.03906,2.49023c0.18779,0.45057 0.36354,0.87343 0.50391,1.20508c0.14036,0.33165 0.21642,0.51683 0.30469,0.69336v0.00195l0.00195,0.00195c0.08654,0.17075 0.07889,0.06143 0.04883,0.12109c-0.21103,0.41883 -0.23966,0.52166 -0.45312,0.76758c-0.32502,0.37443 -0.65655,0.792 -0.83203,0.96484c-0.15353,0.15082 -0.43055,0.38578 -0.60352,0.8457c-0.17323,0.46063 -0.09238,1.09262 0.18555,1.56445c0.37003,0.62819 1.58941,2.6129 3.48438,4.28125c1.19338,1.05202 2.30519,1.74828 3.19336,2.2168c0.88817,0.46852 1.61157,0.74215 1.77344,0.82227c0.38438,0.19023 0.80448,0.33795 1.29297,0.2793c0.48849,-0.05865 0.90964,-0.35504 1.17773,-0.6582l0.00195,-0.00195c0.3568,-0.40451 1.41702,-1.61513 1.92578,-2.36133c0.02156,0.0076 0.0145,0.0017 0.18359,0.0625v0.00195h0.00195c0.0772,0.02749 1.04413,0.46028 1.90625,0.87109c0.86212,0.41081 1.73716,0.8378 2.02148,0.97852c0.41033,0.20308 0.60422,0.33529 0.6543,0.33594c0.00338,0.08798 0.0068,0.18333 -0.00586,0.32813c-0.03507,0.40164 -0.14243,0.95757 -0.35742,1.55273c-0.10532,0.29136 -0.65389,0.89227 -1.3457,1.33008c-0.69181,0.43781 -1.53386,0.74705 -1.8457,0.79297c-0.9376,0.13815 -2.05083,0.18859 -3.27344,-0.19531c-0.84773,-0.26609 -1.90476,-0.61053 -3.27344,-1.19336c-2.77581,-1.18381 -5.13503,-3.19825 -6.82031,-4.97266c-0.84264,-0.8872 -1.51775,-1.71309 -1.99805,-2.33789c-0.4794,-0.62364 -0.68874,-0.94816 -0.86328,-1.17773l-0.00195,-0.00195c-0.30983,-0.40973 -2.20703,-3.04868 -2.20703,-5.42578c0,-2.51576 1.1685,-3.50231 1.80078,-4.18359c0.33194,-0.35766 0.69484,-0.41016 0.8418,-0.41016z"></path></g></g>
</svg></div></div></a></div>
<div class="wrap_wa_list">
    <ul>
';
        
        if( !empty( $wa_room ) ) {
            $wa_html .= '<li><a  href="https://wa.me/'. $wa_room .'" target="_blank">Villa Reservation</a></li>';
        }
        
        if( !empty( $wa_restaurant ) ) {
             $wa_html .= '<li><a  href="https://wa.me/'. $wa_restaurant .'" target="_blank">Restaurant Reservation</a></li>';
        }
        
        if( !empty( $wa_inhouse_guest ) ) {
            $wa_html .= '<li><a  href="https://wa.me/'. $wa_inhouse_guest .'" target="_blank">In-house Guest Inquiry</a></li>';
        }

        $wa_html .= '</ul></div>';
        
         add_variable('wa_numb', $wa_html);
    }
    
    return $result;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function page_template($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations='')
{
    global $db;
    $version = attemp_actions('version_js_css');
    
    // STRING TRANSLATIONS
    $translations = array('Single', 'King Size', 'Double', 'Double Twin', 'Villa Size', 'Bed Type', 'Maximum Capacity', 'Pool Size', 'Villa Benefits', 'Book Now', 'Reserve Table', 'Details', 'Spa Menu', 'Explore', 'Make an Inquiry', 'Reserve Spa');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    set_template( TEMPLATE_PATH . '/template/page.html', 'page_template' );
    add_block( 'acco-block', 'ablock', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'loop-list-only-spa-block', 'llosblock', 'page_template' );
    add_block( 'page-list-only-spa-block', 'plosblock', 'page_template' );
    add_block( 'page-list-block', 'plblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    $title_page       = $data_page['title'];
    $description_page = $data_page['description'];
    $code_rest_diary  = $data_page['code_rest_diary'];
    
    //add_actions( 'code_rest_diary_page_list', $code_rest_diary );
    
    add_variable( 'title_page', $title_page );
    add_variable( 'description_page', $description_page );

    $appsef        = get_uri_sef();
    $product       = get_post_list($appsef, '', '', $destination);
    $key           = 1;
    $nav_li        = "";
    $option_spa    = "";
    $option_events = "";


    // $page_url = HTTP . site_url().'/'.$destination.'/'.$appsef.'/';
    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$appsef.'/' : HTTP . site_url().'/'.$destination.'/'.$appsef.'/');

    config_meta_data($page_url, $data_page);

    if($appsef == "dining")
    {
        if($destination=="seminyak")
        {
            $dining_link = 'https://www.resdiary.com/restaurant/breezesamaya';
        }
        else
        {
            $dining_link = 'https://www.resdiary.com/restaurant/sweptawaysamaya';
        }
    }


    if(count($product) > 0)
    {
        
        add_actions( 'list_link_product_hero', 'make_list_product_hero', $product, $lang, $check_lang );

        foreach($product as $d)
        {
            $post_id         = $d['post_id'];
            $post_title      = $d['post_title'];
            $post_brief      = $d['post_brief'];
            $post_content    = $d['post_content'];
            $post_sef        = $d['post_sef'];
            $post_attachment = $d['post_attachment'];
            $post_link       = HTTP . site_url().'/'.$destination.'/'.$appsef.'/'.$post_sef.'.html';
            $post_gallery    = set_post_gallery_html($post_attachment);
            $nav_li          .= '<li class="' . ( $key == 1 ? 'active': '' ) . '"><a href="#'.$post_sef.'"></a></li>';
            $class_scroll    = "paralax-$key";
            $featured_image  = HTTP.site_url().'/'.$d['post_featured_img']['large'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, $appsef);
                $post_brief_lang = get_additional_field($post_id, 'brief_'.$lang, $appsef);
                
                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_brief      = (empty($post_brief_lang) ? $post_brief: $post_brief_lang);
                $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$appsef.'/'.$post_sef.'.html';
            }

        
            // echo strtolower($post_title);
            if($appsef!="spa")
            {
                if(strpos(strtolower($post_title), ' of '))
                {
                    $post_title = explode_title_by_at_of(" of ", strtolower($post_title));
                }
                elseif(strpos(strtolower($post_title), ' at '))
                {
                    $post_title = explode_title_by_at_of(" at ", strtolower($post_title));
                }
            }

            if($appsef=="accommodation")
            {
                
                $size         = get_additional_field( $post_id, 'size', $appsef );
                // $occupancy = get_additional_field( $post_id, 'occupancy', $appsef );
                $bed          = get_additional_field( $post_id, 'bed', $appsef );
                $pool_size    = get_additional_field( $post_id, 'pool_size', $appsef );

                if($check_lang)
                {
                    $str_replace_bed = strtolower(str_replace("/ ", "", $bed));
                    $str_replace_bed = strtolower(str_replace(" ", "_", $str_replace_bed));
                    $bed_lang        = $tr_string[$str_replace_bed];
                    $size_lang       = get_additional_field( $post_id, 'size_'.$lang, $appsef );
                    // $occupancy_lang  = get_additional_field( $post_id, 'occupancy_'.$lang, $appsef );
                    $pool_size_lang  = get_additional_field($post_id, 'pool_size_'.$lang, $appsef);

                    $size         = (empty($size_lang) ? $size:           $size_lang);
                    $pool_size    = (empty($pool_size_lang) ? $pool_size: $pool_size_lang);
                    // $occupancy = (empty($occupancy_lang) ? $occupancy: $occupancy_lang);
                    $bed          = (empty($bed_lang) ? $bed:             $bed_lang);
                }

                $villa_details = "";
                if(!empty($size))
                {
                    $villa_details .= '
                        <div class="container container-detail-acco clearfix">
                            <p class="text _text text-label">'.$tr_string['villa_size'].'</p>
                            <p class="text _text text-value">'.$size.'</p>
                        </div>
                    ';
                }
                if(!empty($bed))
                {
                    $villa_details .= '
                        <div class="container container-detail-acco clearfix">
                            <p class="text _text text-label">'.$tr_string['bed_type'].'</p>
                            <p class="text _text text-value">'.$bed.'</p>
                        </div>
                    ';
                }
                if(!empty($pool_size))
                {
                    $villa_details .= '
                        <div class="container container-detail-acco clearfix">
                            <p class="text _text text-label">'.$tr_string['pool_size'].'</p>
                            <p class="text _text text-value">'.$pool_size.'</p>
                        </div>
                    ';
                }

                add_variable('villa_details', $villa_details);
    
                parse_template('acco-block', 'ablock', false);
            }
            
            $link_details = empty($post_content) ? '' : '<a href="'.$post_link.'" class="text link-detail-list-product">'.$tr_string['details'].'</a>';
            $fleft_on = empty($post_content) ? 'fleft' : '';

            $link_details_spa = "";
            if($appsef == "spa")
            {
                $treatment_pdf = get_additional_field( $post_id, 'treatment_pdf', 'spa' );
                $link_pdf      = $post_link;

                if(!empty($treatment_pdf) && file_exists( PLUGINS_PATH . '/treatments/pdf/' . $treatment_pdf ))
                {
                    $link_pdf = URL_PLUGINS . 'treatments/pdf/' . $treatment_pdf;
                }

                $option_spa  .= '<option value="'.$post_id.'">'.ucwords(strtolower($post_title)).'</option>';
                $link_details = '<a href="'.$link_pdf.'" class="text link-detail-list-product" target="_blank">'.$tr_string['spa_menu'].'</a>';
                $link_details_spa = empty($post_content) ? '' : '<p class="text _text list-item-text-explore" data-aos="fade-up">'.$tr_string['explore'].'</p>';

                $post_link = empty($post_content) ? '#' : $post_link;
            }

            add_variable('post_title', $post_title);
            add_variable('post_brief', $post_brief);
            add_variable('post_gallery', $post_gallery);
            add_variable('post_link', $post_link);
            add_variable('container_id', $post_sef);
            add_variable('class_scroll', $class_scroll);
            add_variable('link_details', $link_details);
            add_variable('link_details_spa', $link_details_spa);
            add_variable('featured_image', $featured_image);

            // SET BASE64
            $val_array = array(
                'key'   => $post_id,
                'title' => $post_title
            );
            $values_json = base64_encode( json_encode( $val_array ) );
            add_variable('values_json', $values_json);

            if($appsef == "events")
            {
                $option_events  .= '<option value="'.ucwords(strtolower($post_title)).'">'.ucwords(strtolower($post_title)).'</option>';
                // $button_book_now_list = '
                //     <a href="'.$post_link.'" class="text _text text-book-now '.$fleft_on.'"><p>'.$tr_string['book_now'].'</p> </a>
                // ';
                $button_book_now_list = '';
            }
            elseif($appsef == "dining") 
            {
                $button_book_now_list = "";
                $code_rest_diary    = get_additional_field( $post_id, 'code_rest_diary', 'dining' );
                if(!empty($code_rest_diary))
                {
                    $button_book_now_list = '
                        <a href="'.$post_link.'" class="text _text text-book-now '.$fleft_on.'"><p>'.$tr_string['reserve_table'].'</p> </a>
                    ';
                }
            }
            else
            {
                $button_book_now_list = '
                    <div class="'.$appsef.'_form_book">
                    <button data-title="'. $post_title .'" class="text _text text-book-now '.$fleft_on.'" data-type="'.$appsef.'" data-key="'.$values_json.'"><p>'.$tr_string['book_now'].'</p> </button></div>
                ';
            }
            add_variable('button_book_now_list', $button_book_now_list);
            
            if($appsef != "spa")
            {
                parse_template('loop-list-block', 'llblock', true);
            }
            else
            {
                parse_template('loop-list-only-spa-block', 'llosblock', true);
            }
            $key++;
        }
        add_variable('nav_li', $nav_li);
    }

    $destination_id = $corporatedata[0]['id'];

    if($appsef == "spa")
    {
        $pdf_file_spa_menu = isset($data_page['pdf_file']) ? $data_page['pdf_file'] : '';

        $folder_lang = "";
        if($check_lang)
        {
            $folder_lang = $lang.'/';
        }

        if(!empty($pdf_file_spa_menu) && file_exists(PLUGINS_PATH . '/custom-post/pdf/'.$folder_lang.$pdf_file_spa_menu))
        {
            $download_spa_menu_link = URL_PLUGINS . 'custom-post/pdf/'.$folder_lang.$pdf_file_spa_menu;
            $download_spa_menu_link = '<a class="spa-menu-pdf-link" href="'.$download_spa_menu_link.'" target="_blank"><span>'.$tr_string['spa_menu'].'</span></a>';
            add_variable( 'download_spa_menu_link', $download_spa_menu_link );
        }
        
        add_actions( 'booking_popup_spa', 'booking_popup_spa_content', $destination, $destination_id );
        add_actions( 'option_spa', $option_spa );
        add_actions( 'lang', $lang );
        add_actions( 'check_lang', $check_lang );

        $button_book_hero = '
            <button class="container container-button-book-now book-now-hero clearfix" data-key="">
                <p class="text _text text-book-now">'.$tr_string['reserve_spa'].'</p>
            </button>
        ';

        add_actions('button_book_hero', $button_book_hero);
        // add_actions( 'booking_popup_dinings', 'booking_popup_dinings_content' );
        add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
        add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );
    }
    elseif($appsef == "weddings")
    {
        add_actions( 'booking_popup_wedding', 'booking_popup_wedding_content', $destination, $destination_id );
        add_actions( 'option_spa', $option_spa );
        add_actions( 'lang', $lang );
        add_actions( 'check_lang', $check_lang );
    }
    elseif($appsef == "events")
    {
        add_actions( 'option_events', $option_events );
        $button_book_hero = '
            <button class="container container-button-book-now book-now-hero clearfix" data-key="">
                <p class="text _text text-book-now">'.$tr_string['book_now'].'</p>
            </button>
        ';
        add_actions('button_book_hero', $button_book_hero);
        add_actions( 'booking_popup_events', 'booking_popup_events_content' );
        add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
        add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );

        $book_now_under_description = '<a href="#" class="btn-book-now-events">'.$tr_string['make_an_inquiry'].'</a>';
        add_variable('book_now_under_description', $book_now_under_description);
    }
    elseif($appsef == "dining")
    {
        // BUTTON BOOK NOW
        $button_book_hero = '
            <a href="'.$dining_link.'" class="container container-button-book-now-link book-now-hero clearfix" target="_blank">
                <p class="text _text text-book-now">'.$tr_string['reserve_table'].'</p>
            </a>
        ';

        add_actions('button_book_hero', $button_book_hero);
        // add_actions( 'booking_popup_dinings', 'booking_popup_dinings_content' );

        $html_rest_diary = '<div id="rd-widget-frame" style="max-width: 600px; margin: auto;">' . $data_page['code_rest_diary'] . '</div>';
        add_variable('html_rest_diary', $html_rest_diary);
    }
   
    add_variable('appsef', $appsef);

    if($appsef != "spa")
    {
        parse_template( 'page-list-block', 'plblock', false ); 
    }
    else
    {
        parse_template( 'page-list-only-spa-block', 'plosblock', false );
    }

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}



/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT BUTTON LIST PRODUCT UNTUK DI TAMPILKAN PADA HERO
| -------------------------------------------------------------------------------------------------------------------------
*/
function make_list_product_hero($product, $lang='', $check_lang=false)
{
    $appsef  = get_uri_sef();
    
    $html = '
    <div class="wrap-list-product-link">
        <ul>';
        foreach($product as $d)
        {
            $post_id         = $d['post_id'];
            $post_title      = $d['post_title'];
            $post_sef        = $d['post_sef'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, $appsef);
                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
            }

            $html .='<li data-filter="#'.$post_sef.'"><div class="bullet"></div><span>'.$post_title.'</span></li>';
        }

    $html .= '
        </ul>
    </div>
    ';

    return $html;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function offers_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations='')
{
    $version = attemp_actions('version_js_css');
    
    set_template( TEMPLATE_PATH . '/template/offers.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // STRING TRANSLATIONS
    $translations = array('Book Now');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];
    $type        = $data_page['type'];
    $product     = get_post_list('special-offers', '', '', $destination);

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);


    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/special-offers/' : HTTP . site_url().'/'.$destination.'/special-offers/');
    config_meta_data($page_url, $data_page);

    if(count($product) > 0)
    {
        foreach($product as $d)
        {
            $post_id         = $d['post_id'];
            $post_title      = $d['post_title'];
            $post_brief      = $d['post_brief'];
            $post_sef        = $d['post_sef'];
            $booking_link    = get_additional_field($post_id, 'booking_link', 'special-offers');
            $label_link      = get_additional_field($post_id, 'label_link', 'special-offers');
            $form_type       = get_additional_field($post_id, 'form_type', 'special-offers');
            $label_link      = ((empty($label_link)) ? $tr_string['book_now'] : $label_link);
            $post_link       = HTTP . site_url().'/'.$destination.'/special-offers/'.$post_sef.'.html';

            $form_type = (empty($form_type) ? 2 : $form_type );
            
            $featured_image  = get_featured_img( $post_id, 'special-offers', false );
            $featured_image  = HTTP . site_url().$featured_image['medium'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, 'special-offers');
                $post_brief_lang = get_additional_field($post_id, 'brief_'.$lang, 'special-offers');
                
                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_brief      = (empty($post_brief_lang) ? $post_brief: $post_brief_lang);
                $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/special-offers/'.$post_sef.'.html';
            }

            if(empty($booking_link))
            {
                $button_book = '<button class="book-now-item" data-title="'.ucwords(strtolower($post_title)).'" data-form_type="'.$form_type.'">'.$label_link.'</button>';
            }
            else
            {
                $button_book = '<a href="'.$booking_link.'" class="book-now-item-link">'.$label_link.'</a>';
            }
            
            // $permalink = HTTP . site_url().'/'.$destination.'/offers/'.$sef_offer.'.html';

            add_variable('post_title', $post_title);
            add_variable('post_link', $post_link);
            add_variable('post_brief', $post_brief);
            add_variable('button_book', $button_book);
            add_variable('featured_image', $featured_image);
            
            parse_template('loop-list-block', 'llblock', true);
        }
    }

    add_actions( 'booking_popup_offers', 'booking_popup_offers_content' );
    add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
    add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK GIFT CARD TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function gift_card_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations='')
{
    $version = attemp_actions('version_js_css');
    
    set_template( TEMPLATE_PATH . '/template/gift_card.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // STRING TRANSLATIONS
    $translations = array('Book Now');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];
    $type        = $data_page['type'];
    $product     = get_post_list('e-gift-card', '', '', $destination);

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);


    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/e-gift-card/' : HTTP . site_url().'/'.$destination.'/e-gift-card/');
    config_meta_data($page_url, $data_page);

    if(count($product) > 0)
    {
        foreach($product as $d)
        {
            $post_id         = $d['post_id'];
            $post_title      = $d['post_title'];
            $post_brief      = $d['post_brief'];
            $post_sef        = $d['post_sef'];
            $booking_link    = get_additional_field($post_id, 'booking_link', 'e-gift-card');
            $label_link      = get_additional_field($post_id, 'label_link', 'e-gift-card');
            $form_type       = get_additional_field($post_id, 'form_type', 'e-gift-card');
            $label_link      = ((empty($label_link)) ? $tr_string['book_now'] : $label_link);
            $post_link       = HTTP . site_url().'/'.$destination.'/e-gift-card/'.$post_sef.'.html';

            $form_type = (empty($form_type) ? 2 : $form_type );
            
            $featured_image  = get_featured_img( $post_id, 'e-gift-card', false );
            $featured_image  = HTTP . site_url().$featured_image['medium'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, 'e-gift-card');
                $post_brief_lang = get_additional_field($post_id, 'brief_'.$lang, 'e-gift-card');
                
                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_brief      = (empty($post_brief_lang) ? $post_brief: $post_brief_lang);
                $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/e-gift-card/'.$post_sef.'.html';
            }

            if(empty($booking_link))
            {
                $button_book = '<button class="book-now-item" data-title="'.ucwords(strtolower($post_title)).'" data-form_type="'.$form_type.'">'.$label_link.'</button>';
            }
            else
            {
                $button_book = '<a href="'.$booking_link.'" class="book-now-item-link">'.$label_link.'</a>';
            }
            
            // $permalink = HTTP . site_url().'/'.$destination.'/offers/'.$sef_offer.'.html';

            add_variable('post_title', $post_title);
            add_variable('post_link', $post_link);
            add_variable('post_brief', $post_brief);
            add_variable('button_book', $button_book);
            add_variable('featured_image', $featured_image);
            
            parse_template('loop-list-block', 'llblock', true);
        }
    }

    add_actions( 'booking_popup_offers', 'booking_popup_offers_content' );
    add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
    add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE NEWS CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function news_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/news.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // STRING TRANSLATIONS
    $translations = array('Download', 'Read More');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];
    $type        = $data_page['type'];

    $press_type = "";
    if($type == "news")
    {
        $where_add = " AND a.lstatus_accolades=0";
        $product     = get_post_list('news', '', $where_add, $destination);
        $press_type = "news";
    }
    elseif($type == "press-download")
    {
        $where_add = " AND a.lstatus_file_download=1 AND a.lstatus_accolades=0";
        $product     = get_post_list('news', '', $where_add, $destination);
        $press_type = "press-download";
    }

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);


    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$type.'/' : HTTP . site_url().'/'.$destination.'/'.$type.'/');
    config_meta_data($page_url, $data_page);

    if(!empty($product))
    {
        foreach($product as $d)
        {
            $post_id        = $d['post_id'];
            $post_title     = $d['post_title'];
            $post_brief     = $d['post_brief'];
            $post_sef       = $d['post_sef'];
            $post_date      = date("M d Y", strtotime($d['post_date']));
            $post_link      = HTTP . site_url().'/'.$destination.'/'.$type.'/'.$post_sef.'.html';
            $featured_image = get_featured_img( $post_id, 'news', false );
            $featured_image = HTTP . site_url().$featured_image['medium'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, 'news');
                $post_brief_lang = get_additional_field($post_id, 'brief_'.$lang, 'news');
                
                $post_title = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_brief = (empty($post_brief_lang) ? $post_brief: $post_brief_lang);
                $post_link  = HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$type.'/'.$post_sef.'.html';
            }
            

            $button = "";
            if($press_type == "news")
            {
                $button = '
                    <div class="detail-btn-item">
                        <a href="'.$post_link.'">'.$tr_string['read_more'].'</a>
                    </div>
                ';
            }
            elseif($press_type == "press-download")
            {
                $file_download = get_additional_field( $post_id, 'file_download', 'news' );

                if( !empty( $file_download ) && file_exists( PLUGINS_PATH . '/additional/file_download/' . $file_download ) )
                {
                    $button = '
                        <div class="detail-download-item">
                            <a target="_blank" href="'.URL_PLUGINS . 'additional/file_download/' . $file_download.'">'.$tr_string['download'].'</a>
                        </div>
                    ';
                }
            }

            add_variable('post_title', $post_title);
            add_variable('post_date', $post_date);
            add_variable('post_brief', $post_brief);
            add_variable('featured_image', $featured_image);
            add_variable('button', $button);
            add_variable('post_link', $post_link);
            
            parse_template('loop-list-block', 'llblock', true);
        }
    }

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE PRESS ACCOLADES CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function accolades_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/accolades.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);

    $where_add = " AND a.lstatus_accolades=1";
    $product     = get_post_list('news', '', $where_add, $destination);

    if(!empty($product))
    {
        foreach($product as $d)
        {
            $post_id        = $d['post_id'];
            $post_title     = $d['post_title'];
            $post_brief     = $d['post_brief'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, 'news');
                $post_brief_lang = get_additional_field($post_id, 'content_'.$lang, 'news');
                
                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_brief      = (empty($post_brief_lang) ? $post_brief: $post_brief_lang);
            }

            $featured_image = get_featured_img( $post_id, 'news', false );
            $featured_image = HTTP . site_url().$featured_image['medium'];

            
            add_variable('post_title', $post_title);
            add_variable('post_brief', $post_brief);
            add_variable('featured_image', $featured_image);

            parse_template('loop-list-block', 'llblock', true);
        }
    }

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );

}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE PRESS GALLERY CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function press_gallery_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations='')
{
    global $db;
    $version = attemp_actions('version_js_css');

    set_template( TEMPLATE_PATH . '/template/press_gallery.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // STRING TRANSLATIONS
    $translations = array('All Photos', 'Photo', 'Video', 'Photo 360');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];
    $type        = $data_page['type'];

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);

    add_variable( 'HTTP', HTTP );
    add_variable( 'web_url', HTTP.site_url() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/press-gallery/' : HTTP . site_url().'/'.$destination.'/press-gallery/');
    config_meta_data($page_url, $data_page);

    add_variable('page_url', $page_url);


    // GET CATEGORY GALLERY
    $category      = get_rule_list_front("gallery", "categories", $lang, $check_lang, false, true);
    if(!empty($category))
    {
        $category_html = "<ul>";
        // $category_html .= "<li data-filter='all' data-text='".$tr_string['all_photos']."'>".$tr_string['all_photos']."</li>";

        foreach($category as $dc)
        {
            if(get_count_rule_article($dc['id'], $destination) > 0)
            {
                $category_html .= '<li data-filter="'.$dc['sef'].'" data-text="'.$dc['name'].'" data-key="category">'.$dc['name'].'</li>';
            }
        }

        $category_html .= "</ul>";

        add_variable('category_html', $category_html);
    }

    $where_add  = "";
    $type       = (isset($_GET['type']) ? $_GET['type'] : 'all');
    $category   = (isset($_GET['category']) ? $_GET['category'] : 'all');
    
    $type_new = "All";
    $inner_join_attachment = "INNER JOIN lumonata_attachment AS d ON (a.larticle_id=d.larticle_id)";

    add_variable('type_search', $type);
    add_variable('type_new', $type_new);

    // WHERE ADD IF CATEGORY NOT EMPTY
    if(!empty($category))
    {
        if($category != "all")
        {
            $rule_id = get_rule_id_category($category, 'gallery');
            if(!empty($rule_id))
            {
                $where_add .= " AND b.lrule_id=$rule_id";
            }
        }
    }

    // PAGING
    $page     = isset($_GET['page']) ? $_GET['page'] : 1;
    $plimit   = 3;
    $viewed   = $plimit;
    $limit    = $viewed * ($page - 1);
    $start    = $limit + 1;

    // ALL LIST GALLERY
    $qallgallery = $db->prepare_query("
        SELECT DISTINCT a.larticle_id AS post_id, a.larticle_title AS post_title
        FROM lumonata_articles AS a
        LEFT JOIN lumonata_rule_relationship AS b 
            ON (a.larticle_id=b.lapp_id)
        INNER JOIN lumonata_additional_fields AS c
            ON (a.larticle_id=c.lapp_id)
        $inner_join_attachment
        WHERE a.larticle_type=%s AND a.larticle_status=%s AND c.lkey=%s AND c.lvalue=%s $where_add
        ORDER BY a.lorder ASC
    ", 
        "gallery", "publish", 'destination', $destination
    );
    $rallgallery = $db->do_query($qallgallery);
    $nallgallery = $db->num_rows($rallgallery);

    // GET LIST GALLERY
    $qgallery = $db->prepare_query("
        SELECT DISTINCT a.larticle_id AS post_id, a.larticle_title AS post_title
        FROM lumonata_articles AS a
        LEFT JOIN lumonata_rule_relationship AS b 
            ON (a.larticle_id=b.lapp_id)
        INNER JOIN lumonata_additional_fields AS c
            ON (a.larticle_id=c.lapp_id)
        $inner_join_attachment
        WHERE a.larticle_type=%s AND a.larticle_status=%s AND c.lkey=%s AND c.lvalue=%s $where_add
        ORDER BY a.lorder ASC
    ", 
        "gallery", "publish", 'destination', $destination, $limit, $viewed
    );
    
    $rgallery = $db->do_query($qgallery);
    $ngallery = $db->num_rows($rgallery);


    $choose_media = array();

    if($ngallery > 0)
    {
        // $i = 1;
        $list_gallery = array();
        while($d = $db->fetch_array($rgallery))
        {
            $post_id         = $d['post_id'];
            $post_title      = $d['post_title'];
            $post_attachment = get_post_attachment( $post_id, 'gallery' );

            if(!empty($post_attachment))
            {
                foreach($post_attachment as $dp)
                {
                    $title_gallery       = $dp['img_title'];
                    $youtube_url         = $dp['img_youtube_url'];
                    $photo360            = $dp['img_photo_360'];
                    $featured_image      = $dp['img_large'];
                    $type_list           = "photo";
                    $class_icon          = '<div class="zoom icon-gallery"></div>';
                    $vcode               = "";
                    $iframe_youtube      = "";
                    $link_fancybox       = $dp['img_original'];
                    $img_attach_language = json_decode($dp['img_attach_language'], true);

                    if($check_lang)
                    {
                        $title_gallery_lang = $img_attach_language[$lang];
                        $title_gallery      = empty($title_gallery_lang) ? $title_gallery : $title_gallery_lang;
                    }

                    $title_image         = '<h3>'.$title_gallery.'</h3>';
                
                    if(!empty($featured_image))
                    {
                        if(!isset($choose_media['image']))
                        {
                            $choose_media['image'] = '<li data-filter="photo" data-text="'.$tr_string['photo'].'" data-key="type">'.$tr_string['photo'].'</li>';
                        }
                    }

                    if(!empty($youtube_url))
                    {
                        $vcode          = get_v_code_youtube($youtube_url);
                        $iframe_youtube = get_iframe_youtube($vcode);
                        $video_embed    = 'https://www.youtube.com/embed/'.$vcode.'?autoplay=1&cc_load_policy=0&theme=dark&color=white&controls=0&disablekb=0&iv_load_policy=3&loop=1&modestbranding=1&rel=0&showinfo=0&html5=1&autohide=1';
                        $link_fancybox  = $youtube_url;
                        $title_image    = "";
                        $class_icon     = '<div class="play-video icon-gallery"></div>';
                        $type_list      = "video";
                        if(!isset($choose_media['video']))
                        {
                            $choose_media['video'] = '<li data-filter="video" data-text="'.$tr_string['video'].'" data-key="type">'.$tr_string['video'].'</li>';
                        }
                    }

                    if(!empty($photo360))
                    {
                        $class_icon    = '<div class="photo360 icon-gallery"></div>';
                        $type_list     = "photo-360";
                        $link_fancybox = $photo360;
                        $title_image   = "";
                        if(!isset($choose_media['photo_360']))
                        {
                            $choose_media['photo_360'] = '<li data-filter="photo-360" data-text="'.$tr_string['photo_360'].'" data-key="type">'.$tr_string['photo_360'].'</li>';
                        }
                    }

                    if($type != "all")
                    {
                        if($type == "photo")
                        {
                            if(empty($youtube_url) && empty($photo360))
                            {
                                $list_gallery[]   = '
                                    <a href="'.$link_fancybox.'" rel="fancy-group" class="fancy-group" data-caption="'.$title_gallery.'" data-fancybox="images">
                                        <div class="container list-item clearfix">
                                            <div class="element container-image lazy" data-src="'.$featured_image.'"></div>
                                            <div class="overlay"></div>
                                            '.$class_icon.'
                                            '.$title_image.'
                                        </div>
                                    </a>
                                ';
                            }
                        }
                        else if($type == "photo-360")
                        {
                            if(!empty($photo360))
                            {
                                $list_gallery[]   = '
                                    <a href="'.$link_fancybox.'" target="_blank" data-caption="'.$title_gallery.'">
                                        <div class="container list-item clearfix">
                                            <div class="element container-image lazy" data-src="'.$featured_image.'"></div>
                                            <div class="overlay"></div>
                                            '.$class_icon.'
                                            '.$title_image.'
                                        </div>
                                    </a>
                                ';
                            }
                        }
                        else if($type == "video")
                        {
                            if(!empty($youtube_url))
                            {
                                $list_gallery[]   = '
                                    <a href="'.$link_fancybox.'" rel="fancy-group" class="fancy-group" data-caption="'.$title_gallery.'" data-fancybox="images">
                                        <div class="container list-item clearfix">
                                            <div class="element container-image lazy" data-src="'.$featured_image.'"></div>
                                            <div class="overlay"></div>
                                            '.$class_icon.'
                                            '.$title_image.'
                                        </div>
                                    </a>
                                ';
                            }
                        }
                    }
                    else
                    {
                        if($type_list == "photo-360")
                        {
                            $html_list_gallery = '
                            <a href="'.$link_fancybox.'" target="_blank" data-caption="'.$title_gallery.'">
                                <div class="container list-item clearfix">
                                    <div class="element container-image lazy" data-src="'.$featured_image.'"></div>
                                    <div class="overlay"></div>
                                    '.$class_icon.'
                                    '.$title_image.'
                                </div>
                            </a>
                            ';
                        }
                        else
                        {
                            $html_list_gallery = '
                            <a href="'.$link_fancybox.'" rel="fancy-group" class="fancy-group" data-caption="'.$title_gallery.'" data-fancybox="images">
                                <div class="container list-item clearfix">
                                    <div class="element container-image lazy" data-src="'.$featured_image.'"></div>
                                    <div class="overlay"></div>
                                    <div class="loader"></div>
                                    '.$class_icon.'
                                    '.$title_image.'
                                </div>
                            </a>
                            ';
                        }
                        $list_gallery[] = $html_list_gallery;
                    }
                }
            }

        }

        // CONFIGURE CHOOSE MEDIA
        $choose_media_html = "";
        foreach($choose_media as $key=>$value)
        {
            $choose_media_html .= $value;
        }
        add_variable('choose_media_html', $choose_media_html);

        // LIST GALLERY
        foreach($list_gallery as $lg)
        {
            add_variable('list_gallery', $lg);
            parse_template( 'loop-list-block', 'llblock', true );
        }
        

        $add_url = "";
        if(!empty($type) && !empty($category))
        {
            $add_url = "&type=$type&category=$category";
        }

        add_variable('paging', getPaginationString($page, $nallgallery, $viewed, $plimit, $page_url, "?page=", $add_url));
    }
    
    // INCLUDE JS
    add_actions( 'include-js', 'get_custom_javascript', 'https://unpkg.com/packery@2/dist/packery.pkgd.min.js'.$version, 'defer' );
    add_actions( 'include-js', 'get_custom_javascript', HTTP.TEMPLATE_URL.'/assets/js/jquery.fancybox.min.js'.$version, 'defer' );
    add_actions( 'include-js', 'get_custom_javascript', 'https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js'.$version, 'defer' );
    add_actions( 'include-css', 'get_custom_css', HTTP.TEMPLATE_URL.'/assets/css/jquery.fancybox.min.css'.$version );

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE CONTACT CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function contact_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false)
{
    global $db;
    $version = attemp_actions('version_js_css');

    set_template( TEMPLATE_PATH . '/template/contact.html', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $dest_id            = $corporatedata[0]['id'];
    $cordinate_location = get_additional_field( $dest_id, 'coordinate_location', 'destinations' );
    $address            = get_additional_field( $dest_id, 'address', 'destinations' );
    $phone_number       = get_additional_field( $dest_id, 'phone_number', 'destinations' );
    $email_address      = get_additional_field( $dest_id, 'email_address', 'destinations' );
    $social_media_html  = $corporatedata[0]['social_media']['social_media_default'];
    $captcha_site_key   = get_meta_data( 'r_public_key', 'static_setting' );

    add_actions( 'include-js-contact', 'get_custom_javascript', HTTP.TEMPLATE_URL.'/assets/js/maps_contact.min.js'.$version , 'async defer' );
    add_actions( 'include-js-contact', 'get_custom_javascript', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAIm9dE3WDFcNOgPHClXTLkJCrFiSVnN68&callback=initialize', 'async defer' );
    add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
    add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/contact-us/' : HTTP . site_url().'/'.$destination.'/contact-us/');
    config_meta_data($page_url, $data_page);

    $latitude  = "";
    $longitude = "";
    $country_option = get_data_country();

    if(!empty($cordinate_location))
    {
        $cordinate = explode(",", $cordinate_location);
        $latitude  = $cordinate[0];
        $longitude = $cordinate[1];
    }

    $maps_icon = HTTP.TEMPLATE_URL.'/assets/images/maps_icon.png';

    add_variable('latitude', $latitude);
    add_variable('longitude', $longitude);
    add_variable('maps_icon', $maps_icon);
    add_variable('destination', $destination);
    add_variable('title_dest', "The Samaya ".ucwords($destination));
    add_variable('address', $address);
    add_variable('phone_number', $phone_number);
    add_variable('email_address', $email_address);
    add_variable('social_media_html', $social_media_html);
    add_variable('captcha_site_key', $captcha_site_key);
    add_variable( 'country_option', $country_option );

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE LOCATION CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function location_content($destination, $corporatedata, $lang='', $check_lang=false)
{
    global $db;
    $version = attemp_actions('version_js_css');

    set_template( TEMPLATE_PATH . '/template/location.html', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );
    
    // add_actions( 'include-js', 'get_custom_javascript', HTTP.TEMPLATE_URL.'/assets/js/scrollbar/jquery.scrollbar.min.js'.$version, 'async defer' );
    add_actions( 'include-js', 'get_custom_javascript', HTTP.TEMPLATE_URL.'/assets/js/locations.min.js'.$version );
    add_actions( 'include-js', 'get_custom_javascript', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAIm9dE3WDFcNOgPHClXTLkJCrFiSVnN68&callback=maps_locations_page', 'async defer' );
    

    $dest_id            = $corporatedata[0]['id'];
    $social_media_html  = $corporatedata[0]['social_media']['social_media_default'];

    $cordinate_location = get_additional_field( $dest_id, 'coordinate_location', 'destinations' );
    $address            = get_additional_field( $dest_id, 'address', 'destinations' );
    $phone_number       = get_additional_field( $dest_id, 'phone_number', 'destinations' );
    $email_address      = get_additional_field( $dest_id, 'email_address', 'destinations' );
    $title_page         = get_additional_field( $dest_id, 'location_title', 'destinations' );
    $desc_page          = get_additional_field( $dest_id, 'location_text', 'destinations' );
    $meta_title         = get_additional_field( $dest_id, 'location_meta_title', 'destinations' );
    $meta_keywords      = get_additional_field( $dest_id, 'location_meta_keywords', 'destinations' );
    $meta_description   = get_additional_field( $dest_id, 'location_meta_description', 'destinations' );

    // CHANGE LANGUAGE DATA
    if($check_lang)
    {
        $title_page_lang       = get_additional_field( $dest_id, 'location_title_'.$lang, 'destinations' );
        $desc_page_lang        = get_additional_field( $dest_id, 'location_text_'.$lang, 'destinations' );
        $meta_title_lang       = get_additional_field( $dest_id, 'location_meta_title_'.$lang, 'destinations' );
        $meta_keywords_lang    = get_additional_field( $dest_id, 'location_meta_keywords_'.$lang, 'destinations' );
        $meta_description_lang = get_additional_field( $dest_id, 'location_meta_description_'.$lang, 'destinations' );

        $title_page       = (empty($title_page_lang) ? $title_page:             $title_page_lang);
        $desc_page        = (empty($desc_page_lang) ? $desc_page:               $desc_page_lang);
        $meta_title       = (empty($meta_title_lang) ? $meta_title:             $meta_title_lang);
        $meta_keywords    = (empty($meta_keywords_lang) ? $meta_keywords:       $meta_keywords_lang);
        $meta_description = (empty($meta_description_lang) ? $meta_description:    $meta_description_lang);
    }

    $latitude  = "";
    $longitude = "";
    if(!empty($cordinate_location))
    {
        $cordinate = explode(",", $cordinate_location);
        $latitude  = $cordinate[0];
        $longitude = $cordinate[1];
    }

    $maps_icon = HTTP.TEMPLATE_URL.'/assets/images/marker_location_'.$destination.'_new.png';
    $data_page = array();

    $data_page['title']       = $title_page;
    $data_page['description'] = $desc_page;
    $data_page['metatitle']   = $meta_title;
    $data_page['metakey']     = $meta_keywords;
    $data_page['metadesc']    = $meta_description;
    $data_page['bg_hero']     = "";

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/locations/' : HTTP . site_url().'/'.$destination.'/locations/');
    config_meta_data($page_url, $data_page);

    add_variable('title_page', $title_page);
    add_variable('desc_page', $desc_page);
    add_variable('latitude', $latitude);
    add_variable('longitude', $longitude);
    add_variable('maps_icon', $maps_icon);
    add_variable('destination', $destination);
    add_variable('title_dest', "The Samaya ".ucwords($destination));
    add_variable('address', $address);
    add_variable('phone_number', $phone_number);
    add_variable('email_address', $email_address);
    add_variable('social_media_html', $social_media_html);
    add_variable( 'HTTP', HTTP );
    add_variable( 'site_url', site_url() );

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE STATIC CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function static_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/static_page.html', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    $description_dest = $data_page['description'];
    // $description_dest = '';
    add_variable('description_dest', $description_dest);

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/about-us/' : HTTP . site_url().'/'.$destination.'/about-us/');
    config_meta_data($page_url, $data_page);

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE ABOUT CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function about_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/about.html', 'page_template' );
    add_block( 'loop-dest-block', 'ldblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $description_dest = $corporatedata[0]['desc'];
    add_variable('description_dest', $description_dest);

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/about-us/' : HTTP . site_url().'/'.$destination.'/about-us/');
    config_meta_data($page_url, $data_page);
    
    // GET DESTINATION LIST
    $destination_list     = get_post_list('destinations', 6, '');
    if(!empty($destination_list))
    {
        foreach($destination_list as $d)
        {
            $post_sef = $d['post_sef'];

            if($post_sef == $destination)
            {
                add_variable('title_dest', $d['post_title']);
                add_variable('content_dest', $d['post_content']);
    
                $gallery_image = $d['post_attachment'];
                $img_list      = "";
                if(!empty($gallery_image))
                {
                    foreach($gallery_image as $d)
                    {
                        $img_medium = $d['img_medium'];
                        $img_list .= '<img class="lazy" data-src="'.$img_medium.'" />';
                    }
                }
                add_variable('img_list', $img_list);
    
                parse_template( 'loop-dest-block', 'ldblock', true );
            }
        }
    }

    // GET ACCOLADES LIST
    // $accolades_html     = "";
    // $qacco = $db->prepare_query("
    //     SELECT larticle_id
    //     FROM lumonata_articles
    //     WHERE larticle_type=%s AND larticle_status=%s
    //     ORDER BY lorder ASC
    // ",
    //     "accolades", "publish"
    // );
    // $racco = $db->do_query($qacco);
    // $nacco = $db->num_rows($racco);

    // $accolades_list     = get_post_list('accolades', 6, '', $destination);
    // if(!empty($accolades_list))
    // {
    //     foreach($accolades_list as $dacco)
    //     {
    //         $featured_img = get_featured_img( $dacco['post_id'], 'accolades', false );
    //         $accolades_html .= '<img src="'.HTTP.site_url().$featured_img['medium'].'" />';
    //     }
    // }
    
    // add_variable('accolades_html', $accolades_html);

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}



/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK AJAX CONTACT FORM INQUIRY
| -----------------------------------------------------------------------------
*/
function contact_form_inquiry($post_data)
{
    if (isset($post_data['recaptcha']) && !empty($post_data['recaptcha'])) 
    {

        $params             = array();
        $params['remoteip'] = $_SERVER['REMOTE_ADDR'];
        $params['secret']   = get_meta_data('r_secret_key', 'static_setting');
        $params['response'] = (!empty($post_data['recaptcha']) ? urlencode($post_data['recaptcha']) : '');

        $prm_string = http_build_query($params);
        $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $prm_string;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $requestURL,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $data = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($data);

        if ($response->success) 
        {
            $fullname = ucwords($post_data['name']);
            $email    = $post_data['email'];
            $country  = $post_data['country'];
            $phone    = $post_data['phone'];
            $find_us  = $post_data['find_us'];

            $subject     = "Contact Form - Samaya - $fullname";
            $destination = $post_data['destination'];
            $dest_id     = get_id_article_by_sef($destination, 'destinations');
            $cont_email  = get_additional_field($dest_id, 'email_address', 'destinations');
            $web_name    = get_meta_data( 'web_title', 'static_setting' );
            
            empty( $message ) ? $message = "-" : $message = $message;

            $msg_body        = ' 
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="content" style="padding-left: 40px;padding-right: 40px;padding-top: 40px;padding-bottom: 20px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;font-size: 13px;color: #666;">
                                    You have a new contact message from the website. Please check the detail below for your reference.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Full Name
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$fullname.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Country
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$country.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Phone
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$phone.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Email
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$email.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    How did you find us?
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$find_us.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            ';
            
            $send_email = send_email($email, $fullname, $cont_email, $web_name, $subject, $msg_body);

            if($send_email)
            {
                $result['status']   = 'success';
                $result['message']  = '<h3><b>Success!</b> Your inquiry has been sent.</h3>';
            }
            else
            {
                $result['status']   = 'error';
                $result['message']  = '<h3><b>Danger!</b> Sorry your message cannot be send, please try again later.</h3>';
            }
        } 
        else 
        {
            $result['status']   = 'error';
            $result['message']  = '<h3><b>Danger!</b> Sorry your message cannot be send, please try again later.</h3>';
        }
    } 
    else 
    {
        $result['status']   = 'warning';
        $result['message']  = '<h3><b>Warning!</b> Please check the reCaptcha.</h3>';
    }

    return $result;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN NAMA SPA BERDASARKAN ID
| -----------------------------------------------------------------------------
*/
function get_name_spa_by_id($id)
{
    global $db;

    $q = $db->prepare_query("
        SELECT larticle_title
        FROM lumonata_articles
        WHERE larticle_id=%d AND larticle_type=%s
    ", $id, "spa");
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    if($n > 0)
    {
        $d = $db->fetch_array($r);
        return $d['larticle_title'];
    }
    return '';
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN NAMA CATEGORY TREATMENT BERDASARKAN ID
| -----------------------------------------------------------------------------
*/
function get_name_treatment_category_by_id($id)
{
    global $db;

    $q = $db->prepare_query("
        SELECT lctreatment_name
        FROM lumonata_treatment_category
        WHERE lctreatment_id=%d
    ", $id);
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    if($n > 0)
    {
        $d = $db->fetch_array($r);
        return $d['lctreatment_name'];
    }

    return '';
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN NAMA TREATMENT BERDASARKAN ID
| -----------------------------------------------------------------------------
*/
function get_name_treatment_by_id($id)
{
    global $db;

    $q = $db->prepare_query("
        SELECT ltreatment_name
        FROM lumonata_treatment
        WHERE ltreatment_id=%d
    ", $id);
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    if($n > 0)
    {
        $d = $db->fetch_array($r);
        return $d['ltreatment_name'];
    }

    return '';
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN TREATMENT CATEGORY BERDASARKAN SPA ID
| -----------------------------------------------------------------------------
*/
function get_treatment_category_list($id, $lang='', $check_lang=false)
{
    $string_translations = get_string_translation_lang();
    $translations = array('Select Treatment Category', 'Select Treatment List');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    global $db;

    $q = $db->prepare_query("
        SELECT lctreatment_id, lctreatment_name
        FROM lumonata_treatment_category
        WHERE larticle_id=%d
    ",
        $id
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    $result['html'] = "";

    if($n > 0)
    {
        $option_category = '<option value="">'.$tr_string['select_treatment_category'].'*</option>';

        $i               = 0;
        $cat_id          = "";

        while($d = $db->fetch_array($r))
        {
            $category_id   = $d['lctreatment_id'];
            $category_name = ucwords(strtolower($d['lctreatment_name']));

            $option_category .= '<option value="'.$category_id.'">'.$category_name.'</option>';

            if($i == 0)
            {
                $cat_id = $category_id;
            }

            $i++;
        }

        $result['html'] = $option_category;
        $result['category_id'] = $cat_id;
    }

    return $result;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN TREATMENT LIST BERDASARKAN TREATMENT CATEGORY ID
| -----------------------------------------------------------------------------
*/
function get_treatment_list($cat_id, $treat_id='', $lang='', $check_lang=false)
{
    $string_translations = get_string_translation_lang();
    $translations = array('Select Treatment Category', 'Select Treatment List');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    global $db;

    $q = $db->prepare_query("
        SELECT ltreatment_id, ltreatment_name, ltreatment_price, ltreatment_tax
        FROM lumonata_treatment
        WHERE lctreatment_id=%d
    ",
        $cat_id
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    $option_treatment = '<option value="">'.$tr_string['select_treatment_list'].'*</option>';
    $result['html'] = "";

    if($n > 0)
    {
        $i     = 0;
        $price = 0;
        $tax   = 0;

        while($d = $db->fetch_array($r))
        {
            $treatment_id       = $d['ltreatment_id'];
            $treatment_name     = ucwords(strtolower($d['ltreatment_name']));
            $treatment_price    = $d['ltreatment_price'];
            $treatment_tax      = $d['ltreatment_tax'];
            $treatment_tax      = (($treatment_tax != 0) ? (($treatment_price * ($treatment_tax/100))) : 0 );

            $option_treatment   .= '<option value="'.$treatment_id.'">'.$treatment_name.'</option>';
            $price              = (($i == 0) ? $treatment_price : $price);
            $tax                = (($i == 0) ? $treatment_tax : $tax);

            if($treatment_id == $treat_id)
            {
                $result['test']   = "ok";;
                $price = $treatment_price;
                $tax   = $treatment_tax;
            }

            $i++;
        }

        $result['html']  = $option_treatment;
        $result['price'] = $price;
        $result['tax']   = $tax;
    }

    return $result;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN TREATMENT PRICE & TAX BERDASARKAN TREATMENT LIST ID
| -----------------------------------------------------------------------------
*/
function get_treatment_price_tax($id)
{
    global $db;

    $q = $db->prepare_query("
        SELECT ltreatment_id, ltreatment_name, ltreatment_price, ltreatment_tax
        FROM lumonata_treatment
        WHERE ltreatment_id=%d
    ",
        $id
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    
    $result['price'] = "";
    $result['tax'] = "";

    if($n > 0)
    {
        $i = 0;
        $treatment_price = 0;
        $treatment_tax = 0;
        while($d = $db->fetch_array($r))
        {
            $treatment_price = $d['ltreatment_price'];
            $treatment_tax   = $d['ltreatment_tax'];
            $treatment_tax = (($treatment_tax != 0) ? (($treatment_price * ($treatment_tax/100))) : 0 );
        }
        
        $result['price'] = $treatment_price;
        $result['tax']   = $treatment_tax;
    }

    return $result;
}
?>