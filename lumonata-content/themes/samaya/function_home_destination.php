<?php
/*
| -------------------------------------------------------------------------------------------------------------------------
| Homepage Destination Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function home_destination_content($post, $appname='',$destination_id='', $lang='', $check_lang=false, $string_translations='')
{
    global $db;
    $cek_url = cek_url();
    $version = attemp_actions('version_js_css');
    
    set_template( TEMPLATE_PATH . '/template/home_destination.html', 'h-template' );
    add_block('loop_activity_list', 'lacblock', 'h-template');
    add_block('loop_offers_list', 'loblock', 'h-template');
    add_block('loop_accommodations_list', 'lablock', 'h-template');
    add_block('loop_accommodations_list_mobile', 'lamblock', 'h-template');
    add_block('loop_experience_list', 'leblock', 'h-template');
    add_block('loop-list-accolades-block', 'llablock', 'h-template');
    add_block('whats-happening-block', 'whblock', 'h-template');
    add_block('home-block', 'h-block', 'h-template');

    // INCLUDE JS
    add_actions( 'include-js', 'get_custom_javascript', 'https://unpkg.com/packery@2/dist/packery.pkgd.min.js', 'defer' );

    // META DATA SETTING
    $meta_title       = get_meta_title($destination_id, 'destinations' , $check_lang , $lang);
    $meta_title       = (empty($meta_title) ? 'Home - '.ucwords($cek_url[0]).' | '.trim(web_title()) : $meta_title);
    $meta_keywords    = get_meta_keywords($destination_id, 'destinations',$check_lang , $lang);
    $meta_description = get_meta_description($destination_id, 'destinations',$check_lang , $lang);
    $og_url           = '';
    $og_title         = '';
    $og_image         = $post[0]['background'];
    $og_site_name     = '';
    $og_description   = '';
    add_meta_data_value($meta_title, $meta_keywords, $meta_description, $og_url, $og_title, $og_image, $og_site_name, $og_description);
    

    // STRING TRANSLATIONS
    set_variable_string_translations($lang, $check_lang, $string_translations);
    $translations = array('Latest Promotion', 'Accommodation', 'Whats Happening At Samaya Seminyak', 'See Packages', 'Whats Happening At Samaya Ubud', 'Review', 'Reviews at Tripadvisor', 'Travellers Choice', 'Latest Activity' );
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $social_media_html = $post[0]['social_media']['social_media_default'];
    $site_url          = site_url();
    $description_dest  = $post[0]['desc'];
    $link_about        = HTTP.site_url().'/'.$appname.'/about-us';

    add_variable( 'appname_upper', strtoupper($appname) );
    add_variable( 'site_url', $site_url );
    add_variable( 'description_dest', $description_dest );
    add_variable( 'HTTP', HTTP );
    add_variable( 'link_about', $link_about );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );
    add_variable( 'social_media_html', $social_media_html );

    // SECTION SPECIAL OFFERS
    $offers = get_post_list('special-offers', 6, '', $appname);
    if(count($offers) > 0)
    {
        foreach($offers as $d)
        {
            $id_offers        = $d['post_id'];
            $title_offers     = $d['post_title'];
            // $subtitle_offers  = $d['post_subtitle'];
            // $subtitle_offers  = (empty($subtitle_offers) ? $tr_string['latest_promotion'] : $subtitle_offers );
            $subtitle_offers  = $tr_string['latest_promotion'];
            $brief_offers     = $d['post_brief'];
            $sef_offers       = $d['post_sef'];
            $content_offers   = $d['post_content'];
            $post_link_offers = HTTP . site_url().'/'.$appname.'/special-offers/'.$sef_offers.'.html';
            $image_offers     = get_featured_img( $id_offers, 'special-offers', false );
            $image_offers     = HTTP . site_url().$image_offers['medium'];

            if($check_lang)
            {   
                $title_offers_lang    = get_additional_field($id_offers, 'title_'.$lang, 'special-offers');
                // $subtitle_offers_lang = get_additional_field($id_offers, 'subtitle_'.$lang, 'special-offers');
                $brief_offers_lang    = get_additional_field($id_offers, 'brief_'.$lang, 'special-offers');
                $content_offers_lang  = get_additional_field($id_offers, 'content_'.$lang, 'special-offers');

                $title_offers     = (empty($title_offers_lang) ? $title_offers:       $title_offers_lang);
                // $subtitle_offers  = (empty($subtitle_offers_lang) ? $subtitle_offers: $subtitle_offers_lang);
                $brief_offers     = (empty($brief_offers_lang) ? $brief_offers:       $brief_offers_lang);
                $content_offers   = (empty($content_offers_lang) ? $content_offers:   $content_offers_lang);
                $post_link_offers = HTTP . site_url().'/'.$lang.'/'.$appname.'/special-offers/'.$sef_offers.'.html';
            }
            

            add_variable('title_offers', $title_offers);
            add_variable('subtitle_offers', $subtitle_offers);
            add_variable('content_offers', $content_offers);
            add_variable('brief_offers', preg_replace('#<a.*?>(.*?)</a>#i', '\1', $brief_offers));
            add_variable('image_offers', $image_offers);
            add_variable('sef_offers', $sef_offers);
            add_variable('post_link_offers', $post_link_offers);
            
            parse_template('loop_offers_list', 'loblock', true);
        }

        $link_exlore_all_offers = HTTP . site_url().'/'.$appname.'/special-offers/';

        add_variable('link_exlore_all_offers', $link_exlore_all_offers);
    }

    // SECTION ACTIVITY
    $activity = get_post_list('what-to-do-act-in', 6, '', $appname);
    if(count($activity) > 0)
    {
        foreach($activity as $d)
        {
            $id_activity        = $d['post_id'];
            $title_activity     = $d['post_title'];
            // $subtitle_activity  = $d['post_subtitle'];
            // $subtitle_activity  = (empty($subtitle_activity) ? $tr_string['latest_promotion'] : $subtitle_activity );
            $subtitle_activity  = $tr_string['latest_activity'];
            $brief_activity     = $d['post_brief'];
            $sef_activity       = $d['post_sef'];
            $content_activity   = $d['post_content'];
            $post_link_activity = HTTP . site_url().'/'.$appname.'/what-to-do-act-in/'.$sef_activity.'.html';

            if($check_lang)
            {   
                $title_activity_lang    = get_additional_field($id_activity, 'title_'.$lang, 'what-to-do-act-in');
                // $subtitle_activity_lang = get_additional_field($id_activity, 'subtitle_'.$lang, 'what-to-do-act-in');
                $brief_activity_lang    = get_additional_field($id_activity, 'brief_'.$lang, 'what-to-do-act-in');
                $content_activity_lang  = get_additional_field($id_activity, 'content_'.$lang, 'what-to-do-act-in');

                $title_activity     = (empty($title_activity_lang) ? $title_activity:       $title_activity_lang);
                // $subtitle_activity  = (empty($subtitle_activity_lang) ? $subtitle_activity: $subtitle_activity_lang);
                $brief_activity     = (empty($brief_activity_lang) ? $brief_activity:       $brief_activity_lang);
                $content_activity   = (empty($content_activity_lang) ? $content_activity:   $content_activity_lang);
                $post_link_activity = HTTP . site_url().'/'.$lang.'/'.$appname.'/what-to-do-act-in/'.$sef_activity.'.html';
            }

            $image_activity = get_featured_img( $id_activity, 'what-to-do-act-in', false );

            if( isset( $image_activity['medium'] ) && !empty( $image_activity['medium'] ) )
            {
                add_variable('image_activity', HTTP . site_url() . $image_activity['medium']);
            }

            add_variable('title_activity', $title_activity);
            add_variable('subtitle_activity', $subtitle_activity);
            add_variable('content_activity', $content_activity);
            add_variable('brief_activity', $brief_activity);
            add_variable('sef_activity', $sef_activity);
            add_variable('post_link_activity', $post_link_activity);
            
            parse_template('loop_activity_list', 'lacblock', true);
        }

        $link_exlore_all_activity = HTTP . site_url().'/'.$appname.'/what-to-do-act-in/';

        add_variable('link_exlore_all_activity', $link_exlore_all_activity);
    }

    // SECTION ACCOMODATION
    $accommodations = get_post_list('accommodation', 6, '', $appname);
    if(count($accommodations) > 0)
    {
        $i = 0;
        $result_acco = "";
        foreach($accommodations as $d)
        {
            $id_accommodations       = $d['post_id'];
            $title_accommodations    = $d['post_title'];
            // $subtitle_accommodations = $d['post_subtitle'];
            // $subtitle_accommodations = (empty($subtitle_accommodations) ? $tr_string['accommodations']: $subtitle_accommodations);
            $subtitle_accommodations = $tr_string['accommodation'];
            $brief_accommodations    = strip_tags($d['post_brief']);
            $content_accommodations  = strip_tags($d['post_content']);          
            $sef_accommodations      = $d['post_sef'];
            $image_accommodations    = get_featured_img( $id_accommodations, 'accommodation', false );
            $image_accommodations    = HTTP . site_url().$image_accommodations['medium'];
            $link_accommodations     = HTTP . site_url().'/'.$appname.'/accommodation/'.$sef_accommodations.'.html';

            if($check_lang)
            {
                $title_accommodations_lang    = get_additional_field($id_accommodations, 'title_'.$lang, 'accommodation');
                $subtitle_accommodations_lang = get_additional_field($id_accommodations, 'subtitle_'.$lang, 'accommodation');
                $brief_accommodations_lang    = strip_tags(get_additional_field($id_accommodations, 'brief_'.$lang, 'accommodation'));
                $content_accommodations_lang  = get_additional_field($id_accommodations, 'content_'.$lang, 'accommodation');

                $title_accommodations    = (empty($title_accommodations_lang) ? $title_accommodations:       $title_accommodations_lang);
                $subtitle_accommodations = (empty($subtitle_accommodations_lang) ? $subtitle_accommodations: $subtitle_accommodations_lang);
                $brief_accommodations    = (empty($brief_accommodations_lang) ? $brief_accommodations:       $brief_accommodations_lang);
                $content_accommodations  = (empty($content_accommodations_lang) ? $content_accommodations:   $content_accommodations_lang);
                $link_accommodations     = HTTP . site_url().'/'.$lang.'/'.$appname.'/accommodation/'.$sef_accommodations.'.html';
            }

        
            add_variable('title_accommodations', $title_accommodations);
            add_variable('subtitle_accommodations', $subtitle_accommodations);
            add_variable('content_accommodations', $content_accommodations);
            add_variable('brief_accommodations', $brief_accommodations);
            add_variable('image_accommodations', $image_accommodations);
            add_variable('sef_accommodations', $sef_accommodations);
            add_variable('link_accommodations', $link_accommodations);

            if($i == 0)
            {
                $result_acco['title']    = $title_accommodations;
                $result_acco['subtitle'] = $subtitle_accommodations;
                $result_acco['brief']    = $brief_accommodations;
                $result_acco['content']  = $content_accommodations;
                $result_acco['link']     = $link_accommodations;
            }
            
            parse_template('loop_accommodations_list', 'lablock', true);
            parse_template('loop_accommodations_list_mobile', 'lamblock', true);
            
            $i++;
        }

        if(!empty($result_acco))
        {
            add_variable('title_acco', $result_acco['title']);
            add_variable('subtitle_acco', $result_acco['subtitle']);
            add_variable('content_acco', $result_acco['content']);
            add_variable('brief_acco', $result_acco['brief']);
            add_variable('link_acco', $result_acco['link']);
        }
    }


    // SECTION EXPERIENCE
    $post_type_setting_exp = get_meta_data('post_type_setting', 'experience', $destination_id);
    if(!empty($post_type_setting_exp))
    {
        $post_type_setting_exp = json_decode($post_type_setting_exp, true);

        $title_exp = $post_type_setting_exp['title'];
        $desc_exp  = $post_type_setting_exp['description'];

        if($check_lang)
        {
            $title_exp_lang = $post_type_setting_exp['title_'.$lang];
            $desc_exp_lang  = $post_type_setting_exp['description_'.$lang];

            $title_exp = (empty($title_exp_lang) ? $title_exp: $title_exp_lang);
            $desc_exp  = (empty($desc_exp_lang) ? $desc_exp:   $desc_exp_lang);
        }

        if(strpos(strtolower($title_exp), ' at '))
        {
            $title_exp = explode_title_by_at_of(" at ", strtolower($title_exp));
        }

        add_variable('title_experience_setting', $title_exp);
        add_variable('desc_experience_setting', $desc_exp);
    }

    $experience = get_post_list('experience', 6, '', $appname);
    if(count($experience) > 0)
    {
        foreach($experience as $d)
        {
            $id_experience      = $d['post_id'];
            $title_experience   = $d['post_title'];
            $sef_experience     = $d['post_sef'];
            $brief_experience   = $d['post_brief'];
            $content_experience = $d['post_content'];
            $link_experience    = get_additional_field($id_experience,'link_experience','experience');

            if($check_lang)
            {
                $title_experience_lang   = get_additional_field($id_experience, 'title_'.$lang, 'experience');
                $brief_experience_lang   = get_additional_field($id_experience, 'brief_'.$lang, 'experience');
                $content_experience_lang = get_additional_field($id_experience, 'content_'.$lang, 'experience');
                $link_experience_lang    = get_additional_field($id_experience,'link_experience_'.$lang,'experience');

                $title_experience        = (empty($title_experience_lang) ? $title_experience:     $title_experience_lang);
                $brief_experience        = (empty($brief_experience_lang) ? $brief_experience:     $brief_experience_lang);
                $content_experience      = (empty($content_experience_lang) ? $content_experience: $content_experience_lang);
                $link_experience         = (empty($link_experience_lang) ? $link_experience:         $link_experience_lang);
            }

            
            $link_experience    = str_replace("http://", HTTP, $link_experience);
            $link_experience    = str_replace("https://", HTTP, $link_experience);

            $image_experience   = get_featured_img( $id_experience, 'experience', false );
            $image_experience   = HTTP . site_url().$image_experience['medium'];
            
            add_variable('title_experience', $title_experience);
            add_variable('content_experience', $content_experience);
            add_variable('brief_experience', $brief_experience);
            add_variable('image_experience', $image_experience);
            add_variable('sef_experience', $sef_experience);
            add_variable('link_experience', $link_experience);

            parse_template('loop_experience_list', 'leblock', true);
        }
    }


    // SECTION EVENTS (WEDDING)
    $post_type_setting_event = get_meta_data('post_type_setting', 'events', $destination_id);
    $type_events = "events";
    if($appname == "ubud")
    {
        $post_type_setting_event = get_meta_data('post_type_setting', 'weddings', $destination_id);
        $type_events = "weddings";
    }
    if(!empty($post_type_setting_event))
    {
        $post_type_setting_event = json_decode($post_type_setting_event, true);
        $subtitle_event          = $post_type_setting_event['subtitle'];
        $brief_event             = $post_type_setting_event['brief'];
        $bg_image                = $post_type_setting_event['bg_image'];
        $bg_image                = get_url_hero_image_post_setting($bg_image);
        $link_events             = HTTP.site_url().'/'.$appname.'/'.$type_events;

        if($check_lang)
        {
            $subtitle_event_lang = $post_type_setting_event['subtitle_'.$lang];
            $brief_event_lang    = $post_type_setting_event['brief_'.$lang];

            $subtitle_event = (empty($subtitle_event_lang) ? $subtitle_event: $subtitle_event_lang);
            $brief_event    = (empty($brief_event_lang) ? $brief_event:       $brief_event_lang);
            $link_events    = HTTP.site_url().'/'.$lang.'/'.$appname.'/'.$type_events;
        }

    
        if(strpos(strtolower($subtitle_event), ' at '))
        {
            $subtitle_event = explode_title_by_at_of(" at ", strtolower($subtitle_event));
        }

        add_variable('title_event_setting', $subtitle_event);
        add_variable('desc_event_setting', $brief_event);
        add_variable('link_events', $link_events);
        add_variable('bgimage_event_setting', $bg_image['bg_image_medium']);

        $product_events = get_post_list($type_events, 6, '', $appname);
        if(!empty($product_events))
        {
            $slide_event = '<div class="inner">';
            foreach($product_events as $pe)
            {
                $post_id_events         = $pe['post_id'];
                $featured_image_events  = $pe['post_featured_img'];
                $featured_image_events  = HTTP . site_url().$featured_image_events['medium'];

                $slide_event .= '
                    <div class="item lazy" data-src="'.$featured_image_events.'">
                        <div class="loader"></div>
                    </div>
                ';
            }
            $slide_event .= '</div>';

            add_variable('slide_event', $slide_event);
        }

        if($appname == "ubud")
        {
            $event_html ='
            <section class="container wrap-wedding-at-samaya clearfix">
                <div class="container container-slider-wedding" data-aos="fade-up">

                    '.$slide_event.'

                    <div class="prev-next"></div>
                    
                    <div class="container container-arrow-slider-wedding clearfix"></div>
                </div>
                <div class="container container-description-wedding clearfix" data-aos="fade-up">
                    <div class="container container-title-wedding clearfix">
                        <h2 class="text-title-wedding">'.$subtitle_event.'</h2>
                    </div>
                    <div class="text _text text-brief-wedding">'.$brief_event.'</div>
                    <div class="container container-see-package-wedding clearfix">
                        <a href="'.$link_events.'"><p class="text _text text-see-package-wedding">'.$tr_string['see_packages'].' </p></a>
                    </div>
                </div>
            </section>
            ';
            add_variable('event_html', $event_html);
        }
    }


    // SECTION TRIP ADVISOR
    $title_advisor = $tr_string['reviews_at_tripadvisor'];
    if(strpos(strtolower($title_advisor), ' at '))
    {
        $title_advisor = explode_title_by_at_of(" at ", $title_advisor);
    }
    add_variable('title_advisor', $title_advisor);
    $tripadvisor_scrapping = get_meta_data('trip_advisor_samaya', 'scrapping_value', $destination_id);
    $tripadvisor_scrapping = ((!empty($tripadvisor_scrapping)) ?  json_decode($tripadvisor_scrapping, true) : '');
    $rating_bullet = "";

    if(is_array($tripadvisor_scrapping))
    {
        $rating_advisor = $tripadvisor_scrapping['overall_rating'];
        $review_advisor = $tripadvisor_scrapping['review_count'];
        $badge_advisor  = $tripadvisor_scrapping['tc_badge'];
        $badge_advisor  = str_replace("Choice", "Choice ", $badge_advisor);

        add_variable('rating_advisor', $rating_advisor);
        add_variable('review_advisor', $review_advisor);
        add_variable('badge_advisor', $badge_advisor);

        $rating_advisor_floor = floor($rating_advisor);
        if($rating_advisor_floor > 0)
        {
            $rating_bullet = '<ul>';
            for($ri=1; $ri<=$rating_advisor; $ri++)
            {
                $rating_bullet .= '<li class="full"></li>';
            }

            if($rating_advisor > $rating_advisor_floor){
                $rating_bullet .= '<li class="half"></li>';
            }

            $rating_bullet .= '</ul>';
    
            add_variable('rating_bullet', $rating_bullet);
        }
    }

    $tripad_review = get_post_list('tripadvisor-review', 6, '', $appname, 1);
    $tripad_review_html = "";
    if(!empty($tripad_review))
    {
        foreach($tripad_review as $tr)
        {
            $review_id         = $tr['post_id'];
            $review_title      = $tr['post_title'];
            $review_desc       = $tr['post_content'];
            $traveller_name    = get_additional_field( $review_id, 'traveller_name', 'tripadvisor-review' );
            $traveller_country = get_additional_field( $review_id, 'country', 'tripadvisor-review' );

            if($check_lang)
            {
                $review_title_lang    = get_additional_field($review_id, 'title_'.$lang, 'tripadvisor-review');
                $review_content_lang    = get_additional_field($review_id, 'content_'.$lang, 'tripadvisor-review');
                $traveller_name_lang    = get_additional_field($review_id, 'traveller_name_'.$lang, 'tripadvisor-review');
                $country_lang    = get_additional_field($review_id, 'country_'.$lang, 'tripadvisor-review');

                $review_title    = (empty($review_title_lang) ? $review_title:       $review_title_lang);
                $review_desc    = (empty($review_content_lang) ? $review_desc:       $review_content_lang);
                $traveller_name    = (empty($traveller_name_lang) ? $traveller_name:       $traveller_name_lang);
                $traveller_country    = (empty($country_lang) ? $traveller_country:       $country_lang);
            }

            $tripad_review_html .= '
                <div class="item">
                    <div class="title-des-reviews">
                        <h3 data-aos="fade-up">'.$review_title.'</h3>
                        <div data-aos="fade-up">'.$review_desc.'</div>
                    </div>
                    <div class="reviewer">
                        <h4 data-aos="fade-up">'.$traveller_name.'</h4>
                        <h5 data-aos="fade-up">'.$traveller_country.'</h5>
                    </div>
                    <img src="'.HTTP.TEMPLATE_URL.'/assets/images/tripads.svg" alt="Tripadvisor" data-aos="fade-up">
                </div>
            ';
        }
    }
    add_variable('tripad_review_html', $tripad_review_html);

    // SECTION WHATS HAPPENING
    $social_media_photo = get_social_media_photo($appname, $destination_id);

    if( !empty( $social_media_photo ) )
    {
        $title_whats_happening = $tr_string['whats_happening_at_samaya_'.$appname];

        if(strpos(strtolower($title_whats_happening), ' at '))
        {
            $title_whats_happening = explode_title_by_at_of(" at ", $title_whats_happening);
        }

        add_variable('title_whats_happening', $title_whats_happening);
        add_variable('get_social_media_photo', $social_media_photo);   

        parse_template('whats-happening-block', 'whblock');
    }

    // SECTION ACCOLADES
    $data_page_accolades   = get_articles_data('pages', 'press-accolades');
    $page_id_accolades     = $data_page_accolades[0]['id'];
    $title_accolades       = $data_page_accolades[0]['title'];
    $subtitle_accolades    = $data_page_accolades[0]['subtitle'];
    $description_accolades = $data_page_accolades[0]['desc'];

    if($check_lang)
    {
        $title_lang_accolades       = get_additional_field($page_id_accolades, 'title_'.$lang, 'pages');
        $subtitle_lang_accolades    = get_additional_field($page_id_accolades, 'subtitle_'.$lang, 'pages');
        $description_lang_accolades = get_additional_field($page_id_accolades, 'content_'.$lang, 'pages');
        
        $title_accolades            = (empty($title_lang_accolades) ? $title_accolades:             $title_lang_accolades);
        $subtitle_accolades         = (empty($subtitle_lang_accolades) ? $subtitle_accolades:       $subtitle_lang_accolades);
        $description_accolades      = (empty($description_lang_accolades) ? $description_accolades: $description_lang_accolades);
    }


    add_variable('title_accolades', $title_accolades);
    add_variable('subtitle_accolades', $subtitle_accolades);
    add_variable('description_accolades', $description_accolades);

    $where_add_accolades = " AND a.lstatus_accolades=1";
    $accolades     = get_post_list('news', 6, $where_add_accolades, $appname);

    if(!empty($accolades))
    {
        foreach($accolades as $d)
        {
            $post_id_accolades    = $d['post_id'];
            $post_title_accolades = $d['post_title'];
            $post_brief_accolades = $d['post_brief'];

            if($check_lang)
            {
                $post_title_accolades_lang = get_additional_field($post_id_accolades, 'title_'.$lang, 'news');
                $post_brief_accolades_lang = get_additional_field($post_id_accolades, 'content_'.$lang, 'news');
                
                $post_title_accolades      = (empty($post_title_lang) ? $post_title_accolades: $post_title_lang);
                $post_brief_accolades      = (empty($post_brief_accolades_lang) ? $post_brief_accolades: $post_brief_accolades_lang);
            }

            $featured_image_accolades = get_featured_img( $post_id_accolades, 'news', false );
            $featured_image_accolades = HTTP . site_url().$featured_image_accolades['medium'];

            
            add_variable('post_title_accolades', $post_title_accolades);
            add_variable('post_brief_accolades', $post_brief_accolades);
            add_variable('featured_image_accolades', $featured_image_accolades);

            parse_template('loop-list-accolades-block', 'llablock', true);
        }
    }

    $post_id = $post[0]['id'];
    $villa_reservation    = get_additional_field($post_id, 'villa_reservation', 'destinations');
    $restaurant_reservation    = get_additional_field($post_id, 'restaurant_reservation', 'destinations');
    $guest_reservation    = get_additional_field($post_id, 'guest_reservation', 'destinations');
    
    $wa_room = isset( $villa_reservation ) && !empty( $villa_reservation ) ? $villa_reservation : '';
    $wa_restaurant = isset( $restaurant_reservation ) && !empty( $restaurant_reservation ) ? $restaurant_reservation : '';
    $wa_inhouse_guest = isset( $guest_reservation  ) && !empty( $guest_reservation  ) ? $guest_reservation  : '';
    
    if( !empty( $wa_room ) || !empty( $wa_restaurant ) ||  !empty( $wa_inhouse_guest ) ) {
        $wa_html = '<div class="floating_wa">
            <a>
                <div class="bg-[#A6631B] rounded-[50%] p-4 ml-4 mr-4 overflow-hidden flex justify-center align-center"><div class="font-bold text-white flex justify-center align-center"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="40" height="60" viewBox="0,0,256,256"
style="fill:#000000;">
<g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.12,5.12)"><path d="M25,2c-12.69047,0 -23,10.30953 -23,23c0,4.0791 1.11869,7.88588 2.98438,11.20898l-2.94727,10.52148c-0.09582,0.34262 -0.00241,0.71035 0.24531,0.96571c0.24772,0.25536 0.61244,0.35989 0.95781,0.27452l10.9707,-2.71875c3.22369,1.72098 6.88165,2.74805 10.78906,2.74805c12.69047,0 23,-10.30953 23,-23c0,-12.69047 -10.30953,-23 -23,-23zM25,4c11.60953,0 21,9.39047 21,21c0,11.60953 -9.39047,21 -21,21c-3.72198,0 -7.20788,-0.97037 -10.23828,-2.66602c-0.22164,-0.12385 -0.48208,-0.15876 -0.72852,-0.09766l-9.60742,2.38086l2.57617,-9.19141c0.07449,-0.26248 0.03851,-0.54399 -0.09961,-0.7793c-1.84166,-3.12289 -2.90234,-6.75638 -2.90234,-10.64648c0,-11.60953 9.39047,-21 21,-21zM16.64258,13c-0.64104,0 -1.55653,0.23849 -2.30859,1.04883c-0.45172,0.48672 -2.33398,2.32068 -2.33398,5.54492c0,3.36152 2.33139,6.2621 2.61328,6.63477h0.00195v0.00195c-0.02674,-0.03514 0.3578,0.52172 0.87109,1.18945c0.5133,0.66773 1.23108,1.54472 2.13281,2.49414c1.80347,1.89885 4.33914,4.09336 7.48633,5.43555c1.44932,0.61717 2.59271,0.98981 3.45898,1.26172c1.60539,0.5041 3.06762,0.42747 4.16602,0.26563c0.82216,-0.12108 1.72641,-0.51584 2.62109,-1.08203c0.89469,-0.56619 1.77153,-1.2702 2.1582,-2.33984c0.27701,-0.76683 0.41783,-1.47548 0.46875,-2.05859c0.02546,-0.29156 0.02869,-0.54888 0.00977,-0.78711c-0.01897,-0.23823 0.0013,-0.42071 -0.2207,-0.78516c-0.46557,-0.76441 -0.99283,-0.78437 -1.54297,-1.05664c-0.30567,-0.15128 -1.17595,-0.57625 -2.04883,-0.99219c-0.8719,-0.41547 -1.62686,-0.78344 -2.0918,-0.94922c-0.29375,-0.10568 -0.65243,-0.25782 -1.16992,-0.19922c-0.51749,0.0586 -1.0286,0.43198 -1.32617,0.87305c-0.28205,0.41807 -1.4175,1.75835 -1.76367,2.15234c-0.0046,-0.0028 0.02544,0.01104 -0.11133,-0.05664c-0.42813,-0.21189 -0.95173,-0.39205 -1.72656,-0.80078c-0.77483,-0.40873 -1.74407,-1.01229 -2.80469,-1.94727v-0.00195c-1.57861,-1.38975 -2.68437,-3.1346 -3.0332,-3.7207c0.0235,-0.02796 -0.00279,0.0059 0.04687,-0.04297l0.00195,-0.00195c0.35652,-0.35115 0.67247,-0.77056 0.93945,-1.07812c0.37854,-0.43609 0.54559,-0.82052 0.72656,-1.17969c0.36067,-0.71583 0.15985,-1.50352 -0.04883,-1.91797v-0.00195c0.01441,0.02867 -0.11288,-0.25219 -0.25,-0.57617c-0.13751,-0.32491 -0.31279,-0.74613 -0.5,-1.19531c-0.37442,-0.89836 -0.79243,-1.90595 -1.04102,-2.49609v-0.00195c-0.29285,-0.69513 -0.68904,-1.1959 -1.20703,-1.4375c-0.51799,-0.2416 -0.97563,-0.17291 -0.99414,-0.17383h-0.00195c-0.36964,-0.01705 -0.77527,-0.02148 -1.17773,-0.02148zM16.64258,15c0.38554,0 0.76564,0.0047 1.08398,0.01953c0.32749,0.01632 0.30712,0.01766 0.24414,-0.01172c-0.06399,-0.02984 0.02283,-0.03953 0.20898,0.40234c0.24341,0.57785 0.66348,1.58909 1.03906,2.49023c0.18779,0.45057 0.36354,0.87343 0.50391,1.20508c0.14036,0.33165 0.21642,0.51683 0.30469,0.69336v0.00195l0.00195,0.00195c0.08654,0.17075 0.07889,0.06143 0.04883,0.12109c-0.21103,0.41883 -0.23966,0.52166 -0.45312,0.76758c-0.32502,0.37443 -0.65655,0.792 -0.83203,0.96484c-0.15353,0.15082 -0.43055,0.38578 -0.60352,0.8457c-0.17323,0.46063 -0.09238,1.09262 0.18555,1.56445c0.37003,0.62819 1.58941,2.6129 3.48438,4.28125c1.19338,1.05202 2.30519,1.74828 3.19336,2.2168c0.88817,0.46852 1.61157,0.74215 1.77344,0.82227c0.38438,0.19023 0.80448,0.33795 1.29297,0.2793c0.48849,-0.05865 0.90964,-0.35504 1.17773,-0.6582l0.00195,-0.00195c0.3568,-0.40451 1.41702,-1.61513 1.92578,-2.36133c0.02156,0.0076 0.0145,0.0017 0.18359,0.0625v0.00195h0.00195c0.0772,0.02749 1.04413,0.46028 1.90625,0.87109c0.86212,0.41081 1.73716,0.8378 2.02148,0.97852c0.41033,0.20308 0.60422,0.33529 0.6543,0.33594c0.00338,0.08798 0.0068,0.18333 -0.00586,0.32813c-0.03507,0.40164 -0.14243,0.95757 -0.35742,1.55273c-0.10532,0.29136 -0.65389,0.89227 -1.3457,1.33008c-0.69181,0.43781 -1.53386,0.74705 -1.8457,0.79297c-0.9376,0.13815 -2.05083,0.18859 -3.27344,-0.19531c-0.84773,-0.26609 -1.90476,-0.61053 -3.27344,-1.19336c-2.77581,-1.18381 -5.13503,-3.19825 -6.82031,-4.97266c-0.84264,-0.8872 -1.51775,-1.71309 -1.99805,-2.33789c-0.4794,-0.62364 -0.68874,-0.94816 -0.86328,-1.17773l-0.00195,-0.00195c-0.30983,-0.40973 -2.20703,-3.04868 -2.20703,-5.42578c0,-2.51576 1.1685,-3.50231 1.80078,-4.18359c0.33194,-0.35766 0.69484,-0.41016 0.8418,-0.41016z"></path></g></g>
</svg></div></div></a></div>
<div class="wrap_wa_list">
    <ul>
';
        
        if( !empty( $wa_room ) ) {
            $wa_html .= '<li><a  href="https://wa.me/'. $wa_room .'" target="_blank">Villa Reservation</a></li>';
        }
        
        if( !empty( $wa_restaurant ) ) {
             $wa_html .= '<li><a  href="https://wa.me/'. $wa_restaurant .'" target="_blank">Restaurant Reservation</a></li>';
        }
        
        if( !empty( $wa_inhouse_guest ) ) {
            $wa_html .= '<li><a  href="https://wa.me/'. $wa_inhouse_guest .'" target="_blank">In-house Guest Inquiry</a></li>';
        }

        $wa_html .= '</ul></div>';
        
         add_variable('wa_numb', $wa_html);
    }
    
    parse_template( 'home-block', 'h-block', false );

    return return_template( 'h-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Title Homepage Destination Hero
| -------------------------------------------------------------------------------------------------------------------------
*/
function title_hero_destination($post, $lang='', $check_lang=false)
{
    $post_id = $post[0]['id'];
    $title   = $post[0]['title'];

    
    $title_home_page    = get_additional_field($post_id, 'title_home_page', 'destinations');
    $subtitle_home_page = get_additional_field($post_id, 'subtitle_home_page', 'destinations');
    
    $pop_up_link_video   = get_additional_field( $post_id, 'pop_up_link_video', 'destinations' );
    $query_str = parse_url($pop_up_link_video, PHP_URL_QUERY);

    parse_str($query_str, $query_params);

    if(!empty($pop_up_link_video)){
        $link_video = $query_params['v'];
        $display  = '';       
    }else{
        $link_video = '';
        $display  = 'display:none;';    
    }

    add_variable('link_video', $link_video);
    add_variable('display', $display);

    add_variable('link_video', $link_video);

    if($check_lang)
    {
        $title_home_page_lang    = get_additional_field($post_id, 'title_home_page_'.$lang, 'destinations');
        $subtitle_home_page_lang = get_additional_field($post_id, 'subtitle_home_page_'.$lang, 'destinations');

        $title_home_page    = (empty($title_home_page_lang) ? $title_home_page:       $title_home_page_lang);
        $subtitle_home_page = (empty($subtitle_home_page_lang) ? $subtitle_home_page: $subtitle_home_page_lang);
    }

    if(strpos(strtolower($title_home_page), ' of '))
    {
        $title_home_page = explode_title_by_at_of(" of ", strtolower($title_home_page));
    }

    $html = '
        <div class="container container-title-hero clearfix">
            <h3 class="text _text text-sub-title">'.$subtitle_home_page.'</h3>
            <h1 class="text _text text-title-1">'.$title_home_page.'</h1>
        </div>
    ';
    return $html;
}
?>