<?php
/*
	Title: Samaya Template
	Preview: -
	Author: Tantri Mindrawan
	Author Url: http://www.lumonata.com
	Description: Custom template for samaya
*/

header( 'Access-Control-Allow-Origin: *' );

require_once 'functions.php';

$string_translations = get_string_translation_lang();

$version = "?v=2.2.3";
add_actions( 'version_js_css', $version );


if( is_ajax_page() )
{    
    set_template( TEMPLATE_PATH . '/index.html' );

    add_block( 'main-block', 'm-block' );

    add_variable( 'content-area', ajax_page_content() );

}
elseif(is_home() || is_language_home())
{
    set_template( TEMPLATE_PATH . '/index.html' );

    $cek_url    = cek_url();
    $lang       = $cek_url['0'];
    $check_lang = check_language_code($lang);

    set_corporate_list($lang, $check_lang, $string_translations);   
    set_about_us_landing($lang, $check_lang);
    set_switch_language();
    
    add_block( 'main-block', 'm-block' );

    set_variable_string_translations($lang, $check_lang, $string_translations);

    $site_url = HTTP.site_url();
    $appcls   = 'home-page';

    add_variable( 'booking_popup_accommodation', attemp_actions( 'booking_popup_accommodation' ) );
    add_variable( 'landing_page_welcome_popup', attemp_actions( 'landing_page_welcome_popup' ) );

    // CONFIG META DATA
    $meta_title       = get_meta_data( 'meta_title', 'global_setting' );
    $meta_keywords    = get_meta_keywords_tag(get_meta_data( 'meta_keywords', 'global_setting' ));
    $meta_description = get_meta_data( 'meta_description', 'global_setting' );
    $og_url           = '';
    $og_title         = '';
    $og_image         = '';
    $og_site_name     = '';
    $og_description   = '';

    add_meta_data_value($meta_title, $meta_keywords, $meta_description, $og_url, $og_title, $og_image, $og_site_name, $og_description);
}
elseif(is_cronjob_scrapping())
{
    $site_url = HTTP.site_url();
    $appcls  = '';
    scrapping_save_database();
}
else
{
    $appname             = trim(get_appname());
    $cek_url             = cek_url();
    $appcls              = '';
    $site_url            = HTTP.site_url().'/'.$appname;
    $thecontent          = "";
    $title_hero          = "";
    $bg_hero             = "";
    $header_layout       = "";
    $social_media_footer = "";
    $lang                = "";
    $check_lang          = false;

    set_template( TEMPLATE_PATH . '/template.html' );

    add_block( 'main-block', 'm-block' );

    if(check_language_code($appname) || check_destination($appname))
    {
        $corporatedata  = "";
        $header_menu    = "";
        $footer_menu    = "";
        $class_body_add = "";

        $lang       = $cek_url['0'];
        $check_lang = check_language_code($lang);

        $language_page = "";
        if($check_lang)
        {
            $class_body_add = $lang;
            $language_page = $lang;
            add_variable('language_page', $language_page);
        }

        if(check_destination($appname))
        {
            $corporatedata = get_articles_data('destinations', $appname, $lang, $check_lang);
            $header_menu   = the_menus( 'menuset='.ucfirst($appname).' Header&show_title=false', $lang, $check_lang );
            $footer_menu   = the_menus( 'menuset='.ucfirst($appname).' Footer&show_title=false', $lang, $check_lang );
        }
        elseif(check_destination($cek_url[1]))
        {
            $corporatedata = get_articles_data('destinations', $cek_url[1], $lang, $check_lang);
            $header_menu   = the_menus( 'menuset='.ucfirst($cek_url[1]).' Header&show_title=false', $lang, $check_lang );
            $footer_menu   = the_menus( 'menuset='.ucfirst($cek_url[1]).' Footer&show_title=false', $lang, $check_lang );
        }

        if(!empty($corporatedata))
        {
            $appsef               = get_uri_sef();
            $destination_id       = $corporatedata[0]['id'];
            $additional_corporate = get_additional_destination_data($destination_id);

            $properties_name = get_additional_field($destination_id,'fb_properties_name','destinations');
            $hotels_name     = get_additional_field($destination_id,'fb_hotels_name','destinations');
            add_actions('properties_name', $properties_name);
            add_actions('hotels_name', $hotels_name);
        
            if(get_uri_count() == 1)
            {
                $bg_hero           = $corporatedata[0]['bg_homepage'];
                $appcls            = "home-destination page-$appname $class_body_add page-all";
                $title_hero        = title_hero_destination($corporatedata);
                $thecontent        = home_destination_content($corporatedata, $appname, $destination_id, $lang, $check_lang, $string_translations);
                $header_layout     = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
            }
            elseif(get_uri_count() == 2)
            {
                // MENGECEK URL ARRAY PERTAMA APAKAH TERMASUK DALAM DESTINATION DATA ATAU TIDAK
                if(check_destination($cek_url[1]))
                {
                    $appname       = $cek_url[1];
                    $bg_hero       = $corporatedata[0]['bg_homepage'];
                    $appcls        = "home-destination page-$appname $class_body_add page-all";
                    $title_hero    = title_hero_destination($corporatedata, $lang, $check_lang);
                    $thecontent    = home_destination_content($corporatedata, $appname, $destination_id, $lang, $check_lang, $string_translations);
                    $header_layout = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
                }
                else
                {
                    if($appsef == 'gallery')
                    {
                        $appsef = "press-gallery";
                    }
                    $appcls     = "$appsef page-$appname page $class_body_add page-all";
                    $data_page  = get_data_page($appsef, $destination_id, $lang, $check_lang, $string_translations);
                    $thecontent = get_page_content($corporatedata, $appsef, $data_page, $lang, $check_lang, $string_translations);
    
                    if(!empty($data_page))
                    {
                        $title_hero    = $data_page['title_hero_template'];
                        $bg_hero       = $data_page['bg_hero'];
                    }
                    
                    if($appsef != "special-offers" && $appsef != "news" && $appsef != 'press-download' && $appsef != 'press-gallery' && $appsef != 'gallery' && $appsef != 'press-accolades' && $appsef != 'contact-us' && $appsef != 'locations')
                    {
                        $header_layout     = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
                    }
                    else
                    {
                        get_list_dest($lang, $check_lang);
                        $appcls     = "$appsef page-$appname page navbar-show page-all";
                    }
                }
            }
            elseif(get_uri_count() == 3)
            {
                if(check_destination($cek_url[1]))
                {
                    if($appsef == 'gallery')
                    {
                        $appsef = "press-gallery";
                    }
                    
                    $appname    = $cek_url[1];
                    $appcls     = "$appsef page-$appname page $class_body_add page-all";
                    $data_page  = get_data_page($appsef, $destination_id, $lang, $check_lang, $string_translations);
                    $thecontent = get_page_content($corporatedata, $appsef, $data_page, $lang, $check_lang, $string_translations);
    
                    if(!empty($data_page))
                    {
                        $title_hero    = $data_page['title_hero_template'];
                        $bg_hero       = $data_page['bg_hero'];
                    }
                    
                    if($appsef != "special-offers" && $appsef != "news" && $appsef != 'press-download' && $appsef != 'press-gallery' && $appsef != 'gallery' && $appsef != 'press-accolades' && $appsef != 'contact-us' && $appsef != 'locations')
                    {
                        $header_layout     = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
                    }
                    else
                    {
                        get_list_dest($lang, $check_lang);
                        $appcls     = "$appsef page-$appname page navbar-show $class_body_add page-all";
                    }
                }
                else
                {
                    $type_product = $cek_url[1];
                    $appcls       = "page-$appname page-detail $type_product-detail $class_body_add page-all";
                    $data_page    = get_data_detail_page($appsef, $appname, $type_product, $lang, $check_lang, $string_translations);
                    $thecontent   = get_page_detail_content($corporatedata, $appsef, $data_page, $lang, $check_lang, $string_translations);
    
                    if(!empty($data_page))
                    {
                        $title_hero    = $data_page['title_hero_template'];
                        $bg_hero       = $data_page['bg_hero'];
                    }
    
                    $header_layout     = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
                }
            }
            elseif(get_uri_count() == 4)
            {
                $appname      = $cek_url[1];
                $type_product = $cek_url[2];
                $appcls       = "page-$appname page-detail $type_product-detail $class_body_add page-all";
                $data_page    = get_data_detail_page($appsef, $appname, $type_product, $lang, $check_lang, $string_translations);
                $thecontent   = get_page_detail_content($corporatedata, $appsef, $data_page, $lang, $check_lang, $string_translations);

                if(!empty($data_page))
                {
                    $title_hero    = $data_page['title_hero_template'];
                    $bg_hero       = $data_page['bg_hero'];
                }

                $header_layout     = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
            }

            $social_media_footer = $corporatedata[0]['social_media']['social_media_footer'];

            set_other_destination($appname, $lang, $check_lang);

            // FOOTER IMAGE
            $footer_image = get_additional_field($destination_id,'footer_image','destinations');
            $footer_image = HTTP.SITE_URL.'/lumonata-plugins/destinations/footer/large/'.$footer_image;
            add_variable( 'footer_image', $footer_image );

        }
        else
        {
            $appcls = "page-404 page-all";
        }

    }
    else
    {
        $appcls = "page-404 page-all";
    }

    $home_url = HTTP.site_url().'/'.$appname;
    if($check_lang)
    {
        $home_url = HTTP.site_url().'/'.$lang.'/'.$appname;
    }

    $mobile_menu = $footer_menu;
    add_variable( 'header_layout', $header_layout );
    add_variable( 'content_area', $thecontent );
    add_variable( 'appname_upper', strtoupper($appname) );
    add_variable( 'appname', $appname );
    add_variable( 'appname_ucwords', ucwords($appname) );
    add_variable( 'header_menu', $header_menu );
    add_variable( 'footer_menu', $footer_menu );
    add_variable( 'mobile_popup_menu', $mobile_menu );
    add_variable( 'home_url', $home_url );
    add_variable( 'social_media_footer', $social_media_footer );
    add_variable( 'booking_popup_accommodation', attemp_actions( 'booking_popup_accommodation' ) );
    add_variable( 'booking_popup_spa', attemp_actions( 'booking_popup_spa' ) );
    add_variable( 'booking_popup_wedding', attemp_actions( 'booking_popup_wedding' ) );
    add_variable( 'booking_popup_events', attemp_actions( 'booking_popup_events' ) );
    add_variable( 'booking_popup_offers', attemp_actions( 'booking_popup_offers' ) );
    // add_variable( 'booking_popup_dinings', attemp_actions( 'booking_popup_dinings' ) );
}

// GOOGLE TAG MANAGER 
$google_tag_manager = get_meta_data("google_tag_manager", 'static_setting');
$google_tag_manager_noscript = get_meta_data("google_tag_manager_noscript", 'static_setting');

add_variable( 'google_tag_manager', $google_tag_manager );
add_variable( 'google_tag_manager_noscript', $google_tag_manager_noscript );
add_variable( 'year', date( 'Y' ) );
add_variable( 'body_class', $appcls );
add_variable( 'web_title', web_title() );
add_variable( 'tagline', web_tagline() );
add_variable( 'meta_description', attemp_actions( 'meta_description' ) );
add_variable( 'meta_keywords', attemp_actions( 'meta_keywords' ) );
add_variable( 'meta_title', attemp_actions( 'meta_title' ) );
add_variable( 'og_url', attemp_actions( 'og_url' ) );
add_variable( 'og_title', attemp_actions( 'og_title' ) );
add_variable( 'og_image', attemp_actions( 'og_image' ) );
add_variable( 'og_site_name', attemp_actions( 'og_site_name' ) );
add_variable( 'og_description', attemp_actions( 'og_description' ) );
add_variable( 'version', $version );
add_variable( 'site_url', $site_url );
add_variable( 'HTTP', HTTP );
add_variable( 'web_url', HTTP.site_url() );
add_variable( 'template_url', TEMPLATE_URL );
add_variable( 'assets_url', TEMPLATE_URL.'/assets' );
add_variable( 'include-js', attemp_actions( 'include-js' ) );
add_variable( 'include-js-contact', attemp_actions( 'include-js-contact' ) );
add_variable( 'include-css', attemp_actions( 'include-css' ) );
add_variable( 'code_rest_diary', attemp_actions( 'code_rest_diary' ) );
add_variable( 'code_rest_diary_page_list', attemp_actions( 'code_rest_diary_page_list' ) );

parse_template( 'main-block', 'm-block' );

print_template();
?>