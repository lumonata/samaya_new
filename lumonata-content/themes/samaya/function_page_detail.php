<?php
/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENAMPILKAN TITLE HERO PADA DETAIL PAGE
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_data_detail_page($sef, $destination, $type, $lang='', $check_lang=false, $string_translations='')
{
    global $db;

    // STRING TRANSLATIONS
    $translations = array('Accommodation', 'Dining', 'Spa', 'Events', 'Weddings', 'What To Do Act In', 'What To Do Around', 'Special Offers', 'News');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );
    $result = array();

    //$data = get_post_detail( $sef, $type );
    $data = get_post_detail_by_dest( $sef, $destination, $type );

    if(!empty($data))
    {
        extract( $data );

        $post_title_new = $post_title;

        if($type != "spa")
        {
            if(strpos($post_title, ' of '))
            {
                $post_title_new = explode_title_by_at_of(" of ", $post_title_new);
            }
            elseif(strpos($post_title_new, ' at '))
            {
                $post_title_new = explode_title_by_at_of(" at ", $post_title_new);
            }
        }

        // GET SLIDER HERO PAGE
        if($type == "accommodation" || $type == "dining" || $type == "what-to-do-act-in" || $type == "what-to-do-around" || $type == "events")
        {
            if(!empty($post_attachment))
            {
                add_actions( 'slider_hero_page', 'get_slider_hero_page', $post_attachment, $post_title );
            }
        }


        $type_new = $type;
        if(strpos($type, '-'))
        {
            $explode_type = explode("-", $type_new);
            $type_new = "";
            foreach($explode_type as $d)
            {
                $type_new .= $d." ";
            }
        }

        $type_for_tr_string = str_replace("-", "_", $type);
        $subtitle = $tr_string[$type_for_tr_string];
        $subtitle = (empty($post_subtitle) ? strtoupper($subtitle) : $post_subtitle);

        // META DATA
        $meta_title         = get_additional_field( $post_id, 'meta_title', $type );
        $meta_keywords      = get_additional_field( $post_id, 'meta_keywords', $type );
        $meta_description   = get_additional_field( $post_id, 'meta_description', $type );

        if($check_lang)
        {

            $post_title_new_lang   = get_additional_field($post_id, 'title_'.$lang, $type);
            $subtitle_lang         = get_additional_field($post_id, 'subtitle_'.$lang, $type);
            $content_lang          = get_additional_field($post_id, 'content_'.$lang, $type);
            $brief_lang            = get_additional_field($post_id, 'brief_'.$lang, $type);
            $meta_title_lang       = get_additional_field( $post_id, 'meta_title_'.$lang, $type );

            // die($post_id . '- maintenance');
            $meta_keywords_lang    = get_additional_field( $post_id, 'meta_keywords_'.$lang, $type );
            $meta_description_lang = get_additional_field( $post_id, 'meta_description_'.$lang, $type );
            
            $post_title_new   = (empty($post_title_new_lang) ? $post_title_new:     $post_title_new_lang);
            $post_title       = (empty($post_title_new_lang) ? $post_title:         $post_title_new_lang);
            $subtitle         = (empty($subtitle_lang) ? $subtitle:                 $subtitle_lang);
            $post_brief       = (empty($brief_lang) ? $post_brief:                  $brief_lang);
            $post_content     = (empty($content_lang) ? $post_content:              $content_lang);
            $meta_title       = (empty($meta_title_lang) ? $meta_title:             $meta_title_lang);
            $meta_keywords    = (empty($meta_keywords_lang) ? $meta_keywords:       $meta_keywords_lang);
            $meta_description = (empty($meta_description_lang) ? $meta_description: $meta_description_lang);
        }


        $result['title_hero_template'] = '
            <div class="container container-title-hero-detail-page clearfix">
                <h2 class="text _text sub-title-detail-page">'.$subtitle.'</h2>
                <h1 class="text _text title-detail-page-1">'.$post_title_new.'</h1>
                <!--<p class="text _text title-detail-page-2">COURTYARD VILLA</p>-->
            </div>
        ';
        
        $result['bg_hero']      = "";
        $result['post_title']   = $post_title;
        $result['post_sef']     = $post_sef;
        $result['post_content'] = $post_content;
        $result['post_brief']   = $post_brief;
        $result['post_id']      = $post_id;
        $result['post_type']    = $post_type;
        $result['metatitle']    = $meta_title;
        $result['metakey']      = $meta_keywords;
        $result['metadesc']     = $meta_description;
        $result['og_image']     = "";

        if(!empty($post_featured_img))
        {
            if($type != "accommodation" && $type != "dining" && $type != "what-to-do-act-in" && $type != "what-to-do-around" && $type != "events")
            {
                $result['bg_hero']      = HTTP.site_url().$post_featured_img['large'];
            }
        }
    }

    return $result;
}


function get_slider_hero_page($attachment='', $title_page)
{
    set_template( TEMPLATE_PATH . '/layout/header_slide.html', 'header_slide_template' );
    add_block( 'loop-popup-slide-block', 'lpsblock', 'header_slide_template' );
    add_block( 'loop-header-slide-block', 'lhsblock', 'header_slide_template' );
    add_block( 'header-slide-block', 'hsblock', 'header_slide_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    set_switch_language();

    add_variable( 'title_page_popup', $title_page);

    if(!empty($attachment))
    {
        foreach($attachment as $d)
        {
            $img_large = $d['img_large'];

            add_variable( 'img_large', $img_large);

            parse_template( 'loop-popup-slide-block', 'lpsblock', true );
            parse_template( 'loop-header-slide-block', 'lhsblock', true );
        }
    }

    parse_template( 'header-slide-block', 'hsblock', false );
    
    return return_template( 'header_slide_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Accomodation Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function accommodation_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/accommodation_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // STRING TRANSLATIONS
    $translations = array('Accommodation', 'Single', 'King Size', 'Double', 'Double Twin', 'Villa Size', 'Bed Type', 'Maximum Capacity', 'Pool Size', 'Villa Benefits', 'Villa Amenities', 'Villa Location');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];
        
        // ADDITIONAL FIELD
        $size             = get_additional_field( $post_id, 'size', 'accommodation' );
        $occupancy        = get_additional_field( $post_id, 'occupancy', 'accommodation' );
        $pool_size        = get_additional_field( $post_id, 'pool_size', 'accommodation' );
        $bed              = get_additional_field( $post_id, 'bed', 'accommodation' );
        $features         = get_additional_field( $post_id, 'features', 'accommodation' );
        $benefits         = get_additional_field( $post_id, 'benefits', 'accommodation' );
        $map_location_img = get_map_location_image( $post_id, 'accommodation', false );

        $wa                = get_additional_field($corporatedata[0]['id'], 'villa_reservation', 'destinations');;
        $wa_restaurant     = get_additional_field($corporatedata[0]['id'], 'restaurant_reservation', 'destinations');
        $wa_inhouse_guest  = get_additional_field($corporatedata[0]['id'], 'guest_reservation', 'destinations');
        
        $wa_room = isset( $wa ) && !empty( $wa ) ? $wa : '';
        $wa_restaurant = isset( $wa_restaurant ) && !empty( $wa_restaurant ) ? $wa_restaurant : '';
        $wa_inhouse_guest = isset( $wa_inhouse_guest ) && !empty( $wa_inhouse_guest ) ? $wa_inhouse_guest : '';
        
        
        if( !empty( $wa_room ) || !empty( $wa_restaurant ) ||  !empty( $wa_inhouse_guest ) ) {
        $wa_html = '<div class="floating_wa">
            <a>
                <div class="bg-[#A6631B] rounded-[50%] p-4 ml-4 mr-4 overflow-hidden flex justify-center align-center"><div class="font-bold text-white flex justify-center align-center"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="40" height="60" viewBox="0,0,256,256" style="fill:#000000;">
            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.12,5.12)"><path d="M25,2c-12.69047,0 -23,10.30953 -23,23c0,4.0791 1.11869,7.88588 2.98438,11.20898l-2.94727,10.52148c-0.09582,0.34262 -0.00241,0.71035 0.24531,0.96571c0.24772,0.25536 0.61244,0.35989 0.95781,0.27452l10.9707,-2.71875c3.22369,1.72098 6.88165,2.74805 10.78906,2.74805c12.69047,0 23,-10.30953 23,-23c0,-12.69047 -10.30953,-23 -23,-23zM25,4c11.60953,0 21,9.39047 21,21c0,11.60953 -9.39047,21 -21,21c-3.72198,0 -7.20788,-0.97037 -10.23828,-2.66602c-0.22164,-0.12385 -0.48208,-0.15876 -0.72852,-0.09766l-9.60742,2.38086l2.57617,-9.19141c0.07449,-0.26248 0.03851,-0.54399 -0.09961,-0.7793c-1.84166,-3.12289 -2.90234,-6.75638 -2.90234,-10.64648c0,-11.60953 9.39047,-21 21,-21zM16.64258,13c-0.64104,0 -1.55653,0.23849 -2.30859,1.04883c-0.45172,0.48672 -2.33398,2.32068 -2.33398,5.54492c0,3.36152 2.33139,6.2621 2.61328,6.63477h0.00195v0.00195c-0.02674,-0.03514 0.3578,0.52172 0.87109,1.18945c0.5133,0.66773 1.23108,1.54472 2.13281,2.49414c1.80347,1.89885 4.33914,4.09336 7.48633,5.43555c1.44932,0.61717 2.59271,0.98981 3.45898,1.26172c1.60539,0.5041 3.06762,0.42747 4.16602,0.26563c0.82216,-0.12108 1.72641,-0.51584 2.62109,-1.08203c0.89469,-0.56619 1.77153,-1.2702 2.1582,-2.33984c0.27701,-0.76683 0.41783,-1.47548 0.46875,-2.05859c0.02546,-0.29156 0.02869,-0.54888 0.00977,-0.78711c-0.01897,-0.23823 0.0013,-0.42071 -0.2207,-0.78516c-0.46557,-0.76441 -0.99283,-0.78437 -1.54297,-1.05664c-0.30567,-0.15128 -1.17595,-0.57625 -2.04883,-0.99219c-0.8719,-0.41547 -1.62686,-0.78344 -2.0918,-0.94922c-0.29375,-0.10568 -0.65243,-0.25782 -1.16992,-0.19922c-0.51749,0.0586 -1.0286,0.43198 -1.32617,0.87305c-0.28205,0.41807 -1.4175,1.75835 -1.76367,2.15234c-0.0046,-0.0028 0.02544,0.01104 -0.11133,-0.05664c-0.42813,-0.21189 -0.95173,-0.39205 -1.72656,-0.80078c-0.77483,-0.40873 -1.74407,-1.01229 -2.80469,-1.94727v-0.00195c-1.57861,-1.38975 -2.68437,-3.1346 -3.0332,-3.7207c0.0235,-0.02796 -0.00279,0.0059 0.04687,-0.04297l0.00195,-0.00195c0.35652,-0.35115 0.67247,-0.77056 0.93945,-1.07812c0.37854,-0.43609 0.54559,-0.82052 0.72656,-1.17969c0.36067,-0.71583 0.15985,-1.50352 -0.04883,-1.91797v-0.00195c0.01441,0.02867 -0.11288,-0.25219 -0.25,-0.57617c-0.13751,-0.32491 -0.31279,-0.74613 -0.5,-1.19531c-0.37442,-0.89836 -0.79243,-1.90595 -1.04102,-2.49609v-0.00195c-0.29285,-0.69513 -0.68904,-1.1959 -1.20703,-1.4375c-0.51799,-0.2416 -0.97563,-0.17291 -0.99414,-0.17383h-0.00195c-0.36964,-0.01705 -0.77527,-0.02148 -1.17773,-0.02148zM16.64258,15c0.38554,0 0.76564,0.0047 1.08398,0.01953c0.32749,0.01632 0.30712,0.01766 0.24414,-0.01172c-0.06399,-0.02984 0.02283,-0.03953 0.20898,0.40234c0.24341,0.57785 0.66348,1.58909 1.03906,2.49023c0.18779,0.45057 0.36354,0.87343 0.50391,1.20508c0.14036,0.33165 0.21642,0.51683 0.30469,0.69336v0.00195l0.00195,0.00195c0.08654,0.17075 0.07889,0.06143 0.04883,0.12109c-0.21103,0.41883 -0.23966,0.52166 -0.45312,0.76758c-0.32502,0.37443 -0.65655,0.792 -0.83203,0.96484c-0.15353,0.15082 -0.43055,0.38578 -0.60352,0.8457c-0.17323,0.46063 -0.09238,1.09262 0.18555,1.56445c0.37003,0.62819 1.58941,2.6129 3.48438,4.28125c1.19338,1.05202 2.30519,1.74828 3.19336,2.2168c0.88817,0.46852 1.61157,0.74215 1.77344,0.82227c0.38438,0.19023 0.80448,0.33795 1.29297,0.2793c0.48849,-0.05865 0.90964,-0.35504 1.17773,-0.6582l0.00195,-0.00195c0.3568,-0.40451 1.41702,-1.61513 1.92578,-2.36133c0.02156,0.0076 0.0145,0.0017 0.18359,0.0625v0.00195h0.00195c0.0772,0.02749 1.04413,0.46028 1.90625,0.87109c0.86212,0.41081 1.73716,0.8378 2.02148,0.97852c0.41033,0.20308 0.60422,0.33529 0.6543,0.33594c0.00338,0.08798 0.0068,0.18333 -0.00586,0.32813c-0.03507,0.40164 -0.14243,0.95757 -0.35742,1.55273c-0.10532,0.29136 -0.65389,0.89227 -1.3457,1.33008c-0.69181,0.43781 -1.53386,0.74705 -1.8457,0.79297c-0.9376,0.13815 -2.05083,0.18859 -3.27344,-0.19531c-0.84773,-0.26609 -1.90476,-0.61053 -3.27344,-1.19336c-2.77581,-1.18381 -5.13503,-3.19825 -6.82031,-4.97266c-0.84264,-0.8872 -1.51775,-1.71309 -1.99805,-2.33789c-0.4794,-0.62364 -0.68874,-0.94816 -0.86328,-1.17773l-0.00195,-0.00195c-0.30983,-0.40973 -2.20703,-3.04868 -2.20703,-5.42578c0,-2.51576 1.1685,-3.50231 1.80078,-4.18359c0.33194,-0.35766 0.69484,-0.41016 0.8418,-0.41016z"></path></g></g>
            </svg></div></div></a></div>
            <div class="wrap_wa_list">
                <ul>
            ';
            
            if( !empty( $wa_room ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_room .'" target="_blank">Villa Reservation</a></li>';
            }
            
            if( !empty( $wa_restaurant ) ) {
                 $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_restaurant .'" target="_blank">Restaurant Reservation</a></li>';
            }
            
            if( !empty( $wa_inhouse_guest ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_inhouse_guest .'" target="_blank"> In-house Guest Inquiry</a></li>';
            }
    
            $wa_html .= '</ul></div>';
            
             add_variable('wa_numb', $wa_html);
        }
    
        if($check_lang)
        {
            $str_replace_bed = strtolower(str_replace("/ ", "", $bed));
            $str_replace_bed = strtolower(str_replace(" ", "_", $str_replace_bed));
            $bed_lang        = $tr_string[$str_replace_bed];
            $size_lang       = get_additional_field($post_id, 'size_'.$lang, 'accommodation');
            $occupancy_lang  = get_additional_field($post_id, 'occupancy_'.$lang, 'accommodation');
            $pool_size_lang  = get_additional_field($post_id, 'pool_size_'.$lang, 'accommodation');
            $features_lang   = get_additional_field($post_id, 'features_'.$lang, 'accommodation');
            $benefits_lang   = get_additional_field($post_id, 'benefits_'.$lang, 'accommodation');
            
            $size      = (empty($size_lang) ? $size:           $size_lang);
            $pool_size = (empty($pool_size_lang) ? $pool_size: $pool_size_lang);
            $occupancy = (empty($occupancy_lang) ? $occupancy: $occupancy_lang);
            $benefits  = (empty($benefits_lang) ? $benefits:   $benefits_lang);
            $features  = (empty($features_lang) ? $features:   $features_lang);
            $bed       = (empty($bed_lang) ? $bed:             $bed_lang);
        }


        // VILLA DETAILS
        $villa_details = "";
        if(!empty($size))
        {
            $villa_details .= '
                <div class="container container-detail-acco clearfix">
                    <p class="text _text text-label">'.$tr_string['villa_size'].'</p>
                    <p class="text _text text-value">'.$size.'</p>
                </div>
            ';
        }
        if(!empty($bed))
        {
            $villa_details .= '
                <div class="container container-detail-acco clearfix">
                    <p class="text _text text-label">'.$tr_string['bed_type'].'</p>
                    <p class="text _text text-value">'.$bed.'</p>
                </div>
            ';
        }
        if(!empty($pool_size))
        {
            $villa_details .= '
                <div class="container container-detail-acco clearfix">
                    <p class="text _text text-label">'.$tr_string['pool_size'].'</p>
                    <p class="text _text text-value">'.$pool_size.'</p>
                </div>
            ';
        }
        if(!empty($occupancy))
        {
            $villa_details .= '
                <div class="container container-detail-acco clearfix">
                    <p class="text _text text-label">'.$tr_string['maximum_capacity'].'</p>
                    <p class="text _text text-value">'.$occupancy.'</p>
                </div>
            ';
        }
        add_variable( 'villa_details', $villa_details);

        // VILLA BENEFITS
        if(!empty($benefits))
        {
            $benefits_html = '
                <div class="benefits-villa" data-aos="fade-up">
                    <h2>'.$tr_string['villa_benefits'].'</h2>
                    <p>'.$benefits.'</p>
                </div>
            ';
            add_variable( 'benefits_html', $benefits_html);
        }
        
        $section_map_location = "";
        if(!empty($map_location_img))
        {
            $map_location_img_medium = HTTP.site_url().$map_location_img['original'];
            $section_map_location = '
                <div class="container-map-location" data-aos="fade-up">
                    <h2>'.$tr_string['villa_location'].'</h2>
                    <img src="'.$map_location_img_medium.'" />
                </div>
            ';
        }

        // VILLA AMENITIS
        $amenities_content = "";
        $amenities = get_rule_list_post($post_id, 'accommodation','amenities', 1, $lang, $check_lang);
        if(!empty($amenities))
        {
            $amenities_content = '
                <div class="container-features" data-aos="fade-up">
                    <h2>'.$tr_string['villa_amenities'].'</h2>
                    <div class="features-acco">
                        '.$amenities.'
                    </div>
                </div>
            ';
        }
        add_variable( 'amenities_content', $amenities_content);

        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'size', $size);
        add_variable( 'occupancy', $occupancy);
        add_variable( 'pool_size', $pool_size);
        add_variable( 'bed', $bed);
        add_variable( 'features', $features);
        add_variable( 'section_map_location', $section_map_location);
        add_variable( 'destination_upper', strtoupper($destination));

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('accommodation', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product       = $d['post_id'];
                $title_product    = $d['post_title'];
                // $subtitle_product = $d['post_subtitle'];
                // $subtitle_product = (empty($subtitle_product) ? $tr_string['accommodations']: $subtitle_product);
                $subtitle_product = $tr_string['accommodation'];
                $sef_product      = $d['post_sef'];
                $brief_product    = $d['post_brief'];
                $content_product  = $d['post_content'];
                $image_product    = get_featured_img( $id_product, 'accommodation', false );
                $image_product    = HTTP . site_url().$image_product['medium'];
                $post_link        = HTTP . site_url().'/'.$destination.'/accommodation/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang    = get_additional_field($id_product, 'title_'.$lang, 'accommodation');
                    $brief_product_lang    = get_additional_field($id_product, 'brief_'.$lang, 'accommodation');
                    $content_product_lang  = get_additional_field($id_product, 'content_'.$lang, 'accommodation');
                    $subtitle_product_lang = get_additional_field($id_product, 'subtitle_'.$lang, 'accommodation');
                    
                    $title_product    = (empty($title_product_lang) ? $title_product:       $title_product_lang);
                    $brief_product    = (empty($brief_product_lang) ? $brief_product:       $brief_product_lang);
                    $content_product  = (empty($content_product_lang) ? $content_product:   $content_product_lang);
                    $subtitle_product = (empty($subtitle_product_lang) ? $subtitle_product: $subtitle_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/accommodation/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('subtitle_product', $subtitle_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }
        
        $link_explore  = HTTP.site_url().'/'.$destination.'/accommodation';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/accommodation';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/accommodation/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/accommodation/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);
        set_canonical_link($post_data);
        $properties_name = get_additional_field($corporatedata[0]['id'],'fb_properties_name','destinations');
        $hotels_name     = get_additional_field($corporatedata[0]['id'],'fb_hotels_name','destinations');
        add_variable( 'properties_name', $properties_name );
        add_variable( 'hotels_name', $hotels_name );

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Dining Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function dining_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/dining_detail.html', 'page_template' );
    // add_block( 'loop-cat-menu-popup-block', 'lcmpblock', 'page_template' );
    add_block( 'loop-cat-menu-block', 'lcmblock', 'page_template' );
    // add_block( 'menu-dining-popup-block', 'mdpblock', 'page_template' );
    add_block( 'menu-dining-block', 'mdblock', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // add_actions( 'booking_popup_dinings', 'booking_popup_dinings_content' );

    add_variable( 'site_url', site_url() );
    add_variable( 'HTTP', HTTP );
    add_variable( 'web_url', HTTP.site_url() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // STRING TRANSLATIONS
    $translations = array('Download Our Menu', 'Download', 'Breakfast', 'Lunch', 'Dining', 'Restaurant Open', 'Dresscode', 'Book Offer Now', 'Book Now', 'Reserve Table', 'Reservation', 'Or', 'Asking Reservations Restaurant Online');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $label_link_hero      = $tr_string['reserve_table'];

    $email_dining_reservation = get_additional_field($corporatedata[0]['id'],'dining_reservation_email_address','destinations');
    add_variable( 'email_dining_reservation', $email_dining_reservation );

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // explode title
        $explode_title = explode(" at ", $post_title);
        
        // ADDITIONAL FIELD
        $breakfast_open          = get_additional_field( $post_id, 'breakfast_open', 'dining' );
        $lunch_open              = get_additional_field( $post_id, 'lunch_open', 'dining' );
        $dining_open             = get_additional_field( $post_id, 'dining_open', 'dining' );
        $dresscode               = get_additional_field( $post_id, 'dresscode', 'dining' );
        $our_menu_desc           = get_additional_field( $post_id, 'our_menu_desc', 'dining' );
        $menu_pdf                = get_additional_field( $post_id, 'menu_pdf', 'dining' );
        $menu_pdf_text_link      = get_additional_field( $post_id, 'menu_pdf_text_link', 'dining' );
        $code_rest_diary         = get_additional_field( $post_id, 'code_rest_diary', 'dining' );
        $code_chope              = get_additional_field( $post_id, 'code_chope', 'dining' );
        $detail_reservation_text = get_additional_field( $post_id, 'detail_reservation_text', 'dining' );


        $cta_link = get_additional_field( $post_id, 'cta_link', 'dining' );
        


        // print_r($code_rest_diary);
        // $hide_reservation = "hide";
        // if(!empty($code_rest_diary))
        // {
        //     $hide_reservation = "";
        //     add_actions( 'code_rest_diary', $code_rest_diary );

        //     // BUTTON BOOK NOW
        //     $button_book_hero = '
        //         <a href="#" class="container container-button-book-now-link book-now-hero clearfix" target="_blank">
        //             <p class="text _text text-book-now">'.$label_link_hero.'</p>
        //         </a>
        //     ';

        //     add_actions('button_book_hero', $button_book_hero);
        // }
        // add_variable( 'hide_reservation', $hide_reservation);

        $hide_reservation = "hide";
        $sttc = 'style="position: static;"';
        if(!empty($code_chope))
        {
            $hide_reservation = "";

            // BUTTON BOOK NOW
            $button_book_hero = '
                <a href="#" class="container container-button-book-now-link book-now-hero clearfix" target="_blank">
                    <p class="text _text text-book-now">'.$label_link_hero.'</p>
                </a>
            ';

            $sttc = '';

            add_actions('button_book_hero', $button_book_hero);
        }
        add_variable( 'hide_reservation', $hide_reservation);
        add_variable( 'sttc', $sttc);
        
        $data_page = get_meta_data('post_type_setting', 'accommodation', $corporatedata[0]['id']);
        
        $post_type_setting = json_decode($data_page, true);
       
       
         $wa                = get_additional_field($corporatedata[0]['id'], 'villa_reservation', 'destinations');;
        $wa_restaurant     = get_additional_field($corporatedata[0]['id'], 'restaurant_reservation', 'destinations');
        $wa_inhouse_guest  = get_additional_field($corporatedata[0]['id'], 'guest_reservation', 'destinations');
        
        $wa_room = isset( $wa ) && !empty( $wa ) ? $wa : '';
        $wa_restaurant = isset( $wa_restaurant ) && !empty( $wa_restaurant ) ? $wa_restaurant : '';
        $wa_inhouse_guest = isset( $wa_inhouse_guest ) && !empty( $wa_inhouse_guest ) ? $wa_inhouse_guest : '';
        
        
        if( !empty( $wa_room ) || !empty( $wa_restaurant ) ||  !empty( $wa_inhouse_guest ) ) {
        $wa_html = '<div class="floating_wa">
            <a>
                <div class="bg-[#A6631B] rounded-[50%] p-4 ml-4 mr-4 overflow-hidden flex justify-center align-center"><div class="font-bold text-white flex justify-center align-center"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="40" height="60" viewBox="0,0,256,256" style="fill:#000000;">
            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.12,5.12)"><path d="M25,2c-12.69047,0 -23,10.30953 -23,23c0,4.0791 1.11869,7.88588 2.98438,11.20898l-2.94727,10.52148c-0.09582,0.34262 -0.00241,0.71035 0.24531,0.96571c0.24772,0.25536 0.61244,0.35989 0.95781,0.27452l10.9707,-2.71875c3.22369,1.72098 6.88165,2.74805 10.78906,2.74805c12.69047,0 23,-10.30953 23,-23c0,-12.69047 -10.30953,-23 -23,-23zM25,4c11.60953,0 21,9.39047 21,21c0,11.60953 -9.39047,21 -21,21c-3.72198,0 -7.20788,-0.97037 -10.23828,-2.66602c-0.22164,-0.12385 -0.48208,-0.15876 -0.72852,-0.09766l-9.60742,2.38086l2.57617,-9.19141c0.07449,-0.26248 0.03851,-0.54399 -0.09961,-0.7793c-1.84166,-3.12289 -2.90234,-6.75638 -2.90234,-10.64648c0,-11.60953 9.39047,-21 21,-21zM16.64258,13c-0.64104,0 -1.55653,0.23849 -2.30859,1.04883c-0.45172,0.48672 -2.33398,2.32068 -2.33398,5.54492c0,3.36152 2.33139,6.2621 2.61328,6.63477h0.00195v0.00195c-0.02674,-0.03514 0.3578,0.52172 0.87109,1.18945c0.5133,0.66773 1.23108,1.54472 2.13281,2.49414c1.80347,1.89885 4.33914,4.09336 7.48633,5.43555c1.44932,0.61717 2.59271,0.98981 3.45898,1.26172c1.60539,0.5041 3.06762,0.42747 4.16602,0.26563c0.82216,-0.12108 1.72641,-0.51584 2.62109,-1.08203c0.89469,-0.56619 1.77153,-1.2702 2.1582,-2.33984c0.27701,-0.76683 0.41783,-1.47548 0.46875,-2.05859c0.02546,-0.29156 0.02869,-0.54888 0.00977,-0.78711c-0.01897,-0.23823 0.0013,-0.42071 -0.2207,-0.78516c-0.46557,-0.76441 -0.99283,-0.78437 -1.54297,-1.05664c-0.30567,-0.15128 -1.17595,-0.57625 -2.04883,-0.99219c-0.8719,-0.41547 -1.62686,-0.78344 -2.0918,-0.94922c-0.29375,-0.10568 -0.65243,-0.25782 -1.16992,-0.19922c-0.51749,0.0586 -1.0286,0.43198 -1.32617,0.87305c-0.28205,0.41807 -1.4175,1.75835 -1.76367,2.15234c-0.0046,-0.0028 0.02544,0.01104 -0.11133,-0.05664c-0.42813,-0.21189 -0.95173,-0.39205 -1.72656,-0.80078c-0.77483,-0.40873 -1.74407,-1.01229 -2.80469,-1.94727v-0.00195c-1.57861,-1.38975 -2.68437,-3.1346 -3.0332,-3.7207c0.0235,-0.02796 -0.00279,0.0059 0.04687,-0.04297l0.00195,-0.00195c0.35652,-0.35115 0.67247,-0.77056 0.93945,-1.07812c0.37854,-0.43609 0.54559,-0.82052 0.72656,-1.17969c0.36067,-0.71583 0.15985,-1.50352 -0.04883,-1.91797v-0.00195c0.01441,0.02867 -0.11288,-0.25219 -0.25,-0.57617c-0.13751,-0.32491 -0.31279,-0.74613 -0.5,-1.19531c-0.37442,-0.89836 -0.79243,-1.90595 -1.04102,-2.49609v-0.00195c-0.29285,-0.69513 -0.68904,-1.1959 -1.20703,-1.4375c-0.51799,-0.2416 -0.97563,-0.17291 -0.99414,-0.17383h-0.00195c-0.36964,-0.01705 -0.77527,-0.02148 -1.17773,-0.02148zM16.64258,15c0.38554,0 0.76564,0.0047 1.08398,0.01953c0.32749,0.01632 0.30712,0.01766 0.24414,-0.01172c-0.06399,-0.02984 0.02283,-0.03953 0.20898,0.40234c0.24341,0.57785 0.66348,1.58909 1.03906,2.49023c0.18779,0.45057 0.36354,0.87343 0.50391,1.20508c0.14036,0.33165 0.21642,0.51683 0.30469,0.69336v0.00195l0.00195,0.00195c0.08654,0.17075 0.07889,0.06143 0.04883,0.12109c-0.21103,0.41883 -0.23966,0.52166 -0.45312,0.76758c-0.32502,0.37443 -0.65655,0.792 -0.83203,0.96484c-0.15353,0.15082 -0.43055,0.38578 -0.60352,0.8457c-0.17323,0.46063 -0.09238,1.09262 0.18555,1.56445c0.37003,0.62819 1.58941,2.6129 3.48438,4.28125c1.19338,1.05202 2.30519,1.74828 3.19336,2.2168c0.88817,0.46852 1.61157,0.74215 1.77344,0.82227c0.38438,0.19023 0.80448,0.33795 1.29297,0.2793c0.48849,-0.05865 0.90964,-0.35504 1.17773,-0.6582l0.00195,-0.00195c0.3568,-0.40451 1.41702,-1.61513 1.92578,-2.36133c0.02156,0.0076 0.0145,0.0017 0.18359,0.0625v0.00195h0.00195c0.0772,0.02749 1.04413,0.46028 1.90625,0.87109c0.86212,0.41081 1.73716,0.8378 2.02148,0.97852c0.41033,0.20308 0.60422,0.33529 0.6543,0.33594c0.00338,0.08798 0.0068,0.18333 -0.00586,0.32813c-0.03507,0.40164 -0.14243,0.95757 -0.35742,1.55273c-0.10532,0.29136 -0.65389,0.89227 -1.3457,1.33008c-0.69181,0.43781 -1.53386,0.74705 -1.8457,0.79297c-0.9376,0.13815 -2.05083,0.18859 -3.27344,-0.19531c-0.84773,-0.26609 -1.90476,-0.61053 -3.27344,-1.19336c-2.77581,-1.18381 -5.13503,-3.19825 -6.82031,-4.97266c-0.84264,-0.8872 -1.51775,-1.71309 -1.99805,-2.33789c-0.4794,-0.62364 -0.68874,-0.94816 -0.86328,-1.17773l-0.00195,-0.00195c-0.30983,-0.40973 -2.20703,-3.04868 -2.20703,-5.42578c0,-2.51576 1.1685,-3.50231 1.80078,-4.18359c0.33194,-0.35766 0.69484,-0.41016 0.8418,-0.41016z"></path></g></g>
            </svg></div></div></a></div>
            <div class="wrap_wa_list">
                <ul>
            ';
            
            if( !empty( $wa_room ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_room .'" target="_blank">Villa Reservation</a></li>';
            }
            
            if( !empty( $wa_restaurant ) ) {
                 $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_restaurant .'" target="_blank">Restaurant Reservation</a></li>';
            }
            
            if( !empty( $wa_inhouse_guest ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_inhouse_guest .'" target="_blank"> In-house Guest Inquiry</a></li>';
            }
    
            $wa_html .= '</ul></div>';
            
             add_variable('wa_numb', $wa_html);
        }
    

        // LANGUAGE CHECK
        if($check_lang)
        {
            $breakfast_open_lang          = get_additional_field($post_id, 'breakfast_open_'.$lang, 'dining');
            $lunch_open_lang              = get_additional_field($post_id, 'lunch_open_'.$lang, 'dining');
            $dining_open_lang             = get_additional_field($post_id, 'dining_open_'.$lang, 'dining');
            $dresscode_lang               = get_additional_field( $post_id, 'dresscode_'.$lang, 'dining' );
            $our_menu_desc_lang           = get_additional_field( $post_id, 'our_menu_desc_'.$lang, 'dining' );
            $detail_reservation_text_lang = get_additional_field( $post_id, 'detail_reservation_text_'.$lang, 'dining' );
            $cta_link_lang = get_additional_field( $post_id, 'cta_link_'.$lang, 'dining' );
            
            $breakfast_open          = (empty($breakfast_open_lang) ? $breakfast_open: $breakfast_open_lang);
            $lunch_open              = (empty($lunch_open_lang) ? $lunch_open:         $lunch_open_lang);
            $dining_open             = (empty($dining_open_lang) ? $dining_open:       $dining_open_lang);
            $dresscode               = (empty($dresscode_lang) ? $dresscode:           $dresscode_lang);
            $our_menu_desc           = (empty($our_menu_desc_lang) ? $our_menu_desc:   $our_menu_desc_lang);
            $detail_reservation_text = (empty($detail_reservation_text_lang) ? $detail_reservation_text:   $detail_reservation_text_lang);
            $cta_link = (empty($cta_link_lang) ? $cta_link:   $cta_link_lang);
        }

        
        $breakfast_open     = (empty($breakfast_open) ? "": "<p>".$tr_string['breakfast']." $breakfast_open</p>");
        $lunch_open         = (empty($lunch_open) ? "":     "<p>".$tr_string['lunch']." $lunch_open</p>");
        $dining_open        = (empty($dining_open) ? "":    "<p>".$tr_string['dining']." $dining_open</p>");
        $restaurant_open    = $breakfast_open.$lunch_open.$dining_open;

        $menu_pdf_download = "";
        if(!empty($menu_pdf) && file_exists( PLUGINS_PATH . '/menus/pdf/' . $menu_pdf ))
        {
            $menu_pdf_download = '
                <div class="container-download-menu">
                    <a href="'.URL_PLUGINS . 'menus/pdf/' . $menu_pdf.'" target="_blank">
                        <p>'.$tr_string['download_our_menu'].'</p>
                    </a>
                </div>
            ';
        }
        elseif(!empty($menu_pdf_text_link))
        {
            $menu_pdf_download = '
                <div class="container-download-menu">
                    <a href="'.$menu_pdf_text_link.'" target="_blank">
                        <p>'.$tr_string['download_our_menu'].'</p>
                    </a>
                </div>
            ';
        }

        // RESTAURANT OPEN & DRESS CODE
        if(!empty($restaurant_open) || !empty($dresscode))
        {
            $dresscode_restaurant = '
            <div class="container-open-dresscode clearfix">';

            if(!empty($restaurant_open))
            {
                $dresscode_restaurant .= '
                <div class="dining-open">
                    <h4>'.$tr_string['restaurant_open'].'</h4>
                    '.$restaurant_open.'
                </div>
                ';
            }
            if(!empty($dresscode))
            {
                $dresscode_restaurant .= '
                <div class="dining-dresscode">
                    <h4>'.$tr_string['dresscode'].'</h4>
                    <p>'.$dresscode.'</p>
                </div>
                ';
            }
            $dresscode_restaurant .= '
            </div>
            ';
            add_variable( 'dresscode_restaurant', $dresscode_restaurant);
        }

        $ctabuttom = '';
        if (!empty($cta_link)) {
            $ctabuttom = '
                
                <a href="'.$cta_link.'" class="nbtn_" target="_blank" style="
                    display: inline-block;
                    padding: 13px 30px;
                    font-size: 20px;
                    color: #fff;
                    background-color: #d5a47f;
                    margin-top: 25px;
                    cursor: pointer;
                ">
                Booking & Inquiries
                </a>
            ';
        }

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'restaurant_open', $restaurant_open);
        add_variable( 'dresscode', $dresscode);
        add_variable( 'our_menu_desc', $our_menu_desc);
        add_variable( 'menu_pdf_download', $menu_pdf_download);
        add_variable( 'cta_button', $ctabuttom);

        $reservation_dining_detail_html = '
            <h3>'.$tr_string['reservation'].'</h3>
            <p>'.$tr_string['asking_reservations_restaurant_online'].'</p>
            <!-- <p>Would Like To Make A Restaurant <br/> Reservation Online?</p> -->
            <div class="container-or clearfix" data-aos="fade-up">
                <div class="element-or"></div>
                <p>'.$tr_string['or'].'</p>
            </div>
        ';
        if(!empty($detail_reservation_text))
        {
            $reservation_dining_detail_html = '
                <h3>'.$post_title.'</h3>
                <p>'.$detail_reservation_text.'</p>
            ';
        }

        add_variable('reservation_dining_detail_html', $reservation_dining_detail_html);

        if( $destination == 'seminyak' )
        {
            add_actions( 'include-js', 'get_custom_javascript', 'https://booking.resdiary.com/bundles/jquery.js' );
            add_variable('reservation_iframe', '<div id="rd-widget-frame" style="max-width: 600px; margin: auto;">' . $code_chope . '</div>');
        }
        else
        {
            //add_variable('reservation_iframe', '<iframe src="https://bookv5.chope.co/booking?rid=sweptaway1904bal&source=rest_thesamayabali.com" name="frame1" scrolling="auto" frameborder="no" align="center" height = "900px" width = "400px"></iframe>');
            add_variable('reservation_iframe', '<iframe src="https://bookv5.chope.co/booking?rid=sweptaway1904bal&select_location=1&source=rest_IGint.sweptawayrestaurant" name="frame1" scrolling="auto" frameborder="no" align="center" height = "900px" width = "400px"></iframe>');
        }

        // GET CATEGORY
        $qcmenu = $db->prepare_query("
            SELECT a.*
            FROM lumonata_menu_category AS a
            WHERE a.larticle_id = %d AND a.lcmenu_parent = %d
            ORDER BY lorder
        ", $post_id, 0);
        $rcmenu = $db->do_query($qcmenu);
        $ncmenu = $db->num_rows($rcmenu);

        if($ncmenu > 0)
        {  
            $icm = 1;
            $menu_type_all = "";
            $category_li_html = "";

            while($dcmenu = $db->fetch_array($rcmenu))
            {
                $category_id         = $dcmenu['lcmenu_id'];
                $category_name       = $dcmenu['lcmenu_name'];
                $category_sanitize   = generateSefUrl($dcmenu['lcmenu_name']);
                $category_menu_image = get_additional_menu_field($category_id, 'category_menu_image', 'menu_category');
                $pdf_link            = get_additional_menu_field($category_id, 'category_menu_pdf', 'menu_category');
                $pdf_link_result     = "";
                $target_blank_pdf    = "";
                $category_menu_image_html = "";

                $full_width = "full_width";

                if(!empty($category_menu_image))
                {
                    $category_menu_image      = json_decode($category_menu_image, true);
                    $category_menu_image      = HTTP.site_url().$category_menu_image['small'];
                    $category_menu_image_html = '<img src="'.$category_menu_image.'" />';
                    $full_width               = "";
                }
                add_variable('full_width', $full_width);
                
                add_variable('category_menu_image_html', $category_menu_image_html);

                add_variable('category_sanitize', $category_sanitize);
                add_variable('category_name', $category_name);
                add_variable('category_menu_image', $category_menu_image);

                // LANGUAGE CHECK
                if($check_lang)
                {
                    $category_name_lang = get_additional_menu_field($category_id, 'lcmenu_name_'.$lang, 'menu_category');
                    $pdf_link_lang      = get_additional_menu_field($category_id, 'category_menu_pdf_'.$lang, 'menu_category');

                    $category_name      = (empty($category_name_lang) ? $category_name: $category_name_lang);
                    $pdf_link = (empty($pdf_link_lang) || !file_exists(ROOT_PATH . $pdf_link_lang) ? $pdf_link: $pdf_link_lang);
                }

                $button_pdf_dining = "";
                if(!empty($pdf_link) && file_exists( ROOT_PATH . $pdf_link))
                {
                    $pdf_link_result = HTTP.SITE_URL.$pdf_link;
                    $target_blank_pdf = 'target="_blank"';
                    $button_pdf_dining = '<a href="'.$pdf_link_result.'" '.$target_blank_pdf.'><span>'.$tr_string['download'].' (PDF)</span></a>';
                }

                
                if($icm == 1)
                {
                    $class_active = "active";
                    $category_li_html .= '<li class="active '.$category_sanitize.'-filter" data-filter="'.$category_sanitize.'" data-title="'.$category_name.'"><span>'.$category_name.'</span></li>';
                }
                else
                {
                    $class_active = "";
                    $category_li_html .= '<li class="'.$category_sanitize.'-filter" data-filter="'.$category_sanitize.'" data-title="'.$category_name.'"><span>'.$category_name.'</span></li>';
                }

                add_variable('class_active', $class_active);
                add_variable('pdf_link_result', $pdf_link_result);
                add_variable('target_blank_pdf', $target_blank_pdf);
                add_variable('button_pdf_dining', $button_pdf_dining);
                

                // GET CHILD CATEGORY
                // $qccmenu = $db->prepare_query("
                //     SELECT *
                //     FROM lumonata_menu_category AS a
                //     WHERE lcmenu_parent = %d
                //     ORDER BY lorder
                // ", $category_id);
                // $rccmenu = $db->do_query($qccmenu);
                // $nccmenu = $db->num_rows($rccmenu);
                
                // $list_item_menu_html = "";

                // if($nccmenu > 0)
                // {
                //     while($dccmenu = $db->fetch_array($rccmenu))
                //     {
                //         $child_category_id       = $dccmenu['lcmenu_id'];
                //         $child_category_name     = $dccmenu['lcmenu_name'];

                //         // LANGUAGE CHECK
                //         if($check_lang)
                //         {
                //             $child_category_name_lang = get_additional_menu_field($child_category_id, 'lcmenu_name_'.$lang, 'menu_category');
                //             $child_category_name      = (empty($child_category_name_lang) ? $child_category_name: $child_category_name_lang);
                //         }

                //         $list_item_menu_html .= '<h3>'.($child_category_name).'</h3><div class="container-list-menu clearfix">';

                //         // START GET MENU LIST
                //         $qmenu = $db->prepare_query("
                //             SELECT *
                //             FROM lumonata_menu
                //             WHERE lcmenu_id = %d
                //             ORDER BY lorder
                //         ", $child_category_id);
                //         $rmenu = $db->do_query($qmenu);
                //         $nmenu = $db->num_rows($rmenu);

                //         if($nmenu > 0)
                //         {
                //             while($dmenu = $db->fetch_array($rmenu))
                //             {
                //                 $menu_id   = $dmenu['lmenu_id'];
                //                 $menu_name = $dmenu['lmenu_name'];
                //                 $menu_desc = $dmenu['lmenu_desc'];

                //                 // LANGUAGE CHECK
                //                 if($check_lang)
                //                 {
                //                     $menu_name_lang = get_additional_menu_field($menu_id, 'lmenu_name_'.$lang, 'menu_list');
                //                     $menu_desc_lang = get_additional_menu_field($menu_id, 'lmenu_desc_'.$lang, 'menu_list');
                                    
                //                     $menu_name      = (empty($menu_name_lang) ? $menu_name: $menu_name_lang);
                //                     $menu_desc      = (empty($menu_desc_lang) ? $menu_desc: $menu_desc_lang);
                //                 }

                //                 $list_item_menu_html .= '
                //                     <div class="list-item-menu" data-aos="fade-up">
                //                         <h4>'.$menu_name.'</h4>
                //                         <p>'.$menu_desc.'</p>
                //                     </div>
                //                 ';
                //             }
                //         }
                //         $list_item_menu_html .= '</div>';
                //     }

                    
                // }
                
                // add_variable('list_item_menu_html', $list_item_menu_html);

                // parse_template('loop-cat-menu-popup-block', 'lcmpblock', true);
                parse_template('loop-cat-menu-block', 'lcmblock', true);

                add_variable('category_li_html', $category_li_html);

                $icm++;
            }

            // parse_template('menu-dining-popup-block', 'mdpblock', false);     
            parse_template('menu-dining-block', 'mdblock', false);
        }
        else
        {
            add_variable( 'hide', 'hide');
        }



        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('dining', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'dining', false );
                $image_product   = isset($image_product['medium']) ? HTTP . site_url().$image_product['medium'] : '';
                $post_link       = HTTP . site_url().'/'.$destination.'/dining/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'dining');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'dining');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'dining');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/dining/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/dining';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/dining';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/dining/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/dining/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);
        set_canonical_link($post_data);

        // print_r($post_data);
        // die();

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN NILAI MENU TYPE DAN MENGHAPUS VALUE YANG SAMA
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_menu_type_dining($menu_type_all)
{
    $last_menu_type   = array();
    $menu_type_before = "";

    foreach($menu_type_all as $d)
    {
        $menu_type = $d;
        if($menu_type != $menu_type_before)
        {
            $last_menu_type[] = $menu_type;
        }
        $menu_type_before = $menu_type;
    }

    return $last_menu_type;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Dining Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function spa_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;
    $version = attemp_actions('version_js_css');

    set_template( TEMPLATE_PATH . '/template/spa_detail.html', 'page_template' );
    add_block( 'loop-cat-treatment-block', 'lctblock', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
    add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );

    // STRING TRANSLATIONS
    $translations = array('Price', 'Book Now', 'Explore', 'Reserve Spa');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $option_spa = "";

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        $val_array = array(
            'key' => $post_id,
            'title' => $post_title
        );
        $values_json = base64_encode( json_encode( $val_array ) );
        add_actions( 'value_json_booking', $values_json );

        // explode title
        $explode_title = explode(" at ", $post_title);
        
        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);

        $option_spa .= '<option value="'.$post_id.'">'.ucwords(strtolower($post_title)).'</option>';

        // GET SPA CATEGORY LIST
        $qcat = $db->prepare_query("
            SELECT * 
            FROM lumonata_treatment_category
            WHERE larticle_id = %d
        ", $post_id);
        $rcat = $db->do_query($qcat);
        $ncat = $db->num_rows($rcat);

        if($ncat > 0)
        {
            while($d = $db->fetch_array($rcat))
            {
                $cat_id   = $d['lctreatment_id'];
                $cat_name = $d['lctreatment_name'];
                $cat_desc = $d['lctreatment_desc'];

                // LANGUAGE CHECK
                if($check_lang)
                {
                    $cat_name_lang = get_additional_treatment_field($cat_id, 'lctreatment_name_'.$lang, 'treatment_category');
                    $cat_desc_lang = get_additional_treatment_field($cat_id, 'lctreatment_desc_'.$lang, 'treatment_category');
                    
                    $cat_name      = (empty($cat_name_lang) ? $cat_name: $cat_name_lang);
                    $cat_desc      = (empty($cat_desc_lang) ? $cat_desc: $cat_desc_lang);
                }

                // echo $cat_desc;

                add_variable( 'cat_name', $cat_name);

                if(!empty($cat_desc))
                {
                    $cat_desc = '
                        <div class="desc-cat">
                            '.$cat_desc.'
                        </div>
                    ';
                }
                add_variable( 'cat_desc', $cat_desc);

                // GET LIST TREATMENT
                $qtreatment = $db->prepare_query("
                    SELECT *
                    FROM lumonata_treatment
                    WHERE lctreatment_id = %d
                ", $cat_id);
                $rtreatment = $db->do_query($qtreatment);
                $ntreatment = $db->num_rows($rtreatment);

                $content_treatment_list = "";

                if($ntreatment > 0)
                {
                    while($dtreatment = $db->fetch_array($rtreatment))
                    {
                        $treatment_id    = $dtreatment['ltreatment_id'];
                        $treatment_name  = $dtreatment['ltreatment_name'];
                        $treatment_price = $dtreatment['ltreatment_price'];

                        $val_array_treatment = array(
                            'key'      => $post_id,
                            'cat_id'   => $cat_id,
                            'treat_id' => $treatment_id
                        );
                        $values_json_treatment = base64_encode( json_encode( $val_array_treatment ) );

                        // LANGUAGE CHECK
                        if($check_lang)
                        {
                            $treatment_name_lang  = get_additional_treatment_field($treatment_id, 'ltreatment_name_'.$lang, 'treatment_list');
                            
                            $treatment_name  = (empty($treatment_name_lang) ? $treatment_name:        $treatment_name_lang);
                        }

                        $content_treatment_list .= '
                            <div class="list-treatment clearfix">
                                <div class="left">
                                    <p>'.$treatment_name.'</p>
                                    <p class="price-spa">'.$tr_string['price'].': '.$treatment_price.' k</p>
                                </div>
                                <div class="right clearfix">
                                    <p class="price-spa">'.$tr_string['price'].': '.$treatment_price.' k</p>
                                    <button class="book-now-treatment" data-key="'.$values_json_treatment.'">'.$tr_string['book_now'].'</button>
                                </div>
                            </div>
                        ';
                    }

                    add_variable( 'content_treatment_list', $content_treatment_list);
                }

                parse_template('loop-cat-treatment-block', 'lctblock', true);
            }
        }

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('spa', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'spa', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/spa/'.$sef_product.'.html';

                $title_option_book = ucwords(strtolower($title_product));

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'spa');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'spa');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'spa');
                    
                    $title_product     = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $title_option_book = (empty($title_product_lang) ? $title_option_book: $title_product_lang);
                    $brief_product     = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product   = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/spa/'.$sef_product.'.html';
                }

                $link_detail = '<p class="text _text list-item-text-explore">'.$tr_string['explore'].'</p>';
                if(empty($content_product))
                {
                    $post_link = "#";
                    $link_detail = "";
                }

                $option_spa .= '<option value="'.$id_product.'">'.$title_option_book.'</option>';

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);
                add_variable('link_detail', $link_detail);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $wa                = get_additional_field($corporatedata[0]['id'], 'villa_reservation', 'destinations');;
        $wa_restaurant     = get_additional_field($corporatedata[0]['id'], 'restaurant_reservation', 'destinations');
        $wa_inhouse_guest  = get_additional_field($corporatedata[0]['id'], 'guest_reservation', 'destinations');
        
        $wa_room = isset( $wa ) && !empty( $wa ) ? $wa : '';
        $wa_restaurant = isset( $wa_restaurant ) && !empty( $wa_restaurant ) ? $wa_restaurant : '';
        $wa_inhouse_guest = isset( $wa_inhouse_guest ) && !empty( $wa_inhouse_guest ) ? $wa_inhouse_guest : '';
        
        
        if( !empty( $wa_room ) || !empty( $wa_restaurant ) ||  !empty( $wa_inhouse_guest ) ) {
        $wa_html = '<div class="floating_wa">
            <a>
                <div class="bg-[#A6631B] rounded-[50%] p-4 ml-4 mr-4 overflow-hidden flex justify-center align-center"><div class="font-bold text-white flex justify-center align-center"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="40" height="60" viewBox="0,0,256,256" style="fill:#000000;">
            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.12,5.12)"><path d="M25,2c-12.69047,0 -23,10.30953 -23,23c0,4.0791 1.11869,7.88588 2.98438,11.20898l-2.94727,10.52148c-0.09582,0.34262 -0.00241,0.71035 0.24531,0.96571c0.24772,0.25536 0.61244,0.35989 0.95781,0.27452l10.9707,-2.71875c3.22369,1.72098 6.88165,2.74805 10.78906,2.74805c12.69047,0 23,-10.30953 23,-23c0,-12.69047 -10.30953,-23 -23,-23zM25,4c11.60953,0 21,9.39047 21,21c0,11.60953 -9.39047,21 -21,21c-3.72198,0 -7.20788,-0.97037 -10.23828,-2.66602c-0.22164,-0.12385 -0.48208,-0.15876 -0.72852,-0.09766l-9.60742,2.38086l2.57617,-9.19141c0.07449,-0.26248 0.03851,-0.54399 -0.09961,-0.7793c-1.84166,-3.12289 -2.90234,-6.75638 -2.90234,-10.64648c0,-11.60953 9.39047,-21 21,-21zM16.64258,13c-0.64104,0 -1.55653,0.23849 -2.30859,1.04883c-0.45172,0.48672 -2.33398,2.32068 -2.33398,5.54492c0,3.36152 2.33139,6.2621 2.61328,6.63477h0.00195v0.00195c-0.02674,-0.03514 0.3578,0.52172 0.87109,1.18945c0.5133,0.66773 1.23108,1.54472 2.13281,2.49414c1.80347,1.89885 4.33914,4.09336 7.48633,5.43555c1.44932,0.61717 2.59271,0.98981 3.45898,1.26172c1.60539,0.5041 3.06762,0.42747 4.16602,0.26563c0.82216,-0.12108 1.72641,-0.51584 2.62109,-1.08203c0.89469,-0.56619 1.77153,-1.2702 2.1582,-2.33984c0.27701,-0.76683 0.41783,-1.47548 0.46875,-2.05859c0.02546,-0.29156 0.02869,-0.54888 0.00977,-0.78711c-0.01897,-0.23823 0.0013,-0.42071 -0.2207,-0.78516c-0.46557,-0.76441 -0.99283,-0.78437 -1.54297,-1.05664c-0.30567,-0.15128 -1.17595,-0.57625 -2.04883,-0.99219c-0.8719,-0.41547 -1.62686,-0.78344 -2.0918,-0.94922c-0.29375,-0.10568 -0.65243,-0.25782 -1.16992,-0.19922c-0.51749,0.0586 -1.0286,0.43198 -1.32617,0.87305c-0.28205,0.41807 -1.4175,1.75835 -1.76367,2.15234c-0.0046,-0.0028 0.02544,0.01104 -0.11133,-0.05664c-0.42813,-0.21189 -0.95173,-0.39205 -1.72656,-0.80078c-0.77483,-0.40873 -1.74407,-1.01229 -2.80469,-1.94727v-0.00195c-1.57861,-1.38975 -2.68437,-3.1346 -3.0332,-3.7207c0.0235,-0.02796 -0.00279,0.0059 0.04687,-0.04297l0.00195,-0.00195c0.35652,-0.35115 0.67247,-0.77056 0.93945,-1.07812c0.37854,-0.43609 0.54559,-0.82052 0.72656,-1.17969c0.36067,-0.71583 0.15985,-1.50352 -0.04883,-1.91797v-0.00195c0.01441,0.02867 -0.11288,-0.25219 -0.25,-0.57617c-0.13751,-0.32491 -0.31279,-0.74613 -0.5,-1.19531c-0.37442,-0.89836 -0.79243,-1.90595 -1.04102,-2.49609v-0.00195c-0.29285,-0.69513 -0.68904,-1.1959 -1.20703,-1.4375c-0.51799,-0.2416 -0.97563,-0.17291 -0.99414,-0.17383h-0.00195c-0.36964,-0.01705 -0.77527,-0.02148 -1.17773,-0.02148zM16.64258,15c0.38554,0 0.76564,0.0047 1.08398,0.01953c0.32749,0.01632 0.30712,0.01766 0.24414,-0.01172c-0.06399,-0.02984 0.02283,-0.03953 0.20898,0.40234c0.24341,0.57785 0.66348,1.58909 1.03906,2.49023c0.18779,0.45057 0.36354,0.87343 0.50391,1.20508c0.14036,0.33165 0.21642,0.51683 0.30469,0.69336v0.00195l0.00195,0.00195c0.08654,0.17075 0.07889,0.06143 0.04883,0.12109c-0.21103,0.41883 -0.23966,0.52166 -0.45312,0.76758c-0.32502,0.37443 -0.65655,0.792 -0.83203,0.96484c-0.15353,0.15082 -0.43055,0.38578 -0.60352,0.8457c-0.17323,0.46063 -0.09238,1.09262 0.18555,1.56445c0.37003,0.62819 1.58941,2.6129 3.48438,4.28125c1.19338,1.05202 2.30519,1.74828 3.19336,2.2168c0.88817,0.46852 1.61157,0.74215 1.77344,0.82227c0.38438,0.19023 0.80448,0.33795 1.29297,0.2793c0.48849,-0.05865 0.90964,-0.35504 1.17773,-0.6582l0.00195,-0.00195c0.3568,-0.40451 1.41702,-1.61513 1.92578,-2.36133c0.02156,0.0076 0.0145,0.0017 0.18359,0.0625v0.00195h0.00195c0.0772,0.02749 1.04413,0.46028 1.90625,0.87109c0.86212,0.41081 1.73716,0.8378 2.02148,0.97852c0.41033,0.20308 0.60422,0.33529 0.6543,0.33594c0.00338,0.08798 0.0068,0.18333 -0.00586,0.32813c-0.03507,0.40164 -0.14243,0.95757 -0.35742,1.55273c-0.10532,0.29136 -0.65389,0.89227 -1.3457,1.33008c-0.69181,0.43781 -1.53386,0.74705 -1.8457,0.79297c-0.9376,0.13815 -2.05083,0.18859 -3.27344,-0.19531c-0.84773,-0.26609 -1.90476,-0.61053 -3.27344,-1.19336c-2.77581,-1.18381 -5.13503,-3.19825 -6.82031,-4.97266c-0.84264,-0.8872 -1.51775,-1.71309 -1.99805,-2.33789c-0.4794,-0.62364 -0.68874,-0.94816 -0.86328,-1.17773l-0.00195,-0.00195c-0.30983,-0.40973 -2.20703,-3.04868 -2.20703,-5.42578c0,-2.51576 1.1685,-3.50231 1.80078,-4.18359c0.33194,-0.35766 0.69484,-0.41016 0.8418,-0.41016z"></path></g></g>
            </svg></div></div></a></div>
            <div class="wrap_wa_list">
                <ul>
            ';
            
            if( !empty( $wa_room ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_room .'" target="_blank">Villa Reservation</a></li>';
            }
            
            if( !empty( $wa_restaurant ) ) {
                 $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_restaurant .'" target="_blank">Restaurant Reservation</a></li>';
            }
            
            if( !empty( $wa_inhouse_guest ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_inhouse_guest .'" target="_blank"> In-house Guest Inquiry</a></li>';
            }
    
            $wa_html .= '</ul></div>';
            
             add_variable('wa_numb', $wa_html);
        }
        
        $link_explore  = HTTP.site_url().'/'.$destination.'/spa';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/spa';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/spa/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/spa/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);
        set_canonical_link($post_data);

        $destination_id = $corporatedata[0]['id'];
        add_actions( 'booking_popup_spa', 'booking_popup_spa_content', $destination, $destination_id );
        add_actions( 'option_spa', $option_spa );
        add_actions( 'lang', $lang );
        add_actions( 'check_lang', $check_lang );

        $button_book_hero = '
            <button class="container container-button-book-now book-now-hero clearfix" data-key="'.$values_json.'">
                <p class="text _text text-book-now">'.$tr_string['reserve_spa'].'</p>
            </button>
        ';

        add_actions('button_book_hero', $button_book_hero);

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}



/*
| -------------------------------------------------------------------------------------------------------------------------
| Special Offers Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function offers_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    $version = attemp_actions('version_js_css');
    global $db;

    add_actions( 'booking_popup_offers', 'booking_popup_offers_content' );

    set_template( TEMPLATE_PATH . '/template/offers_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // STRING TRANSLATIONS
    $translations = array('Additional Value Adds', 'Terms Conditions', 'Book Offer Now', 'Book Now', 'Price', 'Valid Until');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        $booking_link    = get_additional_field($post_id, 'booking_link', 'special-offers');
        $label_link      = get_additional_field($post_id, 'label_link', 'special-offers');
        $label_link_book = ((empty($label_link)) ? $tr_string['book_offer_now']: $label_link);
        $label_link_hero = ((empty($label_link)) ? $tr_string['book_now']:       $label_link);
        $form_type       = get_additional_field($post_id, 'form_type', 'special-offers');
        $form_type       = (empty($form_type) ? 2 : $form_type );

        if(empty($booking_link))
        {
            $button_book = '<button class="book-now" data-aos="fade-up" data-title="'.$post_title.'" data-form_type="'.$form_type.'">'.$label_link_book.'</button>';
            $button_book_hero = '
                <button class="container container-button-book-now book-now-hero clearfix" data-title="'.$post_title.'" data-form_type="'.$form_type.'">
                    <p class="text _text text-book-now">'.$label_link_hero.'</p>
                </button>
            ';
        }
        else
        {
            $button_book = '<a href="'.$booking_link.'" class="book-now-link" data-aos="fade-up">'.$label_link_book.'</a>';
            $button_book_hero = '
                <a href="'.$booking_link.'" class="container container-button-book-now-link book-now-hero clearfix">
                    <p class="text _text text-book-now">'.$label_link_hero.'</p>
                </a>
            ';
        }
        add_variable( 'button_book', $button_book);
        add_actions( 'button_book_hero', $button_book_hero);


        // explode title
        $explode_title = explode(" at ", $post_title);
        
        // ADDITIONAL FIELD
        $price      = get_additional_field( $post_id, 'price', 'special-offers' );
        $valid_date = get_additional_field( $post_id, 'valid_date', 'special-offers' );

        if(!empty($price))
        {
            $html_price = '
                <div class="container-price" data-aos="fade-up">
                    <h4>'.$tr_string['price'].'</h4>
                    <p>'.$price.'</p>
                </div>
            ';
            add_variable( 'html_price', $html_price);
        }

        if(!empty($valid_date))
        {
            $valid_date = date("d M Y", strtotime($valid_date));
            $html_date = '
                <div class="container-valid-date" data-aos="fade-up">
                    <h4>'.$tr_string['valid_until'].'</h4>
                    <p>'.$valid_date.'</p>
                </div>
            ';
            add_variable( 'html_date', $html_date);
        }

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);


        // GET ADDITIONAL VALUE CONTENT (RULE)
        $additional_adds_content = "";
        $additional_adds = get_rule_list_post($post_id, 'special-offers','additional_value', 1, $lang, $check_lang);
        if(!empty($additional_adds))
        {
            $additional_adds_content = '
                <div class="additional-list clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['additional_value_adds'].':</h3>
                    '.$additional_adds.'
                </div>
            ';
        }
        add_variable( 'additional_adds_content', $additional_adds_content);

        // GET TERMS AND CONDITIONS (RULE)
        $terms_conditions_content = "";
        $terms_conditions = get_rule_list_post($post_id, 'special-offers','terms_conditions', 1, $lang, $check_lang);
        if(!empty($terms_conditions))
        {
            $terms_conditions_content = '
                <div class="terms-conditions clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['terms_conditions'].':</h3>
                    '.$terms_conditions.'
                </div>
            ';
        }
        add_variable( 'terms_conditions_content', $terms_conditions_content);

        $data_page = get_meta_data('post_type_setting', 'accommodation', $corporatedata[0]['id']);
        
        $post_type_setting = json_decode($data_page, true);
       
       
         $wa                = get_additional_field($corporatedata[0]['id'], 'villa_reservation', 'destinations');;
        $wa_restaurant     = get_additional_field($corporatedata[0]['id'], 'restaurant_reservation', 'destinations');
        $wa_inhouse_guest  = get_additional_field($corporatedata[0]['id'], 'guest_reservation', 'destinations');
        
        $wa_room = isset( $wa ) && !empty( $wa ) ? $wa : '';
        $wa_restaurant = isset( $wa_restaurant ) && !empty( $wa_restaurant ) ? $wa_restaurant : '';
        $wa_inhouse_guest = isset( $wa_inhouse_guest ) && !empty( $wa_inhouse_guest ) ? $wa_inhouse_guest : '';
        
        
        if( !empty( $wa_room ) || !empty( $wa_restaurant ) ||  !empty( $wa_inhouse_guest ) ) {
        $wa_html = '<div class="floating_wa">
            <a>
                <div class="bg-[#A6631B] rounded-[50%] p-4 ml-4 mr-4 overflow-hidden flex justify-center align-center"><div class="font-bold text-white flex justify-center align-center"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="40" height="60" viewBox="0,0,256,256" style="fill:#000000;">
            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.12,5.12)"><path d="M25,2c-12.69047,0 -23,10.30953 -23,23c0,4.0791 1.11869,7.88588 2.98438,11.20898l-2.94727,10.52148c-0.09582,0.34262 -0.00241,0.71035 0.24531,0.96571c0.24772,0.25536 0.61244,0.35989 0.95781,0.27452l10.9707,-2.71875c3.22369,1.72098 6.88165,2.74805 10.78906,2.74805c12.69047,0 23,-10.30953 23,-23c0,-12.69047 -10.30953,-23 -23,-23zM25,4c11.60953,0 21,9.39047 21,21c0,11.60953 -9.39047,21 -21,21c-3.72198,0 -7.20788,-0.97037 -10.23828,-2.66602c-0.22164,-0.12385 -0.48208,-0.15876 -0.72852,-0.09766l-9.60742,2.38086l2.57617,-9.19141c0.07449,-0.26248 0.03851,-0.54399 -0.09961,-0.7793c-1.84166,-3.12289 -2.90234,-6.75638 -2.90234,-10.64648c0,-11.60953 9.39047,-21 21,-21zM16.64258,13c-0.64104,0 -1.55653,0.23849 -2.30859,1.04883c-0.45172,0.48672 -2.33398,2.32068 -2.33398,5.54492c0,3.36152 2.33139,6.2621 2.61328,6.63477h0.00195v0.00195c-0.02674,-0.03514 0.3578,0.52172 0.87109,1.18945c0.5133,0.66773 1.23108,1.54472 2.13281,2.49414c1.80347,1.89885 4.33914,4.09336 7.48633,5.43555c1.44932,0.61717 2.59271,0.98981 3.45898,1.26172c1.60539,0.5041 3.06762,0.42747 4.16602,0.26563c0.82216,-0.12108 1.72641,-0.51584 2.62109,-1.08203c0.89469,-0.56619 1.77153,-1.2702 2.1582,-2.33984c0.27701,-0.76683 0.41783,-1.47548 0.46875,-2.05859c0.02546,-0.29156 0.02869,-0.54888 0.00977,-0.78711c-0.01897,-0.23823 0.0013,-0.42071 -0.2207,-0.78516c-0.46557,-0.76441 -0.99283,-0.78437 -1.54297,-1.05664c-0.30567,-0.15128 -1.17595,-0.57625 -2.04883,-0.99219c-0.8719,-0.41547 -1.62686,-0.78344 -2.0918,-0.94922c-0.29375,-0.10568 -0.65243,-0.25782 -1.16992,-0.19922c-0.51749,0.0586 -1.0286,0.43198 -1.32617,0.87305c-0.28205,0.41807 -1.4175,1.75835 -1.76367,2.15234c-0.0046,-0.0028 0.02544,0.01104 -0.11133,-0.05664c-0.42813,-0.21189 -0.95173,-0.39205 -1.72656,-0.80078c-0.77483,-0.40873 -1.74407,-1.01229 -2.80469,-1.94727v-0.00195c-1.57861,-1.38975 -2.68437,-3.1346 -3.0332,-3.7207c0.0235,-0.02796 -0.00279,0.0059 0.04687,-0.04297l0.00195,-0.00195c0.35652,-0.35115 0.67247,-0.77056 0.93945,-1.07812c0.37854,-0.43609 0.54559,-0.82052 0.72656,-1.17969c0.36067,-0.71583 0.15985,-1.50352 -0.04883,-1.91797v-0.00195c0.01441,0.02867 -0.11288,-0.25219 -0.25,-0.57617c-0.13751,-0.32491 -0.31279,-0.74613 -0.5,-1.19531c-0.37442,-0.89836 -0.79243,-1.90595 -1.04102,-2.49609v-0.00195c-0.29285,-0.69513 -0.68904,-1.1959 -1.20703,-1.4375c-0.51799,-0.2416 -0.97563,-0.17291 -0.99414,-0.17383h-0.00195c-0.36964,-0.01705 -0.77527,-0.02148 -1.17773,-0.02148zM16.64258,15c0.38554,0 0.76564,0.0047 1.08398,0.01953c0.32749,0.01632 0.30712,0.01766 0.24414,-0.01172c-0.06399,-0.02984 0.02283,-0.03953 0.20898,0.40234c0.24341,0.57785 0.66348,1.58909 1.03906,2.49023c0.18779,0.45057 0.36354,0.87343 0.50391,1.20508c0.14036,0.33165 0.21642,0.51683 0.30469,0.69336v0.00195l0.00195,0.00195c0.08654,0.17075 0.07889,0.06143 0.04883,0.12109c-0.21103,0.41883 -0.23966,0.52166 -0.45312,0.76758c-0.32502,0.37443 -0.65655,0.792 -0.83203,0.96484c-0.15353,0.15082 -0.43055,0.38578 -0.60352,0.8457c-0.17323,0.46063 -0.09238,1.09262 0.18555,1.56445c0.37003,0.62819 1.58941,2.6129 3.48438,4.28125c1.19338,1.05202 2.30519,1.74828 3.19336,2.2168c0.88817,0.46852 1.61157,0.74215 1.77344,0.82227c0.38438,0.19023 0.80448,0.33795 1.29297,0.2793c0.48849,-0.05865 0.90964,-0.35504 1.17773,-0.6582l0.00195,-0.00195c0.3568,-0.40451 1.41702,-1.61513 1.92578,-2.36133c0.02156,0.0076 0.0145,0.0017 0.18359,0.0625v0.00195h0.00195c0.0772,0.02749 1.04413,0.46028 1.90625,0.87109c0.86212,0.41081 1.73716,0.8378 2.02148,0.97852c0.41033,0.20308 0.60422,0.33529 0.6543,0.33594c0.00338,0.08798 0.0068,0.18333 -0.00586,0.32813c-0.03507,0.40164 -0.14243,0.95757 -0.35742,1.55273c-0.10532,0.29136 -0.65389,0.89227 -1.3457,1.33008c-0.69181,0.43781 -1.53386,0.74705 -1.8457,0.79297c-0.9376,0.13815 -2.05083,0.18859 -3.27344,-0.19531c-0.84773,-0.26609 -1.90476,-0.61053 -3.27344,-1.19336c-2.77581,-1.18381 -5.13503,-3.19825 -6.82031,-4.97266c-0.84264,-0.8872 -1.51775,-1.71309 -1.99805,-2.33789c-0.4794,-0.62364 -0.68874,-0.94816 -0.86328,-1.17773l-0.00195,-0.00195c-0.30983,-0.40973 -2.20703,-3.04868 -2.20703,-5.42578c0,-2.51576 1.1685,-3.50231 1.80078,-4.18359c0.33194,-0.35766 0.69484,-0.41016 0.8418,-0.41016z"></path></g></g>
            </svg></div></div></a></div>
            <div class="wrap_wa_list">
                <ul>
            ';
            
            if( !empty( $wa_room ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_room .'" target="_blank">Villa Reservation</a></li>';
            }
            
            if( !empty( $wa_restaurant ) ) {
                 $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_restaurant .'" target="_blank">Restaurant Reservation</a></li>';
            }
            
            if( !empty( $wa_inhouse_guest ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_inhouse_guest .'" target="_blank"> In-house Guest Inquiry</a></li>';
            }
    
            $wa_html .= '</ul></div>';
            
             add_variable('wa_numb', $wa_html);
        }
        
        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('special-offers', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $id_product, 'special-offers', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/special-offers/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'special-offers');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'special-offers');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'special-offers');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/special-offers/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/special-offers';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/special-offers';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/special-offers/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/special-offers/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);
        set_canonical_link($post_data);
        
        add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
        add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );

        parse_template( 'page-block', 'pblock', false );
    }

    return return_template( 'page_template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| E-Gift Card Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function gift_card_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    $version = attemp_actions('version_js_css');
    global $db;

    add_actions( 'booking_popup_offers', 'booking_popup_offers_content' );

    set_template( TEMPLATE_PATH . '/template/gift_card_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // STRING TRANSLATIONS
    $translations = array('Additional Value Adds', 'Terms Conditions', 'Book Offer Now', 'Book Now', 'Price', 'Valid Until');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        $booking_link    = get_additional_field($post_id, 'booking_link', 'e-gift-card');
        $label_link      = get_additional_field($post_id, 'label_link', 'e-gift-card');
        $label_link_book = ((empty($label_link)) ? $tr_string['book_offer_now']: $label_link);
        $label_link_hero = ((empty($label_link)) ? $tr_string['book_now']:       $label_link);
        $form_type       = get_additional_field($post_id, 'form_type', 'e-gift-card');
        $form_type       = (empty($form_type) ? 2 : $form_type );

        if(empty($booking_link))
        {
            $button_book = '<button class="book-now" data-aos="fade-up" data-title="'.$post_title.'" data-form_type="'.$form_type.'">'.$label_link_book.'</button>';
            $button_book_hero = '
                <button class="container container-button-book-now book-now-hero clearfix" data-title="'.$post_title.'" data-form_type="'.$form_type.'">
                    <p class="text _text text-book-now">'.$label_link_hero.'</p>
                </button>
            ';
        }
        else
        {
            $button_book = '<a href="'.$booking_link.'" class="book-now-link" data-aos="fade-up">'.$label_link_book.'</a>';
            $button_book_hero = '
                <a href="'.$booking_link.'" class="container container-button-book-now-link book-now-hero clearfix">
                    <p class="text _text text-book-now">'.$label_link_hero.'</p>
                </a>
            ';
        }
        add_variable( 'button_book', $button_book);
        add_actions( 'button_book_hero', $button_book_hero);


        // explode title
        $explode_title = explode(" at ", $post_title);
        
        // ADDITIONAL FIELD
        $price      = get_additional_field( $post_id, 'price', 'e-gift-card' );
        $valid_date = get_additional_field( $post_id, 'valid_date', 'e-gift-card' );

        if(!empty($price))
        {
            $html_price = '
                <div class="container-price" data-aos="fade-up">
                    <h4>'.$tr_string['price'].'</h4>
                    <p>'.$price.'</p>
                </div>
            ';
            add_variable( 'html_price', $html_price);
        }

        if(!empty($valid_date))
        {
            $valid_date = date("d M Y", strtotime($valid_date));
            $html_date = '
                <div class="container-valid-date" data-aos="fade-up">
                    <h4>'.$tr_string['valid_until'].'</h4>
                    <p>'.$valid_date.'</p>
                </div>
            ';
            add_variable( 'html_date', $html_date);
        }

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);


        // GET ADDITIONAL VALUE CONTENT (RULE)
        $additional_adds_content = "";
        $additional_adds = get_rule_list_post($post_id, 'e-gift-card','additional_value', 1, $lang, $check_lang);
        if(!empty($additional_adds))
        {
            $additional_adds_content = '
                <div class="additional-list clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['additional_value_adds'].':</h3>
                    '.$additional_adds.'
                </div>
            ';
        }
        add_variable( 'additional_adds_content', $additional_adds_content);

        // GET TERMS AND CONDITIONS (RULE)
        $terms_conditions_content = "";
        $terms_conditions = get_rule_list_post($post_id, 'e-gift-card','terms_conditions', 1, $lang, $check_lang);
        if(!empty($terms_conditions))
        {
            $terms_conditions_content = '
                <div class="terms-conditions clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['terms_conditions'].':</h3>
                    '.$terms_conditions.'
                </div>
            ';
        }
        add_variable( 'terms_conditions_content', $terms_conditions_content);

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('e-gift-card', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $id_product, 'e-gift-card', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/e-gift-card/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'e-gift-card');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'e-gift-card');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'e-gift-card');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/e-gift-card/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/e-gift-card';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/e-gift-card';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/e-gift-card/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/special-offers/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);
        set_canonical_link($post_data);
        
        add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
        add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );

        parse_template( 'page-block', 'pblock', false );
    }

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Events Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function events_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/events_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // STRING TRANSLATIONS
    $translations = array('Inclusion', 'Additional Service', 'Terms Conditions', 'Book Now');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $destination_id = $corporatedata[0]['id'];
    $bg_wedding_events_reservation      = get_additional_field( $destination_id, 'bg_image_events', 'destinations' );
    if(!empty($bg_wedding_events_reservation))
    {
        $bg_wedding_events_reservation = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$bg_wedding_events_reservation;
    }
    else
    {
        $bg_wedding_events_reservation = HTTP.TEMPLATE_URL.'/assets/images/wedding.jpg';
    }
    add_variable( 'bg_wedding_events_reservation', $bg_wedding_events_reservation);

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // explode title
        $explode_title = explode(" at ", $post_title);
        
        // ADDITIONAL FIELD
        $price      = get_additional_field( $post_id, 'price', 'events' );

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'price', $price);


        // GET INCLUSION (RULE)
        $inclusion_content = "";
        $inclusion = get_rule_list_post($post_id, 'events','inclusion', 1, $lang, $check_lang);
        if(!empty($inclusion))
        {
            $inclusion_content = '
                <div class="additional-list clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['inclusion'].':</h3>
                    '.$inclusion.'
                </div>
            ';
        }
        add_variable( 'inclusion_content', $inclusion_content);

        // GET ADDITIONAL SERVICE (RULE)
        $additional_content = "";
        $additional = get_rule_list_post($post_id, 'events','additional_service', 1, $lang, $check_lang);
        if(!empty($inclusion))
        {
            $additional_content = '
                <div class="additional-list clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['additional_service'].':</h3>
                    '.$additional.'
                </div>
            ';
        }
        add_variable( 'additional_content', $additional_content);

        // GET TERMS AND CONDITIONS (RULE)
        $terms_conditions_content = "";
        $terms_conditions = get_rule_list_post($post_id, 'events','terms_conditions', 1, $lang, $check_lang);
        if(!empty($terms_conditions))
        {
            $terms_conditions_content = '
                <div class="terms-conditions clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['terms_conditions'].':</h3>
                    '.$terms_conditions.'
                </div>
            ';
        }
        add_variable( 'terms_conditions_content', $terms_conditions_content);
        
        $data_page = get_meta_data('post_type_setting', 'accommodation', $corporatedata[0]['id']);
        
        $post_type_setting = json_decode($data_page, true);
       
        $wa                = get_additional_field($corporatedata[0]['id'], 'villa_reservation', 'destinations');;
        $wa_restaurant     = get_additional_field($corporatedata[0]['id'], 'restaurant_reservation', 'destinations');
        $wa_inhouse_guest  = get_additional_field($corporatedata[0]['id'], 'guest_reservation', 'destinations');
        
        $wa_room = isset( $wa ) && !empty( $wa ) ? $wa : '';
        $wa_restaurant = isset( $wa_restaurant ) && !empty( $wa_restaurant ) ? $wa_restaurant : '';
        $wa_inhouse_guest = isset( $wa_inhouse_guest ) && !empty( $wa_inhouse_guest ) ? $wa_inhouse_guest : '';
        
        
        if( !empty( $wa_room ) || !empty( $wa_restaurant ) ||  !empty( $wa_inhouse_guest ) ) {
        $wa_html = '<div class="floating_wa">
            <a>
                <div class="bg-[#A6631B] rounded-[50%] p-4 ml-4 mr-4 overflow-hidden flex justify-center align-center"><div class="font-bold text-white flex justify-center align-center"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="40" height="60" viewBox="0,0,256,256" style="fill:#000000;">
            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.12,5.12)"><path d="M25,2c-12.69047,0 -23,10.30953 -23,23c0,4.0791 1.11869,7.88588 2.98438,11.20898l-2.94727,10.52148c-0.09582,0.34262 -0.00241,0.71035 0.24531,0.96571c0.24772,0.25536 0.61244,0.35989 0.95781,0.27452l10.9707,-2.71875c3.22369,1.72098 6.88165,2.74805 10.78906,2.74805c12.69047,0 23,-10.30953 23,-23c0,-12.69047 -10.30953,-23 -23,-23zM25,4c11.60953,0 21,9.39047 21,21c0,11.60953 -9.39047,21 -21,21c-3.72198,0 -7.20788,-0.97037 -10.23828,-2.66602c-0.22164,-0.12385 -0.48208,-0.15876 -0.72852,-0.09766l-9.60742,2.38086l2.57617,-9.19141c0.07449,-0.26248 0.03851,-0.54399 -0.09961,-0.7793c-1.84166,-3.12289 -2.90234,-6.75638 -2.90234,-10.64648c0,-11.60953 9.39047,-21 21,-21zM16.64258,13c-0.64104,0 -1.55653,0.23849 -2.30859,1.04883c-0.45172,0.48672 -2.33398,2.32068 -2.33398,5.54492c0,3.36152 2.33139,6.2621 2.61328,6.63477h0.00195v0.00195c-0.02674,-0.03514 0.3578,0.52172 0.87109,1.18945c0.5133,0.66773 1.23108,1.54472 2.13281,2.49414c1.80347,1.89885 4.33914,4.09336 7.48633,5.43555c1.44932,0.61717 2.59271,0.98981 3.45898,1.26172c1.60539,0.5041 3.06762,0.42747 4.16602,0.26563c0.82216,-0.12108 1.72641,-0.51584 2.62109,-1.08203c0.89469,-0.56619 1.77153,-1.2702 2.1582,-2.33984c0.27701,-0.76683 0.41783,-1.47548 0.46875,-2.05859c0.02546,-0.29156 0.02869,-0.54888 0.00977,-0.78711c-0.01897,-0.23823 0.0013,-0.42071 -0.2207,-0.78516c-0.46557,-0.76441 -0.99283,-0.78437 -1.54297,-1.05664c-0.30567,-0.15128 -1.17595,-0.57625 -2.04883,-0.99219c-0.8719,-0.41547 -1.62686,-0.78344 -2.0918,-0.94922c-0.29375,-0.10568 -0.65243,-0.25782 -1.16992,-0.19922c-0.51749,0.0586 -1.0286,0.43198 -1.32617,0.87305c-0.28205,0.41807 -1.4175,1.75835 -1.76367,2.15234c-0.0046,-0.0028 0.02544,0.01104 -0.11133,-0.05664c-0.42813,-0.21189 -0.95173,-0.39205 -1.72656,-0.80078c-0.77483,-0.40873 -1.74407,-1.01229 -2.80469,-1.94727v-0.00195c-1.57861,-1.38975 -2.68437,-3.1346 -3.0332,-3.7207c0.0235,-0.02796 -0.00279,0.0059 0.04687,-0.04297l0.00195,-0.00195c0.35652,-0.35115 0.67247,-0.77056 0.93945,-1.07812c0.37854,-0.43609 0.54559,-0.82052 0.72656,-1.17969c0.36067,-0.71583 0.15985,-1.50352 -0.04883,-1.91797v-0.00195c0.01441,0.02867 -0.11288,-0.25219 -0.25,-0.57617c-0.13751,-0.32491 -0.31279,-0.74613 -0.5,-1.19531c-0.37442,-0.89836 -0.79243,-1.90595 -1.04102,-2.49609v-0.00195c-0.29285,-0.69513 -0.68904,-1.1959 -1.20703,-1.4375c-0.51799,-0.2416 -0.97563,-0.17291 -0.99414,-0.17383h-0.00195c-0.36964,-0.01705 -0.77527,-0.02148 -1.17773,-0.02148zM16.64258,15c0.38554,0 0.76564,0.0047 1.08398,0.01953c0.32749,0.01632 0.30712,0.01766 0.24414,-0.01172c-0.06399,-0.02984 0.02283,-0.03953 0.20898,0.40234c0.24341,0.57785 0.66348,1.58909 1.03906,2.49023c0.18779,0.45057 0.36354,0.87343 0.50391,1.20508c0.14036,0.33165 0.21642,0.51683 0.30469,0.69336v0.00195l0.00195,0.00195c0.08654,0.17075 0.07889,0.06143 0.04883,0.12109c-0.21103,0.41883 -0.23966,0.52166 -0.45312,0.76758c-0.32502,0.37443 -0.65655,0.792 -0.83203,0.96484c-0.15353,0.15082 -0.43055,0.38578 -0.60352,0.8457c-0.17323,0.46063 -0.09238,1.09262 0.18555,1.56445c0.37003,0.62819 1.58941,2.6129 3.48438,4.28125c1.19338,1.05202 2.30519,1.74828 3.19336,2.2168c0.88817,0.46852 1.61157,0.74215 1.77344,0.82227c0.38438,0.19023 0.80448,0.33795 1.29297,0.2793c0.48849,-0.05865 0.90964,-0.35504 1.17773,-0.6582l0.00195,-0.00195c0.3568,-0.40451 1.41702,-1.61513 1.92578,-2.36133c0.02156,0.0076 0.0145,0.0017 0.18359,0.0625v0.00195h0.00195c0.0772,0.02749 1.04413,0.46028 1.90625,0.87109c0.86212,0.41081 1.73716,0.8378 2.02148,0.97852c0.41033,0.20308 0.60422,0.33529 0.6543,0.33594c0.00338,0.08798 0.0068,0.18333 -0.00586,0.32813c-0.03507,0.40164 -0.14243,0.95757 -0.35742,1.55273c-0.10532,0.29136 -0.65389,0.89227 -1.3457,1.33008c-0.69181,0.43781 -1.53386,0.74705 -1.8457,0.79297c-0.9376,0.13815 -2.05083,0.18859 -3.27344,-0.19531c-0.84773,-0.26609 -1.90476,-0.61053 -3.27344,-1.19336c-2.77581,-1.18381 -5.13503,-3.19825 -6.82031,-4.97266c-0.84264,-0.8872 -1.51775,-1.71309 -1.99805,-2.33789c-0.4794,-0.62364 -0.68874,-0.94816 -0.86328,-1.17773l-0.00195,-0.00195c-0.30983,-0.40973 -2.20703,-3.04868 -2.20703,-5.42578c0,-2.51576 1.1685,-3.50231 1.80078,-4.18359c0.33194,-0.35766 0.69484,-0.41016 0.8418,-0.41016z"></path></g></g>
            </svg></div></div></a></div>
            <div class="wrap_wa_list">
                <ul>
            ';
            
            if( !empty( $wa_room ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_room .'" target="_blank">Villa Reservation</a></li>';
            }
            
            if( !empty( $wa_restaurant ) ) {
                 $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_restaurant .'" target="_blank">Restaurant Reservation</a></li>';
            }
            
            if( !empty( $wa_inhouse_guest ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_inhouse_guest .'" target="_blank"> In-house Guest Inquiry</a></li>';
            }
    
            $wa_html .= '</ul></div>';
            
             add_variable('wa_numb', $wa_html);
        }

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('events', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'events', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/events/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'events');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'events');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'events');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/events/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/events';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/events';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/events/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/events/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);
        set_canonical_link($post_data);

        $button_book_hero = '
            <button class="container container-button-book-now book-now-hero clearfix">
                <p class="text _text text-book-now">'.$tr_string['book_now'].'</p>
            </button>
        ';
        add_actions( 'button_book_hero', $button_book_hero);

        parse_template( 'page-block', 'pblock', false );
    }
    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Weddings Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function weddings_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/weddings_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // STRING TRANSLATIONS
    $translations = array('Inclusion', 'Additional Service', 'Terms Conditions');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);
    
    $destination_id = $corporatedata[0]['id'];
    add_actions( 'booking_popup_wedding', 'booking_popup_wedding_content', $destination, $destination_id );
    
    $bg_wedding_events_reservation      = get_additional_field( $destination_id, 'bg_image_events', 'destinations' );
    if(!empty($bg_wedding_events_reservation))
    {
        $bg_wedding_events_reservation = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$bg_wedding_events_reservation;
    }
    else
    {
        $bg_wedding_events_reservation = HTTP.TEMPLATE_URL.'/assets/images/wedding.jpg';
    }
    add_variable( 'bg_wedding_events_reservation', $bg_wedding_events_reservation);

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // explode title
        $explode_title = explode(" at ", $post_title);
        
        // ADDITIONAL FIELD
        $price      = get_additional_field( $post_id, 'price', 'weddings' );

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'price', $price);

        $button_book = '<button data-title="'. $post_title .'" class="text-book-now button-book-bg" data-aos="fade-up"> Book Now </button>';
        
        // GET INCLUSION (RULE)
        $inclusion_content = "";
        $inclusion = get_rule_list_post($post_id, 'weddings','inclusion', 1, $lang, $check_lang);
        if(!empty($inclusion))
        {
            $inclusion_content = '
                <div class="additional-list clearfix">
                    <h3>'.$tr_string['inclusion'].':</h3>
                    '.$inclusion.'
                </div>
            ';
        }
        add_variable( 'inclusion_content', $inclusion_content);
        add_variable( 'button_book', $button_book);

        // GET ADDITIONAL SERVICE (RULE)
        $additional_content = "";
        $additional = get_rule_list_post($post_id, 'weddings','additional_service', 1, $lang, $check_lang);
        if(!empty($inclusion))
        {
            $additional_content = '
                <div class="additional-list clearfix">
                    <h3>'.$tr_string['additional_service'].':</h3>
                    '.$additional.'
                </div>
            ';
        }
        add_variable( 'additional_content', $additional_content);

        // GET TERMS AND CONDITIONS (RULE)
        $terms_conditions_content = "";
        $terms_conditions = get_rule_list_post($post_id, 'weddings','terms_conditions', 1, $lang, $check_lang);
        if(!empty($terms_conditions))
        {
            $terms_conditions_content = '
                <div class="terms-conditions clearfix">
                    <h3>'.$tr_string['terms_conditions'].':</h3>
                    '.$terms_conditions.'
                </div>
            ';
        }
        add_variable( 'terms_conditions_content', $terms_conditions_content);
        
        $data_page = get_meta_data('post_type_setting', 'accommodation', $corporatedata[0]['id']);
        
        $post_type_setting = json_decode($data_page, true);
       
       
        $wa                = get_additional_field($corporatedata[0]['id'], 'villa_reservation', 'destinations');;
        $wa_restaurant     = get_additional_field($corporatedata[0]['id'], 'restaurant_reservation', 'destinations');
        $wa_inhouse_guest  = get_additional_field($corporatedata[0]['id'], 'guest_reservation', 'destinations');
        
        $wa_room = isset( $wa ) && !empty( $wa ) ? $wa : '';
        $wa_restaurant = isset( $wa_restaurant ) && !empty( $wa_restaurant ) ? $wa_restaurant : '';
        $wa_inhouse_guest = isset( $wa_inhouse_guest ) && !empty( $wa_inhouse_guest ) ? $wa_inhouse_guest : '';
        
        
        if( !empty( $wa_room ) || !empty( $wa_restaurant ) ||  !empty( $wa_inhouse_guest ) ) {
        $wa_html = '<div class="floating_wa">
            <a>
                <div class="bg-[#A6631B] rounded-[50%] p-4 ml-4 mr-4 overflow-hidden flex justify-center align-center"><div class="font-bold text-white flex justify-center align-center"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="40" height="60" viewBox="0,0,256,256" style="fill:#000000;">
            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.12,5.12)"><path d="M25,2c-12.69047,0 -23,10.30953 -23,23c0,4.0791 1.11869,7.88588 2.98438,11.20898l-2.94727,10.52148c-0.09582,0.34262 -0.00241,0.71035 0.24531,0.96571c0.24772,0.25536 0.61244,0.35989 0.95781,0.27452l10.9707,-2.71875c3.22369,1.72098 6.88165,2.74805 10.78906,2.74805c12.69047,0 23,-10.30953 23,-23c0,-12.69047 -10.30953,-23 -23,-23zM25,4c11.60953,0 21,9.39047 21,21c0,11.60953 -9.39047,21 -21,21c-3.72198,0 -7.20788,-0.97037 -10.23828,-2.66602c-0.22164,-0.12385 -0.48208,-0.15876 -0.72852,-0.09766l-9.60742,2.38086l2.57617,-9.19141c0.07449,-0.26248 0.03851,-0.54399 -0.09961,-0.7793c-1.84166,-3.12289 -2.90234,-6.75638 -2.90234,-10.64648c0,-11.60953 9.39047,-21 21,-21zM16.64258,13c-0.64104,0 -1.55653,0.23849 -2.30859,1.04883c-0.45172,0.48672 -2.33398,2.32068 -2.33398,5.54492c0,3.36152 2.33139,6.2621 2.61328,6.63477h0.00195v0.00195c-0.02674,-0.03514 0.3578,0.52172 0.87109,1.18945c0.5133,0.66773 1.23108,1.54472 2.13281,2.49414c1.80347,1.89885 4.33914,4.09336 7.48633,5.43555c1.44932,0.61717 2.59271,0.98981 3.45898,1.26172c1.60539,0.5041 3.06762,0.42747 4.16602,0.26563c0.82216,-0.12108 1.72641,-0.51584 2.62109,-1.08203c0.89469,-0.56619 1.77153,-1.2702 2.1582,-2.33984c0.27701,-0.76683 0.41783,-1.47548 0.46875,-2.05859c0.02546,-0.29156 0.02869,-0.54888 0.00977,-0.78711c-0.01897,-0.23823 0.0013,-0.42071 -0.2207,-0.78516c-0.46557,-0.76441 -0.99283,-0.78437 -1.54297,-1.05664c-0.30567,-0.15128 -1.17595,-0.57625 -2.04883,-0.99219c-0.8719,-0.41547 -1.62686,-0.78344 -2.0918,-0.94922c-0.29375,-0.10568 -0.65243,-0.25782 -1.16992,-0.19922c-0.51749,0.0586 -1.0286,0.43198 -1.32617,0.87305c-0.28205,0.41807 -1.4175,1.75835 -1.76367,2.15234c-0.0046,-0.0028 0.02544,0.01104 -0.11133,-0.05664c-0.42813,-0.21189 -0.95173,-0.39205 -1.72656,-0.80078c-0.77483,-0.40873 -1.74407,-1.01229 -2.80469,-1.94727v-0.00195c-1.57861,-1.38975 -2.68437,-3.1346 -3.0332,-3.7207c0.0235,-0.02796 -0.00279,0.0059 0.04687,-0.04297l0.00195,-0.00195c0.35652,-0.35115 0.67247,-0.77056 0.93945,-1.07812c0.37854,-0.43609 0.54559,-0.82052 0.72656,-1.17969c0.36067,-0.71583 0.15985,-1.50352 -0.04883,-1.91797v-0.00195c0.01441,0.02867 -0.11288,-0.25219 -0.25,-0.57617c-0.13751,-0.32491 -0.31279,-0.74613 -0.5,-1.19531c-0.37442,-0.89836 -0.79243,-1.90595 -1.04102,-2.49609v-0.00195c-0.29285,-0.69513 -0.68904,-1.1959 -1.20703,-1.4375c-0.51799,-0.2416 -0.97563,-0.17291 -0.99414,-0.17383h-0.00195c-0.36964,-0.01705 -0.77527,-0.02148 -1.17773,-0.02148zM16.64258,15c0.38554,0 0.76564,0.0047 1.08398,0.01953c0.32749,0.01632 0.30712,0.01766 0.24414,-0.01172c-0.06399,-0.02984 0.02283,-0.03953 0.20898,0.40234c0.24341,0.57785 0.66348,1.58909 1.03906,2.49023c0.18779,0.45057 0.36354,0.87343 0.50391,1.20508c0.14036,0.33165 0.21642,0.51683 0.30469,0.69336v0.00195l0.00195,0.00195c0.08654,0.17075 0.07889,0.06143 0.04883,0.12109c-0.21103,0.41883 -0.23966,0.52166 -0.45312,0.76758c-0.32502,0.37443 -0.65655,0.792 -0.83203,0.96484c-0.15353,0.15082 -0.43055,0.38578 -0.60352,0.8457c-0.17323,0.46063 -0.09238,1.09262 0.18555,1.56445c0.37003,0.62819 1.58941,2.6129 3.48438,4.28125c1.19338,1.05202 2.30519,1.74828 3.19336,2.2168c0.88817,0.46852 1.61157,0.74215 1.77344,0.82227c0.38438,0.19023 0.80448,0.33795 1.29297,0.2793c0.48849,-0.05865 0.90964,-0.35504 1.17773,-0.6582l0.00195,-0.00195c0.3568,-0.40451 1.41702,-1.61513 1.92578,-2.36133c0.02156,0.0076 0.0145,0.0017 0.18359,0.0625v0.00195h0.00195c0.0772,0.02749 1.04413,0.46028 1.90625,0.87109c0.86212,0.41081 1.73716,0.8378 2.02148,0.97852c0.41033,0.20308 0.60422,0.33529 0.6543,0.33594c0.00338,0.08798 0.0068,0.18333 -0.00586,0.32813c-0.03507,0.40164 -0.14243,0.95757 -0.35742,1.55273c-0.10532,0.29136 -0.65389,0.89227 -1.3457,1.33008c-0.69181,0.43781 -1.53386,0.74705 -1.8457,0.79297c-0.9376,0.13815 -2.05083,0.18859 -3.27344,-0.19531c-0.84773,-0.26609 -1.90476,-0.61053 -3.27344,-1.19336c-2.77581,-1.18381 -5.13503,-3.19825 -6.82031,-4.97266c-0.84264,-0.8872 -1.51775,-1.71309 -1.99805,-2.33789c-0.4794,-0.62364 -0.68874,-0.94816 -0.86328,-1.17773l-0.00195,-0.00195c-0.30983,-0.40973 -2.20703,-3.04868 -2.20703,-5.42578c0,-2.51576 1.1685,-3.50231 1.80078,-4.18359c0.33194,-0.35766 0.69484,-0.41016 0.8418,-0.41016z"></path></g></g>
            </svg></div></div></a></div>
            <div class="wrap_wa_list">
                <ul>
            ';
            
            if( !empty( $wa_room ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_room .'" target="_blank">Villa Reservation</a></li>';
            }
            
            if( !empty( $wa_restaurant ) ) {
                 $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_restaurant .'" target="_blank">Restaurant Reservation</a></li>';
            }
            
            if( !empty( $wa_inhouse_guest ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_inhouse_guest .'" target="_blank"> In-house Guest Inquiry</a></li>';
            }
    
            $wa_html .= '</ul></div>';
            
             add_variable('wa_numb', $wa_html);
        }
        
        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('weddings', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'weddings', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/weddings/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'weddings');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'weddings');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'weddings');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/weddings/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/weddings';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/weddings';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/weddings/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/weddings/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);
        set_canonical_link($post_data);

        add_actions( 'lang', $lang );
        add_actions( 'check_lang', $check_lang );

        parse_template( 'page-block', 'pblock', false );
    }
    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| WHAT TO DO Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function whattodo_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    // STRING TRANSLATIONS
    $translations = array('What To Do Act In', 'What To Do Around');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    set_template( TEMPLATE_PATH . '/template/whattodo_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];
        $post_type    = $post_data['post_type'];

        $type_for_tr_string = str_replace("-", "_", $post_type);
        $subtitle = $tr_string[$type_for_tr_string];

        // explode title
        $explode_title = explode(" at ", $post_title);

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'post_subtitle', $subtitle);

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list($post_type, '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $id_product, $post_type, false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/'.$post_type.'/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, $post_type);
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, $post_type);
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, $post_type);
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$post_type.'/'.$sef_product.'.html';
                }


                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/'.$post_type;
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/'.$post_type;
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$post_type.'/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/'.$post_type.'/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);
        set_canonical_link($post_data);

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| News Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function news_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/news_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $disqus_count_code = get_meta_data( 'disqus_count_code', 'static_setting' );  
    $disqus_script    = get_meta_data( 'disqus_script', 'static_setting' );

    add_variable( 'disqus_script', $disqus_script);
    add_actions( 'include-js', 'get_custom_javascript', $disqus_count_code, '', 1 );
    
    // STRING TRANSLATIONS
    $translations = array('Download This Press', 'Download');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);
    
    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_content = str_replace("<table", '<div class="table-responsive"><table class="responsive"', $post_content);
        $post_content = str_replace("</table>", '</table></div>', $post_content);
        $post_brief   = $post_data['post_brief'];
        $post_type    = $post_data['post_type'];
        
        $file_download = get_additional_field( $post_id, 'file_download', 'news' );

        add_actions( 'button_download_hero', 'get_button_download_hero', $file_download, $tr_string['download_this_press'] );


        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'button_download_file', get_button_download_hero($file_download, $tr_string['download']));

        // GET OTHER NEWS
        $where_add = " AND larticle_id != $post_id AND a.lstatus_file_download=1";
        $product = get_post_list('news', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $id_product, 'news', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/news/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'news');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'news');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'news');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/news/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }
        
        $wa                = get_additional_field($corporatedata[0]['id'], 'villa_reservation', 'destinations');;
        $wa_restaurant     = get_additional_field($corporatedata[0]['id'], 'restaurant_reservation', 'destinations');
        $wa_inhouse_guest  = get_additional_field($corporatedata[0]['id'], 'guest_reservation', 'destinations');
        
        $wa_room = isset( $wa ) && !empty( $wa ) ? $wa : '';
        $wa_restaurant = isset( $wa_restaurant ) && !empty( $wa_restaurant ) ? $wa_restaurant : '';
        $wa_inhouse_guest = isset( $wa_inhouse_guest ) && !empty( $wa_inhouse_guest ) ? $wa_inhouse_guest : '';
        
        
        if( !empty( $wa_room ) || !empty( $wa_restaurant ) ||  !empty( $wa_inhouse_guest ) ) {
        $wa_html = '<div class="floating_wa">
            <a>
                <div class="bg-[#A6631B] rounded-[50%] p-4 ml-4 mr-4 overflow-hidden flex justify-center align-center"><div class="font-bold text-white flex justify-center align-center"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="40" height="60" viewBox="0,0,256,256" style="fill:#000000;">
            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.12,5.12)"><path d="M25,2c-12.69047,0 -23,10.30953 -23,23c0,4.0791 1.11869,7.88588 2.98438,11.20898l-2.94727,10.52148c-0.09582,0.34262 -0.00241,0.71035 0.24531,0.96571c0.24772,0.25536 0.61244,0.35989 0.95781,0.27452l10.9707,-2.71875c3.22369,1.72098 6.88165,2.74805 10.78906,2.74805c12.69047,0 23,-10.30953 23,-23c0,-12.69047 -10.30953,-23 -23,-23zM25,4c11.60953,0 21,9.39047 21,21c0,11.60953 -9.39047,21 -21,21c-3.72198,0 -7.20788,-0.97037 -10.23828,-2.66602c-0.22164,-0.12385 -0.48208,-0.15876 -0.72852,-0.09766l-9.60742,2.38086l2.57617,-9.19141c0.07449,-0.26248 0.03851,-0.54399 -0.09961,-0.7793c-1.84166,-3.12289 -2.90234,-6.75638 -2.90234,-10.64648c0,-11.60953 9.39047,-21 21,-21zM16.64258,13c-0.64104,0 -1.55653,0.23849 -2.30859,1.04883c-0.45172,0.48672 -2.33398,2.32068 -2.33398,5.54492c0,3.36152 2.33139,6.2621 2.61328,6.63477h0.00195v0.00195c-0.02674,-0.03514 0.3578,0.52172 0.87109,1.18945c0.5133,0.66773 1.23108,1.54472 2.13281,2.49414c1.80347,1.89885 4.33914,4.09336 7.48633,5.43555c1.44932,0.61717 2.59271,0.98981 3.45898,1.26172c1.60539,0.5041 3.06762,0.42747 4.16602,0.26563c0.82216,-0.12108 1.72641,-0.51584 2.62109,-1.08203c0.89469,-0.56619 1.77153,-1.2702 2.1582,-2.33984c0.27701,-0.76683 0.41783,-1.47548 0.46875,-2.05859c0.02546,-0.29156 0.02869,-0.54888 0.00977,-0.78711c-0.01897,-0.23823 0.0013,-0.42071 -0.2207,-0.78516c-0.46557,-0.76441 -0.99283,-0.78437 -1.54297,-1.05664c-0.30567,-0.15128 -1.17595,-0.57625 -2.04883,-0.99219c-0.8719,-0.41547 -1.62686,-0.78344 -2.0918,-0.94922c-0.29375,-0.10568 -0.65243,-0.25782 -1.16992,-0.19922c-0.51749,0.0586 -1.0286,0.43198 -1.32617,0.87305c-0.28205,0.41807 -1.4175,1.75835 -1.76367,2.15234c-0.0046,-0.0028 0.02544,0.01104 -0.11133,-0.05664c-0.42813,-0.21189 -0.95173,-0.39205 -1.72656,-0.80078c-0.77483,-0.40873 -1.74407,-1.01229 -2.80469,-1.94727v-0.00195c-1.57861,-1.38975 -2.68437,-3.1346 -3.0332,-3.7207c0.0235,-0.02796 -0.00279,0.0059 0.04687,-0.04297l0.00195,-0.00195c0.35652,-0.35115 0.67247,-0.77056 0.93945,-1.07812c0.37854,-0.43609 0.54559,-0.82052 0.72656,-1.17969c0.36067,-0.71583 0.15985,-1.50352 -0.04883,-1.91797v-0.00195c0.01441,0.02867 -0.11288,-0.25219 -0.25,-0.57617c-0.13751,-0.32491 -0.31279,-0.74613 -0.5,-1.19531c-0.37442,-0.89836 -0.79243,-1.90595 -1.04102,-2.49609v-0.00195c-0.29285,-0.69513 -0.68904,-1.1959 -1.20703,-1.4375c-0.51799,-0.2416 -0.97563,-0.17291 -0.99414,-0.17383h-0.00195c-0.36964,-0.01705 -0.77527,-0.02148 -1.17773,-0.02148zM16.64258,15c0.38554,0 0.76564,0.0047 1.08398,0.01953c0.32749,0.01632 0.30712,0.01766 0.24414,-0.01172c-0.06399,-0.02984 0.02283,-0.03953 0.20898,0.40234c0.24341,0.57785 0.66348,1.58909 1.03906,2.49023c0.18779,0.45057 0.36354,0.87343 0.50391,1.20508c0.14036,0.33165 0.21642,0.51683 0.30469,0.69336v0.00195l0.00195,0.00195c0.08654,0.17075 0.07889,0.06143 0.04883,0.12109c-0.21103,0.41883 -0.23966,0.52166 -0.45312,0.76758c-0.32502,0.37443 -0.65655,0.792 -0.83203,0.96484c-0.15353,0.15082 -0.43055,0.38578 -0.60352,0.8457c-0.17323,0.46063 -0.09238,1.09262 0.18555,1.56445c0.37003,0.62819 1.58941,2.6129 3.48438,4.28125c1.19338,1.05202 2.30519,1.74828 3.19336,2.2168c0.88817,0.46852 1.61157,0.74215 1.77344,0.82227c0.38438,0.19023 0.80448,0.33795 1.29297,0.2793c0.48849,-0.05865 0.90964,-0.35504 1.17773,-0.6582l0.00195,-0.00195c0.3568,-0.40451 1.41702,-1.61513 1.92578,-2.36133c0.02156,0.0076 0.0145,0.0017 0.18359,0.0625v0.00195h0.00195c0.0772,0.02749 1.04413,0.46028 1.90625,0.87109c0.86212,0.41081 1.73716,0.8378 2.02148,0.97852c0.41033,0.20308 0.60422,0.33529 0.6543,0.33594c0.00338,0.08798 0.0068,0.18333 -0.00586,0.32813c-0.03507,0.40164 -0.14243,0.95757 -0.35742,1.55273c-0.10532,0.29136 -0.65389,0.89227 -1.3457,1.33008c-0.69181,0.43781 -1.53386,0.74705 -1.8457,0.79297c-0.9376,0.13815 -2.05083,0.18859 -3.27344,-0.19531c-0.84773,-0.26609 -1.90476,-0.61053 -3.27344,-1.19336c-2.77581,-1.18381 -5.13503,-3.19825 -6.82031,-4.97266c-0.84264,-0.8872 -1.51775,-1.71309 -1.99805,-2.33789c-0.4794,-0.62364 -0.68874,-0.94816 -0.86328,-1.17773l-0.00195,-0.00195c-0.30983,-0.40973 -2.20703,-3.04868 -2.20703,-5.42578c0,-2.51576 1.1685,-3.50231 1.80078,-4.18359c0.33194,-0.35766 0.69484,-0.41016 0.8418,-0.41016z"></path></g></g>
            </svg></div></div></a></div>
            <div class="wrap_wa_list">
                <ul>
            ';
            
            if( !empty( $wa_room ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_room .'" target="_blank">Villa Reservation</a></li>';
            }
            
            if( !empty( $wa_restaurant ) ) {
                 $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_restaurant .'" target="_blank">Restaurant Reservation</a></li>';
            }
            
            if( !empty( $wa_inhouse_guest ) ) {
                $wa_html .= '<li style="list-style: none"><a  href="https://wa.me/'. $wa_inhouse_guest .'" target="_blank"> In-house Guest Inquiry</a></li>';
            }
    
            $wa_html .= '</ul></div>';
            
             add_variable('wa_numb', $wa_html);
        }
        

        $link_explore  = HTTP.site_url().'/'.$destination.'/news';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/news';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/news/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/news/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);
        set_canonical_link($post_data);


        parse_template( 'page-block', 'pblock', false );
    }

    return return_template( 'page_template' );
}

function get_button_download_hero($file_download, $text_download)
{
    $button = "";

    if( !empty( $file_download ) && file_exists( PLUGINS_PATH . '/additional/file_download/' . $file_download ) )
    {
        $button = '
            <div class="detail-download-item">
                <a target="_blank" href="'.URL_PLUGINS . 'additional/file_download/' . $file_download.'">'.$text_download.'</a>
            </div>
        ';
    }

    return $button;
}
?>