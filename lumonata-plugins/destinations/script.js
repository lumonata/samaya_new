

jQuery(document).ready(function(){
    var state_url  = jQuery('[name=state_url]').val();
    var plugin_url = jQuery('[name=plugin_url]').val();

    //-- Background Image Function
    jQuery('[name=bg_image]').change(function(el){
        var data = new FormData();
            data.append('ajax_key', 'add-background-image');
            data.append('app_name', 'destinations');
            data.append('post_id', jQuery('[name=post_id]').val());
            data.append('bg_image', el.currentTarget.files[0]); 
        
        var xhrObject = new XMLHttpRequest();  
        xhrObject.open('POST', state_url);  
        xhrObject.onreadystatechange = function(){
            if(xhrObject.readyState==4 && xhrObject.status==200){
                var e = JSON.parse(xhrObject.responseText);

                if(e.result=='success'){
                     var images =
                    '<div class="box">'+
                        '<img src="http://'+plugin_url+'/background/'+e.filename+'" />'+
                        '<div class="overlay" style="display: none;">'+
                            '<a data-post-id="'+e.post_id+'" class="bg-delete-header">'+
                                '<img src="http://'+plugin_url+'/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.background-wrapp').html(images);
                }else{
                    alert('Failed to add background images')   
                }
            }
        }

        xhrObject.send(data);
    });

    jQuery('.bg-delete-header').live('click',function(){
        var selector = jQuery(this).parent().parent();
        var url = state_url;
        var prm = new Object;
            prm.ajax_key = 'delete-background-image';
            prm.post_id  = jQuery(this).attr('data-post-id');
            
        jQuery.post(url, prm, function(e){
            if(e.result=='success'){
                selector.fadeOut(200,function(){ jQuery(this).remove(); });     
            }
        },'json');
    });

    //-- Background Image HomePage Function
    jQuery('[name=background_image_homepage]').change(function(el){
        var data = new FormData();
            data.append('ajax_key', 'add-background-image-homepage');
            data.append('app_name', 'destinations');
            data.append('post_id', jQuery('[name=post_id]').val());
            data.append('background_image_homepage', el.currentTarget.files[0]); 
        
        var xhrObject = new XMLHttpRequest();  
        xhrObject.open('POST', state_url);  
        xhrObject.onreadystatechange = function(){
            if(xhrObject.readyState==4 && xhrObject.status==200){
                var e = JSON.parse(xhrObject.responseText);

                if(e.result=='success'){
                     var images =
                    '<div class="box">'+
                        '<img src="http://'+plugin_url+'/background/'+e.filename+'" />'+
                        '<div class="overlay" style="display: none;">'+
                            '<a data-post-id="'+e.post_id+'" class="bg-homepage-delete-header">'+
                                '<img src="http://'+plugin_url+'/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.background_image_homepage').html(images);
                }else{
                    alert('Failed to add background images homepage')   
                }
            }
        }

        xhrObject.send(data);
    });

    jQuery('.bg-homepage-delete-header').live('click',function(){
        var selector = jQuery(this).parent().parent();
        var url = state_url;
        var prm = new Object;
            prm.ajax_key = 'delete-background-image-homepage';
            prm.post_id  = jQuery(this).attr('data-post-id');
            
        jQuery.post(url, prm, function(e){
            if(e.result=='success'){
                selector.fadeOut(200,function(){ jQuery(this).remove(); });     
            }
        },'json');
    });


    //-- Background Image Events and Wedding Reservation Function
    jQuery('[name=bg_events]').change(function(el){
        var data = new FormData();
            data.append('ajax_key', 'add-bg-image-events');
            data.append('app_name', 'destinations');
            data.append('post_id', jQuery('[name=post_id]').val());
            data.append('bg_events', el.currentTarget.files[0]); 
        
        var xhrObject = new XMLHttpRequest();  
        xhrObject.open('POST', state_url);  
        xhrObject.onreadystatechange = function(){
            if(xhrObject.readyState==4 && xhrObject.status==200){
                var e = JSON.parse(xhrObject.responseText);

                if(e.result=='success'){
                     var images =
                    '<div class="box">'+
                        '<img src="http://'+plugin_url+'/background/'+e.filename+'" />'+
                        '<div class="overlay" style="display: none;">'+
                            '<a data-post-id="'+e.post_id+'" class="bg-delete-events">'+
                                '<img src="http://'+plugin_url+'/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.bg-events-wrapp').html(images);
                }else{
                    alert('Failed to add background images')   
                }
            }
        }

        xhrObject.send(data);
    });

    jQuery('.bg-delete-events').live('click',function(){
        var selector = jQuery(this).parent().parent();
        var url = state_url;
        var prm = new Object;
            prm.ajax_key = 'delete-bg-image-events';
            prm.post_id  = jQuery(this).attr('data-post-id');
            
        jQuery.post(url, prm, function(e){
            if(e.result=='success'){
                selector.fadeOut(200,function(){ jQuery(this).remove(); });     
            }
        },'json');
    });


    //-- Footer Image Function
    jQuery('[name=footer_image]').change(function(el){
        var data = new FormData();
            data.append('ajax_key', 'add-footer-image');
            data.append('app_name', 'destinations');
            data.append('post_id', jQuery('[name=post_id]').val());
            data.append('footer_image', el.currentTarget.files[0]); 
        
        var xhrObject = new XMLHttpRequest();  
        xhrObject.open('POST', state_url);  
        xhrObject.onreadystatechange = function(){
            if(xhrObject.readyState==4 && xhrObject.status==200){
                var e = JSON.parse(xhrObject.responseText);

                if(e.result=='success'){
                     var images =
                    '<div class="box">'+
                        '<img src="http://'+plugin_url+'/footer/'+e.filename+'" />'+
                        '<div class="overlay" style="display: none;">'+
                            '<a data-post-id="'+e.post_id+'" class="footer-delete-header">'+
                                '<img src="http://'+plugin_url+'/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.footer_image_wrap').html(images);
                }else{
                    alert('Failed to add footer images')   
                }
            }
        }

        xhrObject.send(data);
    });

    jQuery('.footer-delete-header').live('click',function(){
        var selector = jQuery(this).parent().parent();
        var url = state_url;
        var prm = new Object;
            prm.ajax_key = 'delete-footer-image';
            prm.post_id  = jQuery(this).attr('data-post-id');
            
        jQuery.post(url, prm, function(e){
            if(e.result=='success'){
                selector.fadeOut(200,function(){ jQuery(this).remove(); });     
            }
        },'json');
    });


    //-- Popup Image Newsletter Function
    jQuery('[name=popup_image_newsletter]').change(function(el){
        var data = new FormData();
            data.append('ajax_key', 'add-popup-image-newsletter');
            data.append('app_name', 'destinations');
            data.append('post_id', jQuery('[name=post_id]').val());
            data.append('popup_image_newsletter', el.currentTarget.files[0]); 
        
        var xhrObject = new XMLHttpRequest();  
        xhrObject.open('POST', state_url);  
        xhrObject.onreadystatechange = function(){
            if(xhrObject.readyState==4 && xhrObject.status==200){
                var e = JSON.parse(xhrObject.responseText);
                // console.log(e);
                if(e.result=='success'){
                     var images =
                    '<div class="box">'+
                        '<img src="http://'+plugin_url+'/popup_image_newsletter/'+e.filename+'" />'+
                        '<div class="overlay" style="display: none;">'+
                            '<a data-post-id="'+e.post_id+'" class="popup-image-newsletter-delete">'+
                                '<img src="http://'+plugin_url+'/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.popup_image_newsletter').html(images);
                }else{
                    alert('Failed to add popup image newsletter')   
                }
            }
        }

        xhrObject.send(data);
    });

    jQuery('.popup-image-newsletter-delete').live('click',function(){
        var selector = jQuery(this).parent().parent();
        var url = state_url;
        var prm = new Object;
            prm.ajax_key = 'delete-popup-image-newsletter';
            prm.post_id  = jQuery(this).attr('data-post-id');
            
        jQuery.post(url, prm, function(e){
            if(e.result=='success'){
                selector.fadeOut(200,function(){ jQuery(this).remove(); });     
            }
        },'json');
    });


    //-- Background Image Policy Function
    jQuery('[name=bg_image_policy]').change(function(el){
        var data = new FormData();
            data.append('ajax_key', 'add-bg-image-policy');
            data.append('app_name', 'destinations');
            data.append('post_id', jQuery('[name=post_id]').val());
            data.append('bg_image_policy', el.currentTarget.files[0]); 
        
        var xhrObject = new XMLHttpRequest();  
        xhrObject.open('POST', state_url);  
        xhrObject.onreadystatechange = function(){
            if(xhrObject.readyState==4 && xhrObject.status==200){
                var e = JSON.parse(xhrObject.responseText);

                if(e.result=='success'){
                     var images =
                    '<div class="box">'+
                        '<img src="http://'+plugin_url+'/bg_image_policy/'+e.filename+'" />'+
                        '<div class="overlay" style="display: none;">'+
                            '<a data-post-id="'+e.post_id+'" class="bg-policy-delete-header">'+
                                '<img src="http://'+plugin_url+'/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.bg_image_policy').html(images);
                }else{
                    alert('Failed to add background image policy')   
                }
            }
        }

        xhrObject.send(data);
    });

    jQuery('.bg-policy-delete-header').live('click',function(){
        var selector = jQuery(this).parent().parent();
        var url = state_url;
        var prm = new Object;
            prm.ajax_key = 'delete-bg-image-policy';
            prm.post_id  = jQuery(this).attr('data-post-id');
            
        jQuery.post(url, prm, function(e){
            if(e.result=='success'){
                selector.fadeOut(200,function(){ jQuery(this).remove(); });     
            }
        },'json');
    });


    //-- Dinner Reception Venue 1 Function
    jQuery('[name=dinner_reception_venue_1]').change(function(el){
        var data = new FormData();
            data.append('ajax_key', 'add-dinner-reception-venue-1');
            data.append('app_name', 'destinations');
            data.append('post_id', jQuery('[name=post_id]').val());
            data.append('dinner_reception_venue_1', el.currentTarget.files[0]); 
        
        var xhrObject = new XMLHttpRequest();  
        xhrObject.open('POST', state_url);  
        xhrObject.onreadystatechange = function(){
            if(xhrObject.readyState==4 && xhrObject.status==200){
                var e = JSON.parse(xhrObject.responseText);

                if(e.result=='success'){
                     var images =
                    '<div class="box">'+
                        '<img src="http://'+plugin_url+'/reception-venue/'+e.filename+'" />'+
                        '<div class="overlay" style="display: none;">'+
                            '<a data-post-id="'+e.post_id+'" class="reception-1-delete-header">'+
                                '<img src="http://'+plugin_url+'/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.dinner_reception_venue_1').html(images);
                }else{
                    alert('Failed to add dinner reception venue 1 images')   
                }
            }
        }

        xhrObject.send(data);
    });

    jQuery('.reception-1-delete-header').live('click',function(){
        var selector = jQuery(this).parent().parent();
        var url = state_url;
        var prm = new Object;
            prm.ajax_key = 'delete-reception-venue-1-image';
            prm.post_id  = jQuery(this).attr('data-post-id');
            
        jQuery.post(url, prm, function(e){
            if(e.result=='success'){
                selector.fadeOut(200,function(){ jQuery(this).remove(); });     
            }
        },'json');
    });

    //-- Dinner Reception Venue 1 Function
    jQuery('[name=dinner_reception_venue_2]').change(function(el){
        console.log("asdada");
        var data = new FormData();
            data.append('ajax_key', 'add-dinner-reception-venue-2');
            data.append('app_name', 'destinations');
            data.append('post_id', jQuery('[name=post_id]').val());
            data.append('dinner_reception_venue_2', el.currentTarget.files[0]); 
        
        var xhrObject = new XMLHttpRequest();  
        xhrObject.open('POST', state_url);  
        xhrObject.onreadystatechange = function(){
            if(xhrObject.readyState==4 && xhrObject.status==200){
                var e = JSON.parse(xhrObject.responseText);

                if(e.result=='success'){
                     var images =
                    '<div class="box">'+
                        '<img src="http://'+plugin_url+'/reception-venue/'+e.filename+'" />'+
                        '<div class="overlay" style="display: none;">'+
                            '<a data-post-id="'+e.post_id+'" class="reception-2-delete-header">'+
                                '<img src="http://'+plugin_url+'/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.dinner_reception_venue_2').html(images);
                }else{
                    alert('Failed to add dinner reception venue 2 images')   
                }
            }
        }

        xhrObject.send(data);
    });

    jQuery('.reception-2-delete-header').live('click',function(){
        var selector = jQuery(this).parent().parent();
        var url = state_url;
        var prm = new Object;
            prm.ajax_key = 'delete-reception-venue-2-image';
            prm.post_id  = jQuery(this).attr('data-post-id');
            
        jQuery.post(url, prm, function(e){
            if(e.result=='success'){
                selector.fadeOut(200,function(){ jQuery(this).remove(); });     
            }
        },'json');
    });



    //-- Coorporate Image Function
    jQuery('[name=cp_image]').change(function(el){       
        var data = new FormData();
            data.append('ajax_key', 'add-corporate-image');
            data.append('app_name', 'destinations');
            data.append('post_id', jQuery('[name=post_id]').val());
            data.append('cp_image', el.currentTarget.files[0]); 
        
        var xhrObject = new XMLHttpRequest();  
        xhrObject.open('POST', state_url);  
        xhrObject.onreadystatechange = function(){
            if(xhrObject.readyState==4 && xhrObject.status==200){
                var e = JSON.parse(xhrObject.responseText);

                if(e.result=='success'){
                     var images =
                    '<div class="box">'+
                        '<img src="http://'+plugin_url+'/background/'+e.filename+'" />'+
                        '<div class="overlay" style="display: none;">'+
                            '<a data-post-id="'+e.post_id+'" class="cp-delete-header">'+
                                '<img src="http://'+plugin_url+'/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.corporate-wrapp').html(images);
                }else{
                    alert('Failed to add corporate images')   
                }
            }
        }

        xhrObject.send(data);
    });

    jQuery('.cp-delete-header').live('click',function(){
        var selector = jQuery(this).parent().parent();
        var url = state_url;
        var prm = new Object;
            prm.ajax_key = 'delete-corporate-image';
            prm.post_id  = jQuery(this).attr('data-post-id');
            
        jQuery.post(url, prm, function(e){
            if(e.result=='success'){
                selector.fadeOut(200,function(){ jQuery(this).remove(); });     
            }
        },'json');
    });

    jQuery('.list-box, .background-wrapp .box, .corporate-wrapp .box, .footer_image_wrap .box, .dinner_reception_venue_1 .box, .dinner_reception_venue_2 .box, .bg-events-wrapp .box, .bg_image_policy .box, .popup_image_newsletter .box, .background_image_homepage .box').live('mouseover',function(){ 
        jQuery(this).find('.overlay').show(); 
    });

    jQuery('.list-box, .background-wrapp .box, .corporate-wrapp .box, .footer_image_wrap .box, .dinner_reception_venue_1 .box, .dinner_reception_venue_2 .box, .bg-events-wrapp .box, .bg_image_policy .box, .popup_image_newsletter .box, .background_image_homepage .box').live('mouseout',function(){ 
        jQuery(this).find('.overlay').hide(); 
    });
});