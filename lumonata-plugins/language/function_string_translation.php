<?php
add_privileges( 'administrator', 'language-string', 'insert' );
add_privileges( 'administrator', 'language-string', 'update' );
add_privileges( 'administrator', 'language-string', 'delete' );

add_actions('language-string','set_language_string_data');

function set_language_string_data()
{
    return language_string();
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Fungsi ini digunakan untuk mendapatkan jumlah data string language dari database
| -------------------------------------------------------------------------------------------------------------------------
*/
function is_num_string_translation()
{
	global $db;
	
	$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
	$q = $db->prepare_query($s, 'language-string');
	$r = $db->do_query($q);

	return $db->num_rows($r);
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk meyimpan data language ke database
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_string()
{
    global $db;
    
    $message  = '';
    $mess_arr = array();
    
    $qstring = $db->prepare_query("
        SELECT larticle_id, larticle_content
        FROM lumonata_articles
        WHERE larticle_type=%s AND larticle_status=%s
        LIMIT 0,1
    ",
        'string-translations', 'publish'
    );
    $rstring = $db->do_query($qstring);
    $nstring = $db->num_rows($rstring);
    
    if(isset($_POST['save_changes']))
	{
        $post_content = json_encode($_POST['string']);

        if($nstring > 0)
        {
            $dstring = $db->fetch_array($rstring);
            $string_id = $dstring['larticle_id'];

            $is_edit_success = update_article($string_id, 'String Translations', '', '', $post_content, 'publish', 'string-translations','', '', '', '', '', '', '', '', '', 'not-allowed', '');
        }
        else
        {
            $is_save_success = save_article('String Translations', '', '',  $post_content, 'publish', 'string-translations','', '', '', '', '', '', '', '', 'not-allowed');
        }

        header('location:'.get_state_url('language-string'));
    }

    // DEFINE STRING TRANSLATIONS
    $list_str = array(
        'Your Luxury Begins Here',
        'Whats Happening At Samaya Seminyak',
        'Whats Happening At Samaya Ubud',
        'Reviews at Tripadvisor',
        'Reviews',
        'Travellers Choice',
        'Book Now',
        'Explore More',
        'Direction',
        'Best Rate Guaranteed', 
        'Dates', 'Room', 
        'Choose Destination', 
        'Choose Room', 
        'Choose Date', 
        'Guest', 
        'Explore', 
        'See Packages', 
        'Stay Connected With Us', 
        'Destination', 
        'Choose', 
        'Access Code', 
        'Enter Code', 
        'About Us', 
        'Discover', 
        'See Map', 
        'Contact Us', 
        'Subscribe',
        'Please Complete All Fields',
        'Full Name',
        'Country',
        'Phone',
        'How Find Us', 
        'Your Email', 
        'Send', 
        'Visit Other Destination', 
        'Policy', 
        'All Rights Reserved', 
        'Menu', 
        'Details',
        'Accommodation',
        'Single',
        'King Size',
        'Double',
        'Double Twin',
        'Specifications',
        'Villa Amenities',
        'Villa Details', 
        'Villa Benefits', 
        'Villa Features', 
        'Villa Size',
        'Villa Location',
        'Bed Type', 
        'Maximum Capacity',
        'Pool Size',
        'Book', 
        'Arrival', 
        'Departure', 
        'Adults', 
        'Child', 
        'Choose Adults', 
        'Choose Child', 
        'Check Availability',
        'Cancel Booking',
        'Other Villa',
        'Restaurant Open', 
        'Dresscode', 
        'Breakfast', 
        'Lunch', 
        'Dining',
        'Reserve Table',
        'Asking Reservations Restaurant Online',
        'Our Menus', 
        'Download Our Menu', 
        'Reservation', 
        'Or', 
        'Call', 
        'Email', 
        'Other Dining',
        'Price',
        'Spa',
        'Reserve Spa',
        'Spa Menu',
        'Other Treatment',
        'Inclusion',
        'Additional Service',
        'Terms Conditions',
        'Weddings',
        'Other Weddings',
        'Events',
        'Asking Reservations Events Online',
        'Other Events',
        'Other Activities',
        'Latest Promotion',
        'Latest Activity',
        'Special Offers',
        'Offer',
        'Valid From',
        'Valid Until',
        'Additional Value Adds',
        'Book Offer Now',
        'Other Offers',
        'Read More',
        'News',
        'Download',
        'Download This Press',
        'Related News',
        'What To Do Act In',
        'What To Do Around',
        'All',
        'Shops',
        'Restaurants',
        'Attractions',
        'Misc',
        'Please Wait',
        'Reservations Details',
        'Select Spa',
        'Select Treatment Category',
        'Select Treatment List',
        'No Of Person',
        'Promotion Code',
        'Type Promotion Code',
        'Tax Service Charge',
        'Total',
        'Time',
        'Hour',
        'Minute',
        'Where are You Staying',
        'Type Here',
        'Special Request',
        'Treatment Note',
        'Nationality',
        'Choose Nationality',
        'Type Your Name Here',
        'Type Your Email',
        'Mobile Phone Number',
        'Type Your Number',
        'Prefer Contact',
        'Terms Condition Accept Note',
        'Contact Details',
        'Choose Destinations',
        'Book at Samaya',
        'Submit',
        'Location Map',
        'Scroll Top',
        'Follow Us On',
        'Message',
        'Subject',
        'Enter Subject',
        'Wedding Inquiry Form',
        'Spouse Full Name',
        'Spouse Nationality',
        'Phone Wedding',
        'Wedding Date',
        'Number of Guest',
        'Occasion Type',
        'Theme Color',
        'Food',
        'Dinner Reception',
        'Note Dinner Reception',
        'Dinner Reception Venue',
        'Special Request Wedding',
        'Another Question',
        'Type Your Answer',
        'Other',
        'All Media',
        'All Photos',
        'Photo',
        'Video',
        'Photo 360',
        'Select Event',
        'Events Start Date',
        'Events End Date',
        'Please Provide All Necessarry',
        'Number of Guest Events',
        'Phone Number Include Country Code',
        'Make an Inquiry',
        'Title Salutation',
        'First Name',
        'Last Name',
        'Country of Residence',
        'Choose Country of Residence',
        'Enquiry',
        'Return To Home'
    );

    foreach($list_str as $d)
    {
        $string_list[] = array(
            'string' => $d,
            'name' => generateSefUrl($d)
        );
    }


	$button ='
	<li>'.button('button=save_changes&label=Save').'</li>
    ';
    $button .= "<li><input type=\"button\" class=\"btn-default-button\" name=\"language_list\" value=\"Back to Language List\" onclick=\"location='".get_state_url('language')."';\"></li>";
    
    $language_list = array();
    $q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
    $n = $db->num_rows($r);
    
    if($n > 0)
    {
		$i_lang = 0;
        while($d = $db->fetch_array($r))
        {
            $lang_id          = $d['larticle_id'];
			$lang_name        = $d['larticle_title'];
			$code             = strtoupper(get_additional_field( $lang_id, 'language_code', 'language' ));
			$code_lang_lower  = strtolower($code);
			$status_default   = get_additional_field( $lang_id, 'status_default', 'language' );
			
            $language_list[] = array(
                'lang_name'       => $lang_name,
                'code_lang_lower' => $code_lang_lower,
                'default'         => $status_default
            );
        }
    }

    
    
    set_template(PLUGINS_PATH.'/language/form_string.html','language_form_template');
    add_block('language_string_list','lang_string_list','language_form_template');
    add_block('language_form','lang_form','language_form_template');
    add_actions('section_title','String Translations');

    add_variable('form_title','String Translation Language');

    add_variable('site_url',SITE_URL);
	add_variable('plugin_url',SITE_URL.'/lumonata-plugins/language');
	add_variable('state_url',get_state_url('language-string'));
    add_variable('button',$button);

    
    if($nstring > 0)
    {
        $dstring = $db->fetch_array($rstring);
        $string_id = $dstring['larticle_id'];
        $content_string = $dstring['larticle_content'];
    }

    
    foreach($string_list as $d=>$value)
    {
        $name_lang_string = $value['string'];
        add_variable('string', $value['string']);
        add_variable('name', $name_lang_string);

        $content_string_encode = json_decode($content_string, true);

        
        
        $translations = '';
        if(!empty($language_list))
        {
            foreach($language_list as $ll)
            {
                $value_string = '';

                // if($ll['default'])
                // {
                //     $value_string = $name_lang_string;
                // }
                // else
                // {
                    if(isset($content_string_encode[$name_lang_string]))
                    {
                        $name_lang_string_data = $content_string_encode[$name_lang_string];
                        $value_string = $name_lang_string_data[$ll['code_lang_lower']];
                    }
                // }

                $translations .= '
                    <div class="input-translation clearfix">
                        <label>'.$ll['lang_name'].'</label>
                        <input type="text" value="'.$value_string.'" name="string['.$name_lang_string.']['.$ll['code_lang_lower'].']" />
                    </div>
                ';
            }
        }

        
        add_variable('translations', $translations);
        parse_template('language_string_list','lang_string_list', true);
    }
    
    parse_template('language_form','lang_form',false);
	return return_template('language_form_template');
}
?>