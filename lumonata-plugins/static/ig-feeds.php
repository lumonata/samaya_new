<?php

session_start();

require_once(__DIR__.'../../../lumonata_config.php');
require_once(__DIR__.'../../../lumonata-functions/settings.php');
require_once(__DIR__.'../../../lumonata-admin/includes/instagram-api-php/src/Instagram.php');

$app_id     = get_meta_data('ig_client_id','static_setting');
$app_secret = get_meta_data('ig_client_secret','static_setting');

if(!empty($app_id) && !empty($app_secret))
{
	// initialize class
	$instagram = new MetzWeb\Instagram\Instagram(array(
	    'apiKey' => $app_id,
	    'apiSecret' => $app_secret,
	    'apiCallback' => HTTP.site_url().'/lumonata-plugins/static/ig-callback.php'
	));

	$loginUrl = $instagram->getLoginUrl(array('basic','public_content'));
	header('Location:'.$loginUrl);
}
else
{
	header('Location:'.HTTP.site_url().'/lumonata-admin/?state=global_settings&tab=static');
}

?>