function view_detail_slideshow()
{
    jQuery("#thumb_slideshow_list").click(function(){
        jQuery("#detail_thumb_slideshow_list").slideToggle(100);
        return false;
    });
}

function manage_slideshow_image()
{
	if( jQuery('.slideshow-form').length > 0 )
	{
		Dropzone.autoDiscover = false;

		var ajax_url   = jQuery('[name=ajax_url]').val();
	    var myDropzone = new Dropzone('#slide-upload', { 
	        url: ajax_url,
	        previewTemplate: document.querySelector('#preview-template').innerHTML,
	        maxFilesize: 2,
	        init: function()
	        {
	        	var url = ajax_url;
			    var prm = new Object;
			        prm.ajax_key = 'get_slideshow';
			        prm.post_id  = jQuery('[name=post_id]').val();

			    jQuery.post(url, prm, function(e){
			        if( e.result=='success' )
			        {
			            jQuery.each(e.data, function(i, e){
			                var mockFile = { 
			                    url:e.url,
			                    id:e.img_id,
			                    title:e.title, 
			                    name: e.img_title,
			                    subtitle:e.subtitle,
			                    description:e.description,
			                    use_link:e.use_link,
			                    post_link_id:e.post_link_id,
			                    button_text:e.button_text
			                };

			                myDropzone.emit('addedfile', mockFile);
			                myDropzone.emit('thumbnail', mockFile, e.img_thumb);
			                myDropzone.emit('complete', mockFile);
			                myDropzone.files.push(mockFile);
			            });
			        }
			    },'json');

			    this.on('addedfile', function(file) {			        
			        var selector  = jQuery(file.previewElement);
			        var url   = ( typeof file.url != 'undefined' ? file.url : '');
			        var title = ( typeof file.title != 'undefined' ? file.title : '');
			        var sub   = ( typeof file.subtitle != 'undefined' ? file.subtitle : '');
			        var desc  = ( typeof file.description != 'undefined' ? file.description : '');
			        var id    = ( typeof file.id != 'undefined' ? file.id : '');
			        var ulink = ( typeof file.use_link != 'undefined' ? file.use_link : '');
			        var pid   = ( typeof file.post_link_id != 'undefined' ? file.post_link_id : '');
			        var btn   = ( typeof file.button_text != 'undefined' ? file.button_text : '');

			        selector.attr('id','slide_'+id)
			        selector.find('.dz-detail-box [name=url]').val(url);
			        selector.find('.dz-detail-box [name=title]').val(title);
			        selector.find('.dz-detail-box [name=subtitle]').val(sub);
			        selector.find('.dz-detail-box [name=description]').val(desc);
			        selector.find('.dz-detail-box [name=use_link]').val(ulink);
			        selector.find('.dz-detail-box [name=post_link_id]').val(pid);
			        selector.find('.dz-detail-box [name=button_text]').val(btn);

		        	if( ulink==1 ){
		        		selector.find('.dz-detail-box .link-box select').removeAttr('disabled');
		        		selector.find('.dz-detail-box .link-box input').removeAttr('disabled');
		        		selector.find('.dz-detail-box .button-box input').removeAttr('disabled');	        		
		        	}

			        selector.find('.dz-delete').attr('rel', id);
			        selector.find('.dz-detail-box .save-detail').attr('rel', id);

			        selector.find('.dz-edit').unbind('click');
			        selector.find('.dz-edit').click(function(){
			            selector.toggleClass('opened');
			        });

			        selector.find('.dz-delete').unbind('click');
			        selector.find('.dz-delete').click(function(){
			            var url = ajax_url;
			            var prm = new Object;
			                prm.ajax_key  = 'delete_slideshow_img';
			                prm.attach_id = jQuery(this).attr('rel');

			            jQuery.post(url, prm, function(e){

			                if(e.result=='success')
			                {
			                    myDropzone.removeFile(file);
			                }
			                else
			                {
			                    var offset = jQuery('.alert-wrapper').offset().top - 50;
			                    var alert  = 
			                    '<div class="alert_red">'+
			                        'Failed to delete this image'+
			                    '</div>';

			                    jQuery('.alert-wrapper').html(alert);
			                    jQuery('html, body').animate({ scrollTop: offset }, 500);
			                }

			            },'json');
			        }); 

			        selector.find('.dz-detail-box .save-detail').unbind('click');
			        selector.find('.dz-detail-box .save-detail').click(function(){
			            var url = ajax_url;
			            var prm = new Object;
			                prm.ajax_key     = 'edit_slideshow_info';
			                prm.attach_id    = jQuery(this).attr('rel');
			                prm.title        = selector.find('.dz-detail-box [name=title]').val();  
			                prm.subtitle     = selector.find('.dz-detail-box [name=subtitle]').val();  
			                prm.url          = selector.find('.dz-detail-box [name=url]').val();  
			                prm.description  = selector.find('.dz-detail-box [name=description]').val(); 
			                prm.use_link     = selector.find('.dz-detail-box [name=use_link]').val(); 
			                prm.post_link_id = selector.find('.dz-detail-box [name=post_link_id]').val();
			                prm.button_text  = selector.find('.dz-detail-box [name=button_text]').val();

			            jQuery.post(url, prm, function(e){
			                selector.removeClass('opened');
			            },'json'); 

			            return false;
			        });

			        selector.find('.dz-detail-box .close-detail').unbind('click');
			        selector.find('.dz-detail-box .close-detail').click(function(){
			            selector.removeClass('opened');
			        });

			        selector.find('.dz-detail-box .use-link-opt').unbind('change');
			        selector.find('.dz-detail-box .use-link-opt').change(function(){
			        	if( jQuery(this).val()==1 ){
			        		selector.find('.dz-detail-box .link-box select').removeAttr('disabled');
			        		selector.find('.dz-detail-box .link-box input').removeAttr('disabled');
			        		selector.find('.dz-detail-box .button-box input').removeAttr('disabled');	        		
			        	}else{
			        		selector.find('.dz-detail-box .link-box select').attr('disabled', 'disabled');
			        		selector.find('.dz-detail-box .link-box input').attr('disabled', 'disabled');
			        		selector.find('.dz-detail-box .button-box input').attr('disabled', 'disabled');
			        	}
			        });

			        selector.find('.dz-detail-box .link-box select').unbind('change');
			        selector.find('.dz-detail-box .link-box select').change(function(){
			        	var link = jQuery('option:selected', this).attr('data-link');
			        	selector.find('.dz-detail-box .link-box input').val(link);
			        });
				});

				this.on('error', function(file, message) { 
					alert(message);
					myDropzone.removeFile(file);
				});

				this.on('success', function(file, responseText){
					var e = jQuery.parseJSON(responseText);

		            if( e.result = 'success' )
		            {
		                var selector  = jQuery(file.previewElement);
		                    selector.find('.dz-delete').attr('rel', e.attach_id);
		                    selector.find('.dz-detail-box .save-detail').attr('rel', e.attach_id);
		            }
		            else
		            {
		                myDropzone.removeFile(file);   
		            }
		        });

				this.on('sending', function(file, xhr, formData){
		            formData.append( 'ajax_key', 'upload_slideshow' );
		            formData.append( 'post_id', jQuery('[name=post_id]').val() );
				});
	        }
	    });
	}
}

/*
| -------------------------------------------------------------------------------------
| Reorder slideshow image
| -------------------------------------------------------------------------------------
*/
function reorder_slide_image()
{
	jQuery('#slide-upload').nestedSortable({
		handle: 'div',
		items: '.dz-preview',
		listType:'div',
		opacity: .6,
		tolerance: 'pointer',
		maxLevels:0,
		stop: function(eve, ui){
			var url  = jQuery('[name=ajax_url]').val();
			var list = jQuery('#slide-upload').nestedSortable('toArray');
			var objc = new Array;
				jQuery.each(list,function(i,e){
					if( !jQuery.isEmptyObject(e.item_id) )
					{
						objc.push(e.item_id);
					}
				});
				
			var param = new Object;
				param.ajax_key	= 'reorder_slideshow';
				param.id	    = jQuery('[name=post_id]').val();
				param.value     = JSON.stringify(objc);

			jQuery.post(url, param, function(el){
				console.log(el);
			},'json');
		}
	});
}

jQuery(document).ready(function(){

	view_detail_slideshow();

	manage_slideshow_image();

	reorder_slide_image();

});